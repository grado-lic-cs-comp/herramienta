package exceptions;

/**
 * Manejador de excepciones DAO 
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class DAOGenericException extends Exception {

    public DAOGenericException(String message) {
        super(message);
    }

    public DAOGenericException(String message, Throwable cause) {
        super(message, cause);
        System.out.println("DAO EXception CAUSE: "+cause.toString());
    }

    public DAOGenericException(Throwable cause) {
        super(cause);
    }
}
