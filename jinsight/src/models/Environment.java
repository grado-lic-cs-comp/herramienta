package models;

/**
 * Modelo Ambiente
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class Environment {
    private String name;
    private String codigoJava;
    private String html;
    private String rutaHtml;
    private int loc;
    
    public Environment(){
        this.name = "";
        this.codigoJava = "";
        this.html = "";
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getCodigoJava() {
        return codigoJava;
    }

    public void setCodigoJava(String codigoJava) {
        this.codigoJava = codigoJava;
    }

    public String concatCodigoJava(String codigoJava) {
        this.codigoJava+= codigoJava;
        return this.codigoJava;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String concatCodigoHtml(String codigoHtml) {
        this.html+= codigoHtml;
        return this.html;
    }
    
    public String getRutaHtml() {
        return rutaHtml;
    }

    public void setRutaHtml(String rutaHtml) {
        this.rutaHtml = rutaHtml;
    }

    public int getLoc() {
        return loc;
    }

    public void setLoc(int loc) {
        this.loc = loc;
    }
}
