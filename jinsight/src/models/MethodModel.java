package models;

import java.util.ArrayList;

/**
 * Modelo Ambiente Método
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MethodModel extends Environment {
    private ArrayList<String> scope = null;
    private ArrayList<ParameterModel> parametros = null;
    private ArrayList<MethodAttributeModel> atributos = null;
    private boolean override;

    public MethodModel() {
        super();
        this.parametros = new ArrayList<ParameterModel>();
        this.atributos = new ArrayList<MethodAttributeModel>();
        this.scope = new ArrayList<String>();
    }
    
    public ArrayList<ParameterModel> getParametros() {
        return this.parametros;
    }
    
    public void addParametro(String tipo, String nombre, int position) {
        ParameterModel parametro = new ParameterModel(tipo, nombre, position);
        this.parametros.add(parametro);
    }
    
    public ArrayList<MethodAttributeModel> getAtributos() {
        return atributos;
    }

    public void addAtributos(MethodAttributeModel atributo) {
        this.atributos.add(atributo);
    }
    
    public ArrayList<String> getScope() {
        return scope;
    }

    public void setScope(ArrayList<String> scope) {
        this.scope = scope;
    }
    
    public void addScope(String scope) {
        this.scope.add(scope);
    }

    public boolean isOverride() {
        return override;
    }

    public void setOverride(boolean override) {
        this.override = override;
    }
}
