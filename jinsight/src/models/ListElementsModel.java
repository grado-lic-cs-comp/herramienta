package models;

import exceptions.DAOGenericException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.DefaultListModel;

/**
 * Modelo Lista de Elementos
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ListElementsModel {
    private MainWindowModel mainWindowModel;
    private DefaultListModel<String> model;

    public ListElementsModel(MainWindowModel mainWinowModel){
        this.mainWindowModel = mainWinowModel;
        this.model = new DefaultListModel<>();
    }

    public ArrayList<String> getListElementsFile(String filePath) {
        String call = "";
        ArrayList<String> elements = new ArrayList<String>();
        try {
            int file_id = this.mainWindowModel.getConnectionMySQL().getFileDAO().readIdByPath(filePath);
            call = "{call get_elements_file(?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            stmtType.setInt(1, file_id);
            ResultSet resultSet = stmtType.executeQuery();
            while (resultSet.next()) {
                elements.add(resultSet.getString("name"));
            }
        } catch (SQLException | DAOGenericException e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsClass - " + e.toString());
        }
        return elements;
    }

    public ArrayList<String> getListElementsClass(String filePath, String className) {
        String call = "";
        ArrayList<String> elements = new ArrayList<String>();
        try {
            int file_id = this.mainWindowModel.getConnectionMySQL().getFileDAO().readIdByPath(filePath);
            int class_id = this.mainWindowModel.getConnectionMySQL().getClassDAO().readIdByFileName(file_id, className);
            call = "{call get_methods_id_by_class(?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            stmtType.setInt(1, class_id);
            ResultSet resultSet = stmtType.executeQuery();
            while (resultSet.next()) {
                elements.add(this.getNameMethodParameters(resultSet.getInt("id")));
            }
        } catch (SQLException | DAOGenericException e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsClass - " + e.toString());
        }
        return elements;
    }

    public ArrayList<String> getMethodLocalvariables(int method_id) {
        String call = "";
        ArrayList<String> elements = new ArrayList<String>();
        try {
            call = "{call get_method_local_variables_by_method(?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            stmtType.setInt(1, method_id);
            ResultSet resultSet = stmtType.executeQuery();
            while (resultSet.next()) {
                elements.add(resultSet.getString("name"));
            }
        } catch (Exception e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsClass - " + e.toString());
        }
        return elements;
    }
    
    public String getNameMethodParameters(int method_id) {
        String call = "";
        String header = "";
        try {
            call = "{call get_name_method_parameters_by_id(?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            stmtType.setInt(1, method_id);
            ResultSet resultSet = stmtType.executeQuery();
            int i = 0;
            while (resultSet.next()) {
                if (i == 0) {
                    header+= resultSet.getString("m_name") + "(";
                } else {
                    header+= ", ";
                }
                if (resultSet.getString("p_type") != null && resultSet.getString("p_name") != null) {
                    header+= resultSet.getString("p_type");
                    header+= " " + resultSet.getString("p_name");
                    i++;
                } else {
                    break;
                }
            }
            header+= ")";
        } catch (Exception e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsClass - " + e.toString());
        }
        return header;
    }
    
    public DefaultListModel<String> getModel() {
        return model;
    }

    public void setModel(DefaultListModel<String> model) {
        this.model = model;
    }
    
    public void clearModel() {
        this.model.removeAllElements();
    }
}
