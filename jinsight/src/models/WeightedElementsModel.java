package models;

import exceptions.DAOGenericException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 * Modelo Estructura Jerárquica
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class WeightedElementsModel {
    private Object[][] data;
    private TableModel tableModel;
    private MainWindowModel mainWindowModel;
    
    public WeightedElementsModel(MainWindowModel mainWindowModel) {
        this.data = new Object[][]{};
        Object[] columnNames = new Object[]{"Name", "Value"};
        Class[] columnTypes = new Class[]{String.class, Integer.class};
        this.tableModel = new DefaultTableModel(this.data, columnNames) {
            public Class<?> getColumnClass(int columnIndex) {
                return columnTypes[columnIndex];
            }
        };
        this.mainWindowModel = mainWindowModel;
    }
    
    public Object[][] getData() {
        return data;
    }

    public void setData(Object[][] data) {
        try {
        this.data = data;
        } catch(Exception e) {
            System.out.println("Error: WeightedElementsModel - setData - " + e.toString());
        }
    }

    public ArrayList<MetricValueModel> getValuesTreeMapFile(String filePath) {
        String call = "";
        ArrayList<MetricValueModel> arrayValuesMetrics = new ArrayList<MetricValueModel>();
        try {
            int file_id = this.mainWindowModel.getConnectionMySQL().getFileDAO().readIdByPath(filePath);
            call = "{call get_metric_class_loc(?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            stmtType.setInt(1, file_id);
            ResultSet resultSet = stmtType.executeQuery();
            while (resultSet.next()) {
                arrayValuesMetrics.add(new MetricValueModel(
                    resultSet.getString("name"),
                    resultSet.getInt("value")
                ));
            }
        } catch (SQLException | DAOGenericException e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsClass - " + e.toString());
        }
        return arrayValuesMetrics;
    }
    
    public String getNameMethodParameters(int method_id) {
        String call = "";
        String header = "";
        try {
            call = "{call get_name_method_parameters_by_id(?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            stmtType.setInt(1, method_id);
            ResultSet resultSet = stmtType.executeQuery();
            int i = 0;
            while (resultSet.next()) {
                if (i == 0) {
                    header+= resultSet.getString("m_name") + "(";
                } else {
                    header+= ", ";
                }
                if (resultSet.getString("p_type") != null && resultSet.getString("p_name") != null) {
                    header+= resultSet.getString("p_type");
                    header+= " " + resultSet.getString("p_name");
                    i++;
                } else {
                    break;
                }
            }
            header+= ")";
        } catch (Exception e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsClass - " + e.toString());
        }
        return header;
    }
    
    public ArrayList<MetricValueModel> getValuesTreeMapClass(String filePath, String className) {
        String call = "";
        ArrayList<MetricValueModel> arrayValuesMetrics = new ArrayList<MetricValueModel>();
        try {
            int file_id = this.mainWindowModel.getConnectionMySQL().getFileDAO().readIdByPath(filePath);
            int class_id = this.mainWindowModel.getConnectionMySQL().getClassDAO().readIdByFileName(file_id, className);
            call = "{call get_methods_by_class_id(?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            stmtType.setInt(1, class_id);
            ResultSet resultSet = stmtType.executeQuery();
            while (resultSet.next()) {
                arrayValuesMetrics.add(new MetricValueModel(
                    this.getNameMethodParameters(resultSet.getInt("id")),
                    resultSet.getInt("value")
                ));
            }
        } catch (SQLException | DAOGenericException e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsClass - " + e.toString());
        }
        return arrayValuesMetrics;
    }

    public ArrayList<MetricValueModel> getMethodLocalvariables(int method_id) {
        String call = "";
        ArrayList<MetricValueModel> arrayValuesMetrics = new ArrayList<MetricValueModel>();
        try {
            call = "{call get_method_local_variables_by_method(?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            stmtType.setInt(1, method_id);
            ResultSet resultSet = stmtType.executeQuery();
            while (resultSet.next()) {
                arrayValuesMetrics.add(new MetricValueModel(
                    resultSet.getString("name"),
                    1
                ));
            }
        } catch (Exception e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsClass - " + e.toString());
        }
        return arrayValuesMetrics;
    }
    
    public TableModel getTableModel() {
        return tableModel;
    }

    public void setTableModel(TableModel tableModel) {
        this.tableModel = tableModel;
    }
        
    public void loadTableModel() {
        Object[] columnNames = new Object[]{"Name", "Value"};
        Class[] columnTypes = new Class[]{String.class, Integer.class};
        this.tableModel = new DefaultTableModel(this.data, columnNames) {
            public Class<?> getColumnClass(int columnIndex) {
                return columnTypes[columnIndex];
            }
        };
    }    
}
