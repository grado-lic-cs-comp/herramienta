package models.dao.mysqldao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.dto.FileDTO;
import exceptions.DAOGenericException;
import interfaces.FileDAO;

/**
 * Modelo DAO Archivo - Gestor MySQL
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class FileMySQLDAO implements FileDAO {
    private Connection conn;
    private ResultSet resultSet;
    private CallableStatement callableStatement;

    public FileMySQLDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public Integer create(FileDTO object) throws DAOGenericException {
        int id = -1;
        String call = "{call insert_file(?,?,?,?,?,?,?,?)}";
        callableStatement = null;
        try {
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setString(1, object.getName());
            callableStatement.setInt(2, object.getPackage_id());
            callableStatement.setString(3, object.getPath());
            callableStatement.setString(4, object.getTreePath());
            callableStatement.setInt(5, object.getLoc());
            callableStatement.setBoolean(6, object.isActive());
            callableStatement.setString(7, object.getCodeHtml());
            callableStatement.registerOutParameter(8, java.sql.Types.INTEGER);
            callableStatement.execute();
            id = callableStatement.getInt(8);
        } catch(SQLException e) {
            throw new DAOGenericException("Error - File - create ", e);
        } finally {
            this.closeCallableStatement();
        }
        return id;
    }

    @Override
    public FileDTO read(Integer key) throws DAOGenericException {
        FileDTO fileDTO = null;
        callableStatement = null;
        resultSet = null;
        try {
            String call = "{call get_file_by_id(?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, key);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {
                fileDTO = this.convert(resultSet);
            } else {
                throw new DAOGenericException("Not fond register");
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return fileDTO;
    }
    
    public int readIdByPath(String path) throws DAOGenericException {
        int id = -1;
        callableStatement = null;
        resultSet = null;
        try {
            String call = "{call get_id_file_by_path(?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setString(1, path);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {    
                id = resultSet.getInt("id");
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return id;
    }

    public FileDTO readFileDTOByPath(String path) throws DAOGenericException {
        FileDTO fileDTO = null;
        callableStatement = null;
        resultSet = null;
        try {
            String call = "{call get_file_by_path(?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setString(1, path);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {   
                fileDTO = this.convert(resultSet);
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return fileDTO;
    }
    
    @Override
    public ArrayList<FileDTO> readAll() throws DAOGenericException {
        callableStatement = null;
        resultSet = null;
        ArrayList<FileDTO> list = new ArrayList<FileDTO>();
        try {
            String call = "{call get_files_all()}";
            callableStatement = this.conn.prepareCall(call);
            resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {    
                list.add(this.convert(resultSet));
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - readAll", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return list;
    }

    @Override
    public boolean deleteAll() throws DAOGenericException {
        boolean response = false;
        callableStatement = null;
        try {
            String call = "{call delete_files_all()}";
            callableStatement = this.conn.prepareCall(call);
            response = callableStatement.execute();
        } catch(SQLException e) {
            throw new DAOGenericException("Error - deleteAll", e);
        } finally {
            this.closeCallableStatement();
        }
        return response;
    }

    public boolean updateActiveAll() throws DAOGenericException {
        boolean response = false;
        callableStatement = null;
        try {
            String call = "{call update_files_active()}";
            callableStatement = this.conn.prepareCall(call);
            response = callableStatement.execute();
        } catch(SQLException e) {
            throw new DAOGenericException("Error - updateActiveAll", e);
        } finally {
            this.closeCallableStatement();
        }
        return response;    
    }

    public boolean incrementScreenShot(String path) throws DAOGenericException {
        boolean response = false;
        callableStatement = null;
        try {
            String call = "{call update_files_increment_screenshot(?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setString(1, path);
            response = callableStatement.execute();
        } catch(SQLException e) {
            throw new DAOGenericException("Error - incrementScreenShot", e);
        } finally {
            this.closeCallableStatement();
        }
        return response;
    }
    
    private FileDTO convert(ResultSet rs) throws DAOGenericException {
        String name;
        int package_id;
        String path;
        String treePath;
        boolean active;
        int loc;
        String codeHtml;
        int count_screenshot;
        try {
            name = rs.getString("name");
            package_id = rs.getInt("package_id");
            path = rs.getString("path");
            treePath = rs.getString("treePath");
            loc = rs.getInt("loc");
            active = rs.getBoolean("active");
            codeHtml = rs.getString("code_html");
            count_screenshot = rs.getInt("count_screenshot");
        } catch (SQLException ex) {
            throw new DAOGenericException("Error - convert");
        }
        return new FileDTO(name, package_id, path, treePath, loc, active, codeHtml, count_screenshot);
    }
    
    public void closeResultSet() throws DAOGenericException {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error - closeAll - resultSet", e);
            }                
        }
    }

    public void closeCallableStatement() throws DAOGenericException {
        if (callableStatement != null) {
            try {
                callableStatement.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error  - closeAll - callableStatement", e);
            }                
        }    
    }
}
