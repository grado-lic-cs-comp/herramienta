package models.dao.mysqldao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.dto.MetricTypeDTO;
import exceptions.DAOGenericException;
import interfaces.MetricTypeDAO;

/**
 * Modelo DAO Tipo de Métrica - Gestor MySQL
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MetricTypeMySQLDAO implements MetricTypeDAO {
    private Connection conn;
    private ResultSet resultSet;
    private CallableStatement callableStatement;

    public MetricTypeMySQLDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public Integer create(MetricTypeDTO object) throws DAOGenericException {
        int id = -1;
        String call = "{call insert_metric_type(?,?)}";
        callableStatement = null;
        try {
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setString(1, object.getName());
            callableStatement.registerOutParameter(2, java.sql.Types.INTEGER);
            callableStatement.execute();
            id = callableStatement.getInt(2);
        } catch(SQLException e) {
            throw new DAOGenericException("Error - create", e);
        } finally {
            this.closeCallableStatement();
        }
        return id;
    }

    @Override
    public MetricTypeDTO read(Integer key) throws DAOGenericException {
        MetricTypeDTO metricTypeDTO = null;
        callableStatement = null;
        resultSet = null;
        try {
            String call = "{call get_metric_type_by_id(?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, key);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {    
                metricTypeDTO = this.convert(resultSet);
            } else {
                throw new DAOGenericException("Not fond register");
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return metricTypeDTO;
    }

    @Override
    public ArrayList<MetricTypeDTO> readAll() throws DAOGenericException {
        callableStatement = null;
        resultSet = null;
        ArrayList<MetricTypeDTO> list = new ArrayList<MetricTypeDTO>();
        try {
            String call = "{call get_metric_types_all()}";
            callableStatement = this.conn.prepareCall(call);
            resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {    
                list.add(this.convert(resultSet));
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - readAll", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return list;
    }

    @Override
    public boolean deleteAll() throws DAOGenericException {
        boolean response = false;
        callableStatement = null;
        try {
            String call = "{call delete_metric_types_all()}";
            callableStatement = this.conn.prepareCall(call);
            response = callableStatement.execute();
        } catch(SQLException e) {
            throw new DAOGenericException("Error - deleteAll", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return response;
    }

    private MetricTypeDTO convert(ResultSet rs) throws DAOGenericException {
        String name;
        try {
            name = rs.getString("name");
        } catch (SQLException ex) {
            throw new DAOGenericException("Error - convert");
        }
        return new MetricTypeDTO(name);
    }
    
    public void closeResultSet() throws DAOGenericException {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error - closeAll - resultSet", e);
            }                
        }
    }

    public void closeCallableStatement() throws DAOGenericException {
        if (callableStatement != null) {
            try {
                callableStatement.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error  - closeAll - callableStatement", e);
            }                
        }    
    }
}