package models.dao.mysqldao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import exceptions.DAOGenericException;
import interfaces.ClassDAO;
import models.dto.ClassDTO;

/**
 * Modelo DAO Clase - Gestor MySQL
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ClassMySQLDAO implements ClassDAO {

    private Connection conn;
    private ResultSet resultSet;
    private CallableStatement callableStatement;
    
    public ClassMySQLDAO(Connection conn) {
        this.conn = conn;
    }
    
    @Override
    public Integer create(ClassDTO object) throws DAOGenericException {
        int id = -1;
        String call = "{call insert_class(?,?,?,?,?)}";
        callableStatement = null;
        try {
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setString(1, object.getName());
            callableStatement.setInt(2, object.getFile_id());
            callableStatement.setInt(3, object.getLoc());
            callableStatement.setString(4, object.getCodeHtml());
            callableStatement.registerOutParameter(5, java.sql.Types.INTEGER);
            callableStatement.execute();
            id = callableStatement.getInt(5);
        } catch(SQLException e) {
            throw new DAOGenericException("Error - Class - create", e);
        } finally {
            this.closeCallableStatement();
        }
        return id;
    }

    @Override
    public ClassDTO read(Integer key) throws DAOGenericException {
        ClassDTO classDTO = null;
        callableStatement = null;
        resultSet = null;
        try {
            String call = "{call get_class_by_id(?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, key);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {    
                classDTO = this.convert(resultSet);
            } else {
                throw new DAOGenericException("No se ha encontrado registro");
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return classDTO;
    }

    @Override
    public ArrayList<ClassDTO> readAll() throws DAOGenericException {
        callableStatement = null;
        resultSet = null;
        ArrayList<ClassDTO> list = new ArrayList<ClassDTO>();
        try {
            String call = "{call get_class_all()}";
            callableStatement = this.conn.prepareCall(call);
            resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {    
                list.add(this.convert(resultSet));
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - readAll", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return list;
    }

    public ArrayList<ClassDTO> readClassByFile(int file_id) throws DAOGenericException {
        callableStatement = null;
        resultSet = null;
        ArrayList<ClassDTO> list = new ArrayList<ClassDTO>();
        try {
            String call = "{call get_classes_by_file_id(?)}";
            callableStatement.setInt(1, file_id);
            callableStatement = this.conn.prepareCall(call);
            resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {    
                list.add(this.convert(resultSet));
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - readClassByFile", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return list;
    }    
    
    public int readIdByFileName(int file_id, String className) throws DAOGenericException {
        int id = -1;
        callableStatement = null;
        resultSet = null;
        try {
            String call = "{call get_id_class_by_file_classname(?,?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, file_id);
            callableStatement.setString(2, className);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {    
                id = resultSet.getInt("id");
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return id;
    }

    public ClassDTO readClassDTOByFileName(int file_id, String className) throws DAOGenericException {
        int id = -1;
        callableStatement = null;
        resultSet = null;
        ClassDTO classDTO = null;
        try {
            String call = "{call get_class_by_file_classname(?,?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, file_id);
            callableStatement.setString(2, className);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {    
                classDTO = this.convert(resultSet);
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read - ", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return classDTO;
    }

    @Override
    public boolean deleteAll() throws DAOGenericException {
        boolean response = false;
        callableStatement = null;
        try {
            String call = "{call delete_class_all()}";
            callableStatement = this.conn.prepareCall(call);
            response = callableStatement.execute();
        } catch(SQLException e) {
            throw new DAOGenericException("Error - deleteAll", e);
        } finally {
            this.closeCallableStatement();
        }
        return response;
    }
        
    private ClassDTO convert(ResultSet rs) throws DAOGenericException {
        String name;
        int file_id_id;
        int loc;
        String codeHtml;
        try {
            name = rs.getString("name");
            file_id_id = rs.getInt("file_id");
            loc = rs.getInt("loc");
            codeHtml = rs.getString("code_html");
        } catch (SQLException ex) {
            throw new DAOGenericException("Error - convert");
        }
        return new ClassDTO(name, file_id_id, loc, codeHtml);
    }
    
    public void closeResultSet() throws DAOGenericException {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error - closeAll - resultSet", e);
            }                
        }
    }

    public void closeCallableStatement() throws DAOGenericException {
        if (callableStatement != null) {
            try {
                callableStatement.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error  - closeAll - callableStatement", e);
            }                
        }    
    }
}
