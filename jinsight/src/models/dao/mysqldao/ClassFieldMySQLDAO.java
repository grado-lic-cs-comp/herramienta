package models.dao.mysqldao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.dto.ClassFieldDTO;
import exceptions.DAOGenericException;
import interfaces.ClassFieldDAO;

/**
 * Modelo DAO Campo de Clase - Gestor MySQL
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ClassFieldMySQLDAO implements ClassFieldDAO {

    private Connection conn;
    private ResultSet resultSet;
    private CallableStatement callableStatement;

    public ClassFieldMySQLDAO(Connection conn) {
        this.conn = conn;
    }
    
    @Override
    public Integer create(ClassFieldDTO object) throws DAOGenericException {
        int id = -1;
        String call = "{call insert_class_field(?,?,?)}";
        callableStatement = null;
        try {
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setString(1, object.getName());
            callableStatement.setInt(2, object.getClass_id());
            callableStatement.registerOutParameter(3, java.sql.Types.INTEGER);
            callableStatement.execute();
            id = callableStatement.getInt(3);
        } catch(SQLException e) {
            throw new DAOGenericException("Error - ClassField- create", e);
        } finally {
            this.closeCallableStatement();
        }
        return id;
    }

    @Override
    public ClassFieldDTO read(Integer key) throws DAOGenericException {
        ClassFieldDTO classFieldDTO = null;
        callableStatement = null;
        resultSet = null;
        try {
            String call = "{call get_class_field_by_id(?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, key);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {    
                classFieldDTO = this.convert(resultSet);
            } else {
                throw new DAOGenericException("Not fond register");
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return classFieldDTO;
    }

    @Override
    public ArrayList<ClassFieldDTO> readAll() throws DAOGenericException {
        callableStatement = null;
        resultSet = null;
        ArrayList<ClassFieldDTO> list = new ArrayList<ClassFieldDTO>();
        try {
            String call = "{call get_class_field_all()}";
            callableStatement = this.conn.prepareCall(call);
            resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {    
                list.add(this.convert(resultSet));
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - readAll", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return list;
    }

    @Override
    public boolean deleteAll() throws DAOGenericException {
        boolean response = false;
        callableStatement = null;
        try {
            String call = "{call delete_class_field_all()}";
            callableStatement = this.conn.prepareCall(call);
            response = callableStatement.execute();
        } catch(SQLException e) {
            throw new DAOGenericException("Error - deleteAll", e);
        } finally {
            this.closeCallableStatement();
        }
        return response;
    }

    private ClassFieldDTO convert(ResultSet rs) throws DAOGenericException {
        String name;
        int class_id;
        try {
            name = rs.getString("class_id");
            class_id = rs.getInt("class_id");
        } catch (SQLException ex) {
            throw new DAOGenericException("Error - convert");
        }
        return new ClassFieldDTO(name, class_id);
    }
    
    public void closeResultSet() throws DAOGenericException {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error - closeAll - resultSet", e);
            }                
        }
    }

    public void closeCallableStatement() throws DAOGenericException {
        if (callableStatement != null) {
            try {
                callableStatement.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error  - closeAll - callableStatement", e);
            }                
        }    
    }    
}
