package models.dao.mysqldao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.dto.MetricSuggestionDTO;
import exceptions.DAOGenericException;
import interfaces.MetricSuggestionDAO;

/**
 * Modelo DAO Sugerencia de Métrica - Gestor MySQL
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MetricSuggestionMySQLDAO implements MetricSuggestionDAO {
    private Connection conn;
    private ResultSet resultSet;
    private CallableStatement callableStatement;

    public MetricSuggestionMySQLDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public Integer create(MetricSuggestionDTO object) throws DAOGenericException {
        int id = -1;
        String call = "{call insert_metric_suggestion(?,?,?,?)}";
        callableStatement = null;
        try {
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, object.getMetric_id());
            callableStatement.setString(2, object.getSuggestion_spanish());
            callableStatement.setString(3, object.getSuggestion_english());
            callableStatement.registerOutParameter(4, java.sql.Types.INTEGER);
            callableStatement.execute();
            id = callableStatement.getInt(4);
        } catch(SQLException e) {
            throw new DAOGenericException("Error - create", e);
        } finally {
            this.closeCallableStatement();
        }
        return id;
    }

    @Override
    public MetricSuggestionDTO read(Integer key) throws DAOGenericException {
        MetricSuggestionDTO metricSuggestionDTO = null;
        callableStatement = null;
        resultSet = null;
        try {
            String call = "{call get_metric_suggestion_by_id(?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, key);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {    
                metricSuggestionDTO = this.convert(resultSet);
            } else {
                throw new DAOGenericException("Not fond register");
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return metricSuggestionDTO;
    }

    @Override
    public ArrayList<MetricSuggestionDTO> readAll() throws DAOGenericException {
        callableStatement = null;
        resultSet = null;
        ArrayList<MetricSuggestionDTO> list = new ArrayList<MetricSuggestionDTO>();
        try {
            String call = "{call get_metric_suggestions_all()}";
            callableStatement = this.conn.prepareCall(call);
            resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {    
                list.add(this.convert(resultSet));
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - readAll", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return list;
    }

    @Override
    public boolean deleteAll() throws DAOGenericException {
        boolean response = false;
        callableStatement = null;
        try {
            String call = "{call delete_metric_suggestions_all()}";
            callableStatement = this.conn.prepareCall(call);
            response = callableStatement.execute();
        } catch(SQLException e) {
            throw new DAOGenericException("Error - deleteAll", e);
        } finally {
            this.closeCallableStatement();
        }
        return response;
    }

    private MetricSuggestionDTO convert(ResultSet rs) throws DAOGenericException {
        int metric_id;
        String suggestion_spanish;
        String suggestion_english;
        try {
            metric_id = rs.getInt("metric_id");
            suggestion_spanish = rs.getString("suggestion_spanish");
            suggestion_english = rs.getString("suggestion_english");
        } catch (SQLException ex) {
            throw new DAOGenericException("Error - convert");
        }
        return new MetricSuggestionDTO(metric_id, suggestion_spanish, suggestion_english);
    }
    
    public void closeResultSet() throws DAOGenericException {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error - closeAll - resultSet", e);
            }                
        }
    }

    public void closeCallableStatement() throws DAOGenericException {
        if (callableStatement != null) {
            try {
                callableStatement.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error  - closeAll - callableStatement", e);
            }                
        }    
    }        
}
