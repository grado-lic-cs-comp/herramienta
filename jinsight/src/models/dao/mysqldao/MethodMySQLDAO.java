package models.dao.mysqldao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.dto.MethodDTO;
import exceptions.DAOGenericException;
import interfaces.MethodDAO;

/**
 * Modelo DAO Método - Gestor MySQL
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MethodMySQLDAO implements MethodDAO {
    private Connection conn;
    private ResultSet resultSet;
    private CallableStatement callableStatement;

    public MethodMySQLDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public Integer create(MethodDTO object) throws DAOGenericException {
        int id = -1;
        String call = "{call insert_method(?,?,?,?,?,?)}";
        callableStatement = null;
        try {
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setString(1, object.getName());
            callableStatement.setInt(2, object.getClass_id());
            callableStatement.setBoolean(3, object.isOverride());
            callableStatement.setInt(4, object.getLoc());
            callableStatement.setString(5, object.getCodeHtml());
            callableStatement.registerOutParameter(6, java.sql.Types.INTEGER);
            callableStatement.execute();
            id = callableStatement.getInt(6);
        } catch(SQLException e) {
            throw new DAOGenericException("Error - create", e);
        } finally {
            this.closeCallableStatement();
        }
        return id;
    }

    @Override
    public MethodDTO read(Integer key) throws DAOGenericException {
        MethodDTO methodDTO = null;
        callableStatement = null;
        resultSet = null;
        try {
            String call = "{call get_method_by_id(?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, key);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {    
                methodDTO = this.convert(resultSet);
            } else {
                throw new DAOGenericException("Not fond register");
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return methodDTO;
    }
    
    @Override
    public ArrayList<MethodDTO> readAll() throws DAOGenericException {
        callableStatement = null;
        resultSet = null;
        ArrayList<MethodDTO> list = new ArrayList<MethodDTO>();
        try {
            String call = "{call get_methods_all()}";
            callableStatement = this.conn.prepareCall(call);
            resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {    
                list.add(this.convert(resultSet));
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - readAll", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return list;
    }

    @Override
    public boolean deleteAll() throws DAOGenericException {
        boolean response = false;
        callableStatement = null;
        try {
            String call = "{call delete_methods_all()}";
            callableStatement = this.conn.prepareCall(call);
            response = callableStatement.execute();
        } catch(SQLException e) {
            throw new DAOGenericException("Error - deleteAll", e);
        } finally {
            this.closeCallableStatement();
        }
        return response;
    }

    private MethodDTO convert(ResultSet rs) throws DAOGenericException {
        String name;
        int class_id;
        boolean override;
        int loc;
        String codeHtml;
        try {
            name = rs.getString("name");
            class_id = rs.getInt("class_id");
            override = rs.getBoolean("override");
            loc = rs.getInt("loc");
            codeHtml = rs.getString("code_html");
        } catch (SQLException ex) {
            throw new DAOGenericException("Error - convert");
        }
        return new MethodDTO(name, class_id, override, loc, codeHtml);
    }
    
    public void closeResultSet() throws DAOGenericException {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error - closeAll - resultSet", e);
            }                
        }
    }

    public void closeCallableStatement() throws DAOGenericException {
        if (callableStatement != null) {
            try {
                callableStatement.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error  - closeAll - callableStatement", e);
            }                
        }    
    } 
}
