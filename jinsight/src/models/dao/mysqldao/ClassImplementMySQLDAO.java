package models.dao.mysqldao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.dto.ClassImplementDTO;
import exceptions.DAOGenericException;
import interfaces.ClassImplementDAO;

/**
 * Modelo DAO Implementación de Interface - Gestor MySQL
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ClassImplementMySQLDAO implements ClassImplementDAO {
    private Connection conn;
    private ResultSet resultSet;
    private CallableStatement callableStatement;

    public ClassImplementMySQLDAO(Connection conn) {
        this.conn = conn;
    }
    
    @Override
    public Integer create(ClassImplementDTO object) throws DAOGenericException {
        int id = -1;
        String call = "{call insert_class_implement(?,?,?)}";
        callableStatement = null;
        try {
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, object.getClass_id());
            callableStatement.setInt(2, object.getImplemented_class_id());
            callableStatement.registerOutParameter(3, java.sql.Types.INTEGER);
            callableStatement.execute();
            id = callableStatement.getInt(3);
        } catch(SQLException e) {
            throw new DAOGenericException("Error - create", e);
        } finally {
            this.closeCallableStatement();
        }
        return id;
    }

    @Override
    public ClassImplementDTO read(Integer key) throws DAOGenericException {
        ClassImplementDTO classImplementDTO = null;
        callableStatement = null;
        resultSet = null;
        try {
            String call = "{call get_class_implements_by_id(?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, key);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {    
                classImplementDTO = this.convert(resultSet);
            } else {
                throw new DAOGenericException("Not fond register");
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return classImplementDTO;
    }

    @Override
    public ArrayList<ClassImplementDTO> readAll() throws DAOGenericException {
        callableStatement = null;
        resultSet = null;
        ArrayList<ClassImplementDTO> list = new ArrayList<ClassImplementDTO>();
        try {
            String call = "{call get_class_implements_all()}";
            callableStatement = this.conn.prepareCall(call);
            resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {    
                list.add(this.convert(resultSet));
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - readAll", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return list;
    }

    @Override
    public boolean deleteAll() throws DAOGenericException {
        boolean response = false;
        callableStatement = null;
        try {
            String call = "{call delete_class_implements_all()}";
            callableStatement = this.conn.prepareCall(call);
            response = callableStatement.execute();
        } catch(SQLException e) {
            throw new DAOGenericException("Error - deleteAll", e);
        } finally {
            this.closeCallableStatement();
        }
        return response;
    }

    private ClassImplementDTO convert(ResultSet rs) throws DAOGenericException {
        int class_id;
        int implemented_call_id;
        try {
            class_id = rs.getInt("class_id");
            implemented_call_id = rs.getInt("implemented_call_id");
        } catch (SQLException ex) {
            throw new DAOGenericException("Error - convert");
        }
        return new ClassImplementDTO(class_id, implemented_call_id);
    }
    
    public void closeResultSet() throws DAOGenericException {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error - closeAll - resultSet", e);
            }                
        }
    }

    public void closeCallableStatement() throws DAOGenericException {
        if (callableStatement != null) {
            try {
                callableStatement.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error  - closeAll - callableStatement", e);
            }                
        }    
    } 
}
