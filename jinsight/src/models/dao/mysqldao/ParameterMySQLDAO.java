package models.dao.mysqldao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.dto.ParameterDTO;
import exceptions.DAOGenericException;
import interfaces.ParameterDAO;

/**
 * Modelo DAO Parámetro - Gestor MySQL
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ParameterMySQLDAO implements ParameterDAO {
    private Connection conn;
    private ResultSet resultSet;
    private CallableStatement callableStatement;

    public ParameterMySQLDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public Integer create(ParameterDTO object) throws DAOGenericException {
        int id = -1;
        String call = "{call insert_parameter(?,?,?,?,?)}";
        callableStatement = null;
        try {
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setString(1, object.getType());
            callableStatement.setString(2, object.getName());
            callableStatement.setInt(3, object.getPosition());
            callableStatement.setInt(4, object.getMethod_id());
            callableStatement.registerOutParameter(5, java.sql.Types.INTEGER);
            callableStatement.execute();
            id = callableStatement.getInt(5);
        } catch(SQLException e) {
            throw new DAOGenericException("Error - create", e);
        } finally {
            this.closeCallableStatement();
        }
        return id;
    }

    @Override
    public ParameterDTO read(Integer key) throws DAOGenericException {
        ParameterDTO parameterDTO = null;
        callableStatement = null;
        resultSet = null;
        try {
            String call = "{call get_parameter_by_id(?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, key);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {    
                parameterDTO = this.convert(resultSet);
            } else {
                throw new DAOGenericException("Not fond register");
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return parameterDTO;
    }

    @Override
    public ArrayList<ParameterDTO> readAll() throws DAOGenericException {
        callableStatement = null;
        resultSet = null;
        ArrayList<ParameterDTO> list = new ArrayList<ParameterDTO>();
        try {
            String call = "{call get_parameters_all()}";
            callableStatement = this.conn.prepareCall(call);
            resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {    
                list.add(this.convert(resultSet));
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - readAll", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return list;
    }

    @Override
    public boolean deleteAll() throws DAOGenericException {
        boolean response = false;
        callableStatement = null;
        try {
            String call = "{call delete_parameters_all()}";
            callableStatement = this.conn.prepareCall(call);
            response = callableStatement.execute();
        } catch(SQLException e) {
            throw new DAOGenericException("Error - deleteAll", e);
        } finally {
            this.closeCallableStatement();
        }
        return response;
    }

    private ParameterDTO convert(ResultSet rs) throws DAOGenericException {
        String type;
        String name;
        int method_id;
        int position;
        try {
            type = rs.getString("type");
            name = rs.getString("name");
            position = rs.getInt("position");
            method_id = rs.getInt("method_id");
        } catch (SQLException ex) {
            throw new DAOGenericException("Error - convert");
        }
        return new ParameterDTO(type, name, position, method_id);
    }
    
    public void closeResultSet() throws DAOGenericException {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error - closeAll - resultSet", e);
            }                
        }
    }

    public void closeCallableStatement() throws DAOGenericException {
        if (callableStatement != null) {
            try {
                callableStatement.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error  - closeAll - callableStatement", e);
            }                
        }    
    } 
}
