package models.dao.mysqldao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.dto.MetricDTO;
import exceptions.DAOGenericException;
import interfaces.MetricDAO;

/**
 * Modelo DAO Métrica - Gestor MySQL
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MetricMySQLDAO implements MetricDAO {
    private Connection conn;
    private ResultSet resultSet;
    private CallableStatement callableStatement;

    public MetricMySQLDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public Integer create(MetricDTO object) throws DAOGenericException {
        int id = -1;
        String call = "{call insert_metric(?,?,?)}";
        callableStatement = null;
        try {
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setString(1, object.getName());
            callableStatement.setInt(2, object.getMetric_type_id());
            callableStatement.registerOutParameter(3, java.sql.Types.INTEGER);
            callableStatement.execute();
            id = callableStatement.getInt(3);
        } catch(SQLException e) {
            throw new DAOGenericException("Error - create", e);
        } finally {
            this.closeCallableStatement();
        }
        return id;
    }

    @Override
    public MetricDTO read(Integer key) throws DAOGenericException {
        MetricDTO metricDTO = null;
        callableStatement = null;
        resultSet = null;
        try {
            String call = "{call get_metric_by_id(?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, key);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {    
                metricDTO = this.convert(resultSet);
            } else {
                throw new DAOGenericException("Not fond register");
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return metricDTO;
    }

    @Override
    public ArrayList<MetricDTO> readAll() throws DAOGenericException {
        callableStatement = null;
        resultSet = null;
        ArrayList<MetricDTO> list = new ArrayList<MetricDTO>();
        try {
            String call = "{call get_metrics_all()}";
            callableStatement = this.conn.prepareCall(call);
            resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {    
                list.add(this.convert(resultSet));
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - readAll", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return list;
    }

    @Override
    public boolean deleteAll() throws DAOGenericException {
        boolean response = false;
        callableStatement = null;
        try {
            String call = "{call delete_metrics_all()}";
            callableStatement = this.conn.prepareCall(call);
            response = callableStatement.execute();
        } catch(SQLException e) {
            throw new DAOGenericException("Error - deleteAll", e);
        } finally {
            this.closeCallableStatement();
        }
        return response;
    }

    private MetricDTO convert(ResultSet rs) throws DAOGenericException {
        String name;
        int metric_type_id;
        try {
            name = rs.getString("name");
            metric_type_id = rs.getInt("metric_type_id");
        } catch (SQLException ex) {
            throw new DAOGenericException("Error - convert");
        }
        return new MetricDTO(name, metric_type_id);
    }
    
    public void closeResultSet() throws DAOGenericException {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error - closeAll - resultSet", e);
            }                
        }
    }

    public void closeCallableStatement() throws DAOGenericException {
        if (callableStatement != null) {
            try {
                callableStatement.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error  - closeAll - callableStatement", e);
            }                
        }    
    }
}
