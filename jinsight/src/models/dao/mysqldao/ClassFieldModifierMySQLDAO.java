package models.dao.mysqldao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.dto.ClassFieldModifierDTO;
import exceptions.DAOGenericException;
import interfaces.ClassFieldModifierDAO;

/**
 * Modelo DAO Modificador de Campo de Clase - Gestor MySQL
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ClassFieldModifierMySQLDAO implements ClassFieldModifierDAO {

    private Connection conn;
    private ResultSet resultSet;
    private CallableStatement callableStatement;

    public ClassFieldModifierMySQLDAO(Connection conn) {
        this.conn = conn;
    }
    
    @Override
    public Integer create(ClassFieldModifierDTO object) throws DAOGenericException {
        int id = -1;
        String call = "{call insert_class_field_modifier(?,?,?)}";
        callableStatement = null;
        try {
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, object.getClass_field_id());
            callableStatement.setInt(2, object.getModifier_id());
            callableStatement.registerOutParameter(3, java.sql.Types.INTEGER);
            callableStatement.execute();
            id = callableStatement.getInt(3);
        } catch(SQLException e) {
            throw new DAOGenericException("Error - create", e);
        } finally {
            this.closeCallableStatement();
        }
        return id;
    }

    @Override
    public ClassFieldModifierDTO read(Integer key) throws DAOGenericException {
        ClassFieldModifierDTO classFieldModifierDTO = null;
        callableStatement = null;
        resultSet = null;
        try {
            String call = "{call get_class_field_modifier_by_id(?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, key);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {    
                classFieldModifierDTO = this.convert(resultSet);
            } else {
                throw new DAOGenericException("Not fond register");
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return classFieldModifierDTO;
    }

    @Override
    public ArrayList<ClassFieldModifierDTO> readAll() throws DAOGenericException {
        callableStatement = null;
        resultSet = null;
        ArrayList<ClassFieldModifierDTO> list = new ArrayList<ClassFieldModifierDTO>();
        try {
            String call = "{call get_class_field_modifiers_all()}";
            callableStatement = this.conn.prepareCall(call);
            resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {    
                list.add(this.convert(resultSet));
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - readAll", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return list;
    }

    @Override
    public boolean deleteAll() throws DAOGenericException {
        boolean response = false;
        callableStatement = null;
        try {
            String call = "{call delete_class_field_modifiers_all()}";
            callableStatement = this.conn.prepareCall(call);
            response = callableStatement.execute();
        } catch(SQLException e) {
            throw new DAOGenericException("Error - deleteAll", e);
        } finally {
            this.closeCallableStatement();
        }
        return response;
    }
    
    private ClassFieldModifierDTO convert(ResultSet rs) throws DAOGenericException {
        int class_field_id;
        int modifier_id;
        try {
            class_field_id = rs.getInt("class_field_id");
            modifier_id = rs.getInt("modifier_id");
        } catch (SQLException ex) {
            throw new DAOGenericException("Error - convert");
        }
        return new ClassFieldModifierDTO(class_field_id, modifier_id);
    }
    
    public void closeResultSet() throws DAOGenericException {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error - closeAll - resultSet", e);
            }                
        }
    }

    public void closeCallableStatement() throws DAOGenericException {
        if (callableStatement != null) {
            try {
                callableStatement.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error  - closeAll - callableStatement", e);
            }                
        }    
    }    
}
