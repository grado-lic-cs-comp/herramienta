package models.dao.mysqldao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.dto.MetricDescriptionDTO;
import exceptions.DAOGenericException;
import interfaces.MetricDescriptionDAO;

/**
 * Modelo DAO Descripción de Métrica - Gestor MySQL
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MetricDescriptionMySQLDAO implements MetricDescriptionDAO {
    private Connection conn;
    private ResultSet resultSet;
    private CallableStatement callableStatement;

    public MetricDescriptionMySQLDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public Integer create(MetricDescriptionDTO object) throws DAOGenericException {
        int id = -1;
        String call = "{call insert_metric_description(?,?,?,?)}";
        callableStatement = null;
        try {
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, object.getMetric_id());
            callableStatement.setString(2, object.getDescription_spanish());
            callableStatement.setString(3, object.getDescription_english());
            callableStatement.registerOutParameter(4, java.sql.Types.INTEGER);
            callableStatement.execute();
            id = callableStatement.getInt(4);
        } catch(SQLException e) {
            throw new DAOGenericException("Error - create", e);
        } finally {
            this.closeCallableStatement();
        }
        return id;
    }

    @Override
    public MetricDescriptionDTO read(Integer key) throws DAOGenericException {
        MetricDescriptionDTO metricDescriptionDTO = null;
        callableStatement = null;
        resultSet = null;
        try {
            String call = "{call get_metric_description_by_id(?)}";
            callableStatement = this.conn.prepareCall(call);
            callableStatement.setInt(1, key);
            resultSet = callableStatement.executeQuery();
            if (resultSet.next()) {    
                metricDescriptionDTO = this.convert(resultSet);
            } else {
                throw new DAOGenericException("Not fond register");
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - read", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return metricDescriptionDTO;
    }

    @Override
    public ArrayList<MetricDescriptionDTO> readAll() throws DAOGenericException {
        callableStatement = null;
        resultSet = null;
        ArrayList<MetricDescriptionDTO> list = new ArrayList<MetricDescriptionDTO>();
        try {
            String call = "{call get_metric_descriptions_all()}";
            callableStatement = this.conn.prepareCall(call);
            resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {    
                list.add(this.convert(resultSet));
            }
        } catch(SQLException e) {
            throw new DAOGenericException("Error - readAll", e);
        } finally {
            this.closeResultSet();
            this.closeCallableStatement();
        }
        return list;
    }

    @Override
    public boolean deleteAll() throws DAOGenericException {
        boolean response = false;
        callableStatement = null;
        try {
            String call = "{call delete_metric_descriptions_all()}";
            callableStatement = this.conn.prepareCall(call);
            response = callableStatement.execute();
        } catch(SQLException e) {
            throw new DAOGenericException("Error - deleteAll", e);
        } finally {
            this.closeCallableStatement();
        }
        return response;
    }

    private MetricDescriptionDTO convert(ResultSet rs) throws DAOGenericException {
        int metric_id;
        String description_spanish;
        String description_english;
        try {
            metric_id = rs.getInt("metric_id");
            description_spanish = rs.getString("description_spanish");
            description_english = rs.getString("description_english");
        } catch (SQLException ex) {
            throw new DAOGenericException("Error - convert");
        }
        return new MetricDescriptionDTO(metric_id, description_spanish, description_english);
    }
    
    public void closeResultSet() throws DAOGenericException {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error - closeAll - resultSet", e);
            }                
        }
    }

    public void closeCallableStatement() throws DAOGenericException {
        if (callableStatement != null) {
            try {
                callableStatement.close();
            } catch (SQLException e) {
                throw new DAOGenericException("Error  - closeAll - callableStatement", e);
            }                
        }    
    }     
}
