package models;

import java.util.ArrayList;

/**
 * Modelo Ambiente Archivo
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class FileModel extends Environment {
    private String path;
    private String treePath;
    private String paquete;
    private ArrayList<String> importaciones = null;
    private ArrayList<ClassModel> clases = null;


    public FileModel() {
        super();
        this.path = "";
        this.treePath = "";
        this.paquete = "";
        this.importaciones = new ArrayList<String>();
        this.clases = new ArrayList<ClassModel>();
    }

    public String getPath() {
        return this.path;
    }
    
    public void setPath(String path) {
        this.path = path;
    }

    public String getTreePath() {
        return treePath;
    }

    public void setTreePath(String treePath) {
        this.treePath = treePath;
    }
    
    public String getPaquete() {
        return this.paquete;
    }

    public void setPaquete(String paquete) {
        this.paquete = paquete;
    }

    public ArrayList<String> getImportaciones() {
        return this.importaciones;
    }

    public void addImportation(String importation) {
        this.importaciones.add(importation);
    }

    public ArrayList<ClassModel> getClases() {
        return this.clases;
    }

    public void addClases(ClassModel clase) {
        this.clases.add(clase);
    }
}