package models;

/**
 * Modelo Métricas de Paquetes
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class PackageMetricValueModel {

    private String package_name;
    private int file_size;
    private int class_size;
    private int method_size;
    private int line_size;

    public PackageMetricValueModel(String package_name, int file_size, int class_size, int method_size, int line_size) {
        this.package_name = package_name;
        this.file_size = file_size;
        this.class_size = class_size;
        this.method_size = method_size;
        this.line_size = line_size;
    }

    public String getPackage_name() {
        return package_name;
    }

    public int getFile_size() {
        return file_size;
    }

    public int getClass_size() {
        return class_size;
    }

    public int getMethod_size() {
        return method_size;
    }

    public int getLine_size() {
        return line_size;
    }    
}
