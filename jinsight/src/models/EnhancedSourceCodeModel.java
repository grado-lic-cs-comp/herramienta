package models;

import exceptions.DAOGenericException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.dto.ClassDTO;
import models.dto.FileDTO;

/**
 * Modelo Código Fuente Mejorado
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class EnhancedSourceCodeModel {
    private MainWindowModel mainWindowModel;

    public EnhancedSourceCodeModel(MainWindowModel mainWindowModel) {
        this.mainWindowModel = mainWindowModel;
    }
    
    public String getFileCodeHtml(String pathFile) {
        String codeHtml = "";
        try {
            FileDTO fileDTO = this.mainWindowModel.getConnectionMySQL().getFileDAO().readFileDTOByPath(pathFile);
            codeHtml = fileDTO.getCodeHtml();
        } catch (DAOGenericException ex) {
            System.out.println("Error - EnhancedSourceCodeModel- getFileCodeHtml - " + ex.toString());
        }
        return codeHtml;    
    }

    public String getClassCodeHtml(String pathFile, String className) {
        String codeHtml = "";
        try {
            int file_id = this.mainWindowModel.getConnectionMySQL().getFileDAO().readIdByPath(pathFile);
            ClassDTO classDTO = this.mainWindowModel.getConnectionMySQL().getClassDAO().readClassDTOByFileName(file_id, className);
            codeHtml = classDTO.getCodeHtml();
        } catch (DAOGenericException ex) {
            System.out.println("Error - EnhancedSourceCodeModel - getClassCodeHtml - " + ex.toString());
        }
        return codeHtml;    
    }

    public String getMethodCodeHtml(String pathFile, String className, String methodName, ArrayList<ParameterModel> parameters) {
        String codeHtml = "";
        try {
            int file_id = this.mainWindowModel.getConnectionMySQL().getFileDAO().readIdByPath(pathFile);
            int class_id = this.mainWindowModel.getConnectionMySQL().getClassDAO().readIdByFileName(file_id, className);
            String call = "{call get_methods_by_methodName_classId(?,?,?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            stmtType.setInt(1, class_id);
            stmtType.setString(2, methodName);
            int parameters_size = 0;
            if (parameters != null) {
                parameters_size = parameters.size();
            }
            stmtType.setInt(3, parameters_size);
            ResultSet resultSet = stmtType.executeQuery();
            int method_id = -1;
            if (parameters_size > 0) {    
                int i = 0;
                while (resultSet.next()) {
                    if (method_id != resultSet.getInt("m_id")) {
                        i = 0;
                        method_id = resultSet.getInt("m_id");
                    }
                    if ((parameters.get(i).getType().trim().replaceAll("<", "&lt;").equals(resultSet.getString("p_type").trim())) &&
                        (parameters.get(i).getName().trim().equals(resultSet.getString("p_name").trim())) &&
                        (parameters.get(i).getPosition() == resultSet.getInt("p_position"))) {
                        i++;
                        if (i == parameters.size()) {
                            codeHtml = resultSet.getString("m_code_html");
                            break;
                        }
                    } else {
                        while (resultSet.next()) {
                            if (method_id != resultSet.getInt("m_id")) {
                                resultSet.previous();
                                break;
                            } 
                        }
                    }                
                }                
            } else {
                if (resultSet.next()) {
                    codeHtml = resultSet.getString("m_code_html");
                }
            }
        } catch(SQLException | DAOGenericException e) {
            System.out.println("Error: EnhancedSourceCodeModel - getMethodCodeHtml - " + e.toString());
        }
        return codeHtml;    
    }
}
