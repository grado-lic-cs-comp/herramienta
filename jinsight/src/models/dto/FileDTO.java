package models.dto;

/**
 * Modelo DTO Clase
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class FileDTO {
    private String name;
    private int package_id;
    private String path;
    private String treePath;
    private int loc;
    private boolean active;
    private String codeHtml;
    private int count_screenshot;
    
    public FileDTO() {
    }

    public FileDTO(String name, int package_id, String path, String treePath, int loc, boolean active, String codeHtml, int count_screenshot) {
        this.name = name;
        this.package_id = package_id;
        this.path = path;
        this.treePath = treePath;
        this.loc = loc;
        this.active = active;
        this.codeHtml = codeHtml;
        this.count_screenshot = count_screenshot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPackage_id() {
        return package_id;
    }

    public void setPackage_id(int package_id) {
        this.package_id = package_id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTreePath() {
        return treePath;
    }

    public void setTreePath(String treePath) {
        this.treePath = treePath;
    }

    public int getLoc() {
        return loc;
    }

    public void setLoc(int loc) {
        this.loc = loc;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCodeHtml() {
        return codeHtml;
    }

    public void setCodeHtml(String codeHtml) {
        this.codeHtml = codeHtml;
    }
    
    public int getCount_screenshot() {
        return count_screenshot;
    }

    public void setCount_screenshot(int count_screenshot) {
        this.count_screenshot = count_screenshot;
    }

    @Override
    public String toString() {
        return "FileDTO{" + "name=" + name + ", package_id=" + package_id + ", path=" + path + ", treePath=" + treePath + ", loc=" + loc + ", active=" + active + ", codeHtml=" + codeHtml + ", count_screenshot=" + count_screenshot + '}';
    }
}
