package models.dto;

/**
 * Modelo DTO Variable local de Método
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MethodLocalVariableDTO {
    private String name;
    private int method_id;

    public MethodLocalVariableDTO() {
    }

    public MethodLocalVariableDTO(String name, int method_id) {
        this.name = name;
        this.method_id = method_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMethod_id() {
        return method_id;
    }

    public void setMethod_id(int method_id) {
        this.method_id = method_id;
    }

    @Override
    public String toString() {
        return "MethodLocalVariableDTO{" + "name=" + name + ", method_id=" + method_id + '}';
    }
}
