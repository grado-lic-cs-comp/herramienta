package models.dto;

/**
 * Modelo DTO Clase implementa Interface
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ClassImplementDTO {
    private int class_id;
    private int implemented_class_id;

    public ClassImplementDTO() {
    }

    public ClassImplementDTO(int class_id, int implemented_class_id) {
        this.class_id = class_id;
        this.implemented_class_id = implemented_class_id;
    }

    public int getClass_id() {
        return class_id;
    }

    public void setClass_id(int class_id) {
        this.class_id = class_id;
    }

    public int getImplemented_class_id() {
        return implemented_class_id;
    }

    public void setImplemented_class_id(int implemented_class_id) {
        this.implemented_class_id = implemented_class_id;
    }

    @Override
    public String toString() {
        return "ClassImplementDTO{" + "class_id=" + class_id + ", implemented_class_id=" + implemented_class_id + '}';
    }
}
