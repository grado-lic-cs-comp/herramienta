package models.dto;

/**
 * Modelo DTO Modificador
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ModifierDTO {
    private String name;

    public ModifierDTO() {
    }

    public ModifierDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ModifierDTO{" + "name=" + name + '}';
    }
}
