package models.dto;

/**
 * Modelo DTO Paquete
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class PackageDTO {
    private String name;

    public PackageDTO() {
    }

    public PackageDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "PackageDTO{" + "name=" + name + '}';
    }
}
