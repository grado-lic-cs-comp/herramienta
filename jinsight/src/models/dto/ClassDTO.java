package models.dto;

/**
 * Modelo DTO Clase
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ClassDTO {
    private String name;
    private int file_id;
    private int loc;
    private String codeHtml;

    public ClassDTO() {
    }

    public ClassDTO(String name, int file_id, int loc, String codehtml) {
        this.name = name;
        this.file_id = file_id;
        this.loc =loc;
        this.codeHtml = codehtml;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFile_id() {
        return file_id;
    }

    public void setFile_id(int file_id) {
        this.file_id = file_id;
    }

    public int getLoc() {
        return loc;
    }

    public void setLoc(int loc) {
        this.loc = loc;
    }

    public String getCodeHtml() {
        return codeHtml;
    }

    public void setCodeHtml(String codeHtml) {
        this.codeHtml = codeHtml;
    }

    @Override
    public String toString() {
        return "ClassDTO{" + "name=" + name + ", file_id=" + file_id + ", loc=" + loc + ", codehtml=" + codeHtml + '}';
    }
}
