package models.dto;

/**
 * Modelo DTO Clase extiende Clase
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ClassExtendDTO {
    private int class_id;
    private int extended_class_id;

    public ClassExtendDTO() {
    }

    public ClassExtendDTO(int class_id, int extended_class_id) {
        this.class_id = class_id;
        this.extended_class_id = extended_class_id;
    }

    public int getClass_id() {
        return class_id;
    }

    public void setClass_id(int class_id) {
        this.class_id = class_id;
    }

    public int getExtended_class_id() {
        return extended_class_id;
    }

    public void setExtended_class_id(int extended_class_id) {
        this.extended_class_id = extended_class_id;
    }

    @Override
    public String toString() {
        return "ClassExtendDTO{" + "class_id=" + class_id + ", extended_class_id=" + extended_class_id + '}';
    }
}
