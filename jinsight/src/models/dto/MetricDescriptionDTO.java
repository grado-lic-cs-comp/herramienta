package models.dto;

/**
 * Modelo DTO Descripción de Métrica
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MetricDescriptionDTO {
    private int metric_id;
    private String description_spanish;
    private String description_english;

    public MetricDescriptionDTO() {
    }

    public MetricDescriptionDTO(int metric_id, String description_spanish, String description_english) {
        this.metric_id = metric_id;
        this.description_spanish = description_spanish;
        this.description_english = description_english;
    }

    public int getMetric_id() {
        return metric_id;
    }

    public void setMetric_id(int metric_id) {
        this.metric_id = metric_id;
    }

    public String getDescription_spanish() {
        return description_spanish;
    }

    public void setDescription_spanish(String description_spanish) {
        this.description_spanish = description_spanish;
    }

    public String getDescription_english() {
        return description_english;
    }

    public void setDescription_english(String description_english) {
        this.description_english = description_english;
    }

    @Override
    public String toString() {
        return "MetricDescriptionDTO{" + "metric_id=" + metric_id + ", description_spanish=" + description_spanish + ", description_english=" + description_english + '}';
    }
}
