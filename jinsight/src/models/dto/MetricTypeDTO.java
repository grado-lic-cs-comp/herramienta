package models.dto;

/**
 * Modelo DTO Tipo de Métrica
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MetricTypeDTO {
    private String name;

    public MetricTypeDTO() {
    }

    public MetricTypeDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MetricTypeDTO{" + "name=" + name + '}';
    }
}
