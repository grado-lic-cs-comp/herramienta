package models.dto;

/**
 * Modelo DTO Modificador de Método
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MethodModifierDTO {
    private int method_id;
    private int modifier_id;

    public MethodModifierDTO() {
    }

    public MethodModifierDTO(int method_id, int modifier_id) {
        this.method_id = method_id;
        this.modifier_id = modifier_id;
    }

    public int getMethod_id() {
        return method_id;
    }

    public void setMethod_id(int method_id) {
        this.method_id = method_id;
    }

    public int getModifier_id() {
        return modifier_id;
    }

    public void setModifier_id(int modifier_id) {
        this.modifier_id = modifier_id;
    }

    @Override
    public String toString() {
        return "MethodModifierDTO{" + "method_id=" + method_id + ", modifier_id=" + modifier_id + '}';
    }
}
