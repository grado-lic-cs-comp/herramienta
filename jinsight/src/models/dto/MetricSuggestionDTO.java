package models.dto;

/**
 * Modelo DTO Sugerencia de Métrica
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MetricSuggestionDTO {
    private int metric_id;
    private String suggestion_spanish;
    private String suggestion_english;

    public MetricSuggestionDTO() {
    }

    public MetricSuggestionDTO(int metric_id, String suggestion_spanish, String suggestion_english) {
        this.metric_id = metric_id;
        this.suggestion_spanish = suggestion_spanish;
        this.suggestion_english = suggestion_english;
    }

    public int getMetric_id() {
        return metric_id;
    }

    public void setMetric_id(int metric_id) {
        this.metric_id = metric_id;
    }

    public String getSuggestion_spanish() {
        return suggestion_spanish;
    }

    public void setSuggestion_spanish(String suggestion_spanish) {
        this.suggestion_spanish = suggestion_spanish;
    }

    public String getSuggestion_english() {
        return suggestion_english;
    }

    public void setSuggestion_english(String suggestion_english) {
        this.suggestion_english = suggestion_english;
    }

    @Override
    public String toString() {
        return "MetricSuggestionDTO{" + "metric_id=" + metric_id + ", suggestion_spanish=" + suggestion_spanish + ", suggestion_english=" + suggestion_english + '}';
    }
}
