package models.dto;

/**
 * Modelo DTO Método
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MethodDTO {
    private String name;
    private int class_id;
    private boolean override;
    private int loc;
    private String codeHtml;

    public MethodDTO() {
    }

    public MethodDTO(String name, int class_id, boolean override, int loc, String codeHtml) {
        this.name = name;
        this.class_id = class_id;
        this.override = override;
        this.loc = loc;
        this.codeHtml = codeHtml;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getClass_id() {
        return class_id;
    }

    public void setClass_id(int class_id) {
        this.class_id = class_id;
    }

    public boolean isOverride() {
        return override;
    }

    public void setOverride(boolean override) {
        this.override = override;
    }

    public int getLoc() {
        return loc;
    }

    public void setLoc(int loc) {
        this.loc = loc;
    }

    public String getCodeHtml() {
        return codeHtml;
    }

    public void setCodeHtml(String codeHtml) {
        this.codeHtml = codeHtml;
    }

    @Override
    public String toString() {
        return "MethodDTO{" + "name=" + name + ", class_id=" + class_id + ", override=" + override + ", loc=" + loc + ", codeHtml=" + codeHtml + '}';
    }
}
