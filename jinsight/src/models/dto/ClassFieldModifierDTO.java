package models.dto;

/**
 * Modelo DTO Modificador de Campo de Clase
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ClassFieldModifierDTO {
    private int class_field_id;
    private int modifier_id;

    public ClassFieldModifierDTO() {
    }

    public ClassFieldModifierDTO(int class_field_id, int modifier_id) {
        this.class_field_id = class_field_id;
        this.modifier_id = modifier_id;
    }

    public int getClass_field_id() {
        return class_field_id;
    }

    public void setClass_field_id(int class_field_id) {
        this.class_field_id = class_field_id;
    }

    public int getModifier_id() {
        return modifier_id;
    }

    public void setModifier_id(int modifier_id) {
        this.modifier_id = modifier_id;
    }

    @Override
    public String toString() {
        return "ClassFieldModifierDTO{" + "class_field_id=" + class_field_id + ", modifier_id=" + modifier_id + '}';
    }
}
