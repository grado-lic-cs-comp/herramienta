package models.dto;

/**
 * Modelo DTO Métrica
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MetricDTO {
    private String name;
    private int metric_type_id;

    public MetricDTO() {
    }

    public MetricDTO(String name, int metric_type_id) {
        this.name = name;
        this.metric_type_id = metric_type_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMetric_type_id() {
        return metric_type_id;
    }

    public void setMetric_type_id(int metric_type_id) {
        this.metric_type_id = metric_type_id;
    }

    @Override
    public String toString() {
        return "MetricDTO{" + "name=" + name + ", metric_type_id=" + metric_type_id + '}';
    }
}
