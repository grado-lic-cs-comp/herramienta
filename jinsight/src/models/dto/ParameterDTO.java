package models.dto;

/**
 * Modelo DTO Parámetro
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ParameterDTO {
    private String type;
    private String name;
    private int position;
    private int method_id;

    public ParameterDTO() {
    }

    public ParameterDTO(String type, String name, int position, int method_id) {
        this.type = type;
        this.name = name;
        this.position = position;
        this.method_id = method_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getMethod_id() {
        return method_id;
    }

    public void setMethod_id(int method_id) {
        this.method_id = method_id;
    }

    @Override
    public String toString() {
        return "ParameterDTO{" + "type=" + type + ", name=" + name + ", position=" + position + ", method_id=" + method_id + '}';
    }

}
