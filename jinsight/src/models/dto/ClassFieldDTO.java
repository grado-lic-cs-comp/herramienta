package models.dto;

/**
 * Modelo DTO Campo de Clase
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ClassFieldDTO {
    private String name;
    private int class_id;

    public ClassFieldDTO() {
    }

    public ClassFieldDTO(String name, int class_id) {
        this.name = name;
        this.class_id = class_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getClass_id() {
        return class_id;
    }

    public void setClass_id(int class_id) {
        this.class_id = class_id;
    }

    @Override
    public String toString() {
        return "ClassFieldDTO{" + "name=" + name + ", class_id=" + class_id + '}';
    } 
}
