package models;

/**
 * Modelo Valores del Directorio
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class TreeValueModel {
    private String path;
    private String treePath;

    public TreeValueModel(String path, String treePath) {
        this.path = path;
        this.treePath = treePath;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTreePath() {
        return treePath;
    }

    public void setTreePath(String treePath) {
        this.treePath = treePath;
    }
}
