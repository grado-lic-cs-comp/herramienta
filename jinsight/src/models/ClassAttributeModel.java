package models;

import java.util.ArrayList;

/**
 * Modelo Campos de Clase
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ClassAttributeModel {
    private String name;
    private String type;
    private ArrayList<String> scope;

    public ClassAttributeModel() {
        this.scope = new ArrayList<String>();
    }

    public ClassAttributeModel(String name, String type, ArrayList<String> scope) {
        this.name = name;
        this.type = type;
        this.scope = scope;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public ArrayList<String> getScope() {
        return scope;
    }

    public void setScope(ArrayList<String> scope) {
        this.scope = scope;
    }
    
    public void addScope(String scope) {
        this.scope.add(scope);    
    }
}
