package models;

/**
 * Modelo Gráfico de Métricas
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MetricsChartValueModel {
    private String columnName;
    private int value;

    public MetricsChartValueModel() {
    }
    
    public MetricsChartValueModel(String column, int value) {
        this.columnName = column;
        this.value = value;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }   
}
