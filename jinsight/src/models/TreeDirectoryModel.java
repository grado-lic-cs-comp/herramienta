package models;

import exceptions.DAOGenericException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Modelo Directorio
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class TreeDirectoryModel {
    private String name;
    private String path;
    private MainWindowModel mainWindowModel;

    public TreeDirectoryModel(MainWindowModel mainWinowModel){
        this.mainWindowModel = mainWinowModel;
    }
    
    public TreeDirectoryModel(String name, String path) {
            this.name = name;
            this.path = path;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return this.name;
    }
    
    public int getLocTotal() {
        String call = "";
        int value = -1;
        try {
            call = "{call get_metric_total_loc()}";
            CallableStatement callableStatement = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            ResultSet resultSet  = callableStatement.executeQuery();
            if (resultSet.next()) {    
                value = resultSet.getInt("value");
            }
        } catch(SQLException ex) {
            System.out.println("Error: TreeDirectoryController - getLocTotal - " + ex.toString());
        } 
        return value;
    }
    
    public int getLocFile(String path) {
        String call = "";
        int value = -1;
        try {
            int file_id = this.mainWindowModel.getConnectionMySQL().getFileDAO().readIdByPath(path);
            call = "{call get_metric_file_loc_by_id(?)}";
            CallableStatement callableStatement = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            callableStatement.setInt(1, file_id);
            ResultSet resultSet  = callableStatement.executeQuery();
            if (resultSet.next()) {    
                value = resultSet.getInt("value");
            }
        } catch(SQLException | DAOGenericException ex) {
            System.out.println("Error: TreeDirectoryController - getLocFile - " + ex.toString());
        } 
        return value;        
    }

    public ArrayList<PackageMetricValueModel> getValuesPackagesMetrics() {
        String call = "";
        ArrayList<PackageMetricValueModel> arrayValuesMetrics = new ArrayList<PackageMetricValueModel>();
        try {
            call = "{call get_metric_packages_general()}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            ResultSet resultSet = stmtType.executeQuery();
            while (resultSet.next()) {
                arrayValuesMetrics.add(new PackageMetricValueModel(
                    resultSet.getString("package_name"),
                    resultSet.getInt("file_size"),
                    resultSet.getInt("class_size"),
                    resultSet.getInt("method_size"),
                    resultSet.getInt("line_size")
                ));
            }
        } catch (SQLException e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsClass - " + e.toString());
        }
        return arrayValuesMetrics;
    }
    
    public int getIdFile(String path) {
        int id = -1;
        try {
            id = this.mainWindowModel.getConnectionMySQL().getFileDAO().readIdByPath(path);
        } catch (DAOGenericException ex) {
            System.out.println("Error - getIdFile - " + ex.toString());
        }
        return id;
    }

    public String getFilePath(int file_id) {
        String path = "";
        try {
            path = this.mainWindowModel.getConnectionMySQL().getFileDAO().read(file_id).getPath();
        } catch (DAOGenericException ex) {
            System.out.println("Error - getPathFile - " + ex.toString());
        }
        return path;
    }
        
    public String getFileName(int file_id) {
        String name = "";
        try {
            name = this.mainWindowModel.getConnectionMySQL().getFileDAO().read(file_id).getName();
        } catch (DAOGenericException ex) {
            System.out.println("Error - getPathFile - " + ex.toString());
        }
        return name;    
    }
}
