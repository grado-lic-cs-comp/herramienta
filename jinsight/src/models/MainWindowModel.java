package models;

import exceptions.DAOGenericException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.dto.ClassDTO;
import models.dto.ClassFieldDTO;
import models.dto.ClassFieldModifierDTO;
import models.dto.FileDTO;
import models.dto.MethodDTO;
import models.dto.MethodLocalVariableDTO;
import models.dto.MethodModifierDTO;
import models.dto.PackageDTO;
import models.dto.ParameterDTO;
import utils.ManagerMySQLDAO;

/**
 * Modelo Vista Principal
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MainWindowModel {
    private String ambitoActual = null;
    private String pathFile;
    private String className;
    private String methodName;
    private ArrayList<ParameterModel> parameters;
    private ManagerMySQLDAO connectionMySQL;

    public MainWindowModel() {
        this.className = "";
        this.methodName = "";
        this.parameters = new ArrayList<ParameterModel>();
    }
    
    public boolean updateFilesActiveAll() {
        boolean result = false;
        try {
             result = connectionMySQL.getFileDAO().updateActiveAll();
        } catch (DAOGenericException ex) {
            System.out.println("Error: TreeDirectoryController - updateFilesActiveAll - " + ex.toString());
        } 
        return result;
    }
        
    public boolean updateFileCountScreenShot(String path) {
        boolean result = false;
        try {
             result = connectionMySQL.getFileDAO().incrementScreenShot(path);
        } catch (DAOGenericException ex) {
            System.out.println("Error: TreeDirectoryController - updateFilesActiveAll - " + ex.toString());
        } 
        return result;
    }

    public String getAmbitoActual() {
        return ambitoActual;
    }

    public void setAmbitoActual(String ambitoActual) {
        this.ambitoActual = ambitoActual;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public ArrayList<ParameterModel> getParameters() {
        return parameters;
    }

    public void setParameters(ArrayList<ParameterModel> parameters) {
        this.parameters = parameters;
    }
    
    public void addParameter(String type, String name, int position) {
        this.parameters.add(new ParameterModel(type, name, position));
    }
    
    public void clearEnvironment() {
        this.pathFile = "";
        this.className = "";
        this.methodName = "";
        this.parameters = new ArrayList<ParameterModel>();
    }
    
    public int getFileId() {
        int fileId = -1;
        try {
            fileId = this.connectionMySQL.getFileDAO().readIdByPath(pathFile);
        } catch (DAOGenericException ex) {
            System.out.println("Error: MainWindowModel - getFileName - " + ex.toString());
        }
        return fileId;
    }

    public FileDTO getFileDTO() {
        FileDTO fileDTO = null;
        try {
            fileDTO = this.connectionMySQL.getFileDAO().readFileDTOByPath(this.pathFile);        
        } catch (DAOGenericException ex) {
            System.out.println("Error: MainWindowModel - getFileDTO - " + ex.toString());
        }
        return fileDTO;
    }
    
    public MethodDTO getMethodDTO() {
        String call = "";
        int method_id = -1;
        MethodDTO methodDTO = null;
        ArrayList<MetricValueModel> values = new ArrayList<MetricValueModel>();
        try {
            int file_id = this.connectionMySQL.getFileDAO().readIdByPath(pathFile);
            int class_id = this.connectionMySQL.getClassDAO().readIdByFileName(file_id, className);
            call = "{call get_methods_by_methodName_classId(?,?,?)}";
            CallableStatement stmtType = this.connectionMySQL.getConnection().prepareCall(call);
            stmtType.setInt(1, class_id);
            stmtType.setString(2, methodName);
            stmtType.setInt(3, parameters.size());
            ResultSet resultSet = stmtType.executeQuery();
            if (parameters.size() > 0) {    
                int i = 0;
                while (resultSet.next()) {
                    if (method_id != resultSet.getInt("m_id")) {
                        i = 0;
                        method_id = resultSet.getInt("m_id");
                    }
                    if ((parameters.get(i).getType().trim().equals(resultSet.getString("p_type").trim())) &&
                        (parameters.get(i).getName().trim().equals(resultSet.getString("p_name").trim())) &&
                        (parameters.get(i).getPosition() == resultSet.getInt("p_position"))) {
                        i++;
                        if (i == parameters.size()) {
                            break;
                        }
                    } else {
                        while (resultSet.next()) {
                            if (method_id != resultSet.getInt("m_id")) {
                                resultSet.previous();
                                break;
                            } 
                        }
                    }                
                }                
            } else {
                if (resultSet.next()) {
                    method_id = resultSet.getInt("m_id");
                }
            }
            methodDTO = this.connectionMySQL.getMethodDAO().read(method_id);
        } catch(Exception e) {
            System.out.println("Error: MainWindowModel - getMethodDTO - " + e.toString());
        }
        return methodDTO;        
    }
    
    public String getMethodNameParameters() {
        String methodNameParameters = "";
        try {
            if (this.methodName != "") {
                methodNameParameters+= this.methodName + "(";
                for (int i =0; i < parameters.size(); i++) {
                    methodNameParameters+= parameters.get(i).getType() + " " + parameters.get(i).getName();
                    if (i != parameters.size() - 1 ) {
                        methodNameParameters+= ",";
                    }
                }
                methodNameParameters+= ")";                
            }
        } catch(Exception e) {
            System.out.println("Error: MainWindowModel - getMethodDTO - " + e.toString());
        }
        return methodNameParameters;
    }

    public int getMethodId() {
        String call = "";
        int method_id = -1;
        ArrayList<MetricValueModel> values = new ArrayList<MetricValueModel>();
        try {
            int file_id = this.connectionMySQL.getFileDAO().readIdByPath(pathFile);
            int class_id = this.connectionMySQL.getClassDAO().readIdByFileName(file_id, className);
            call = "{call get_methods_by_methodName_classId(?,?,?)}";
            CallableStatement stmtType = this.connectionMySQL.getConnection().prepareCall(call);
            stmtType.setInt(1, class_id);
            stmtType.setString(2, methodName);
            int parameters_size = 0;
            if (parameters != null) {
                parameters_size = parameters.size();
            }
            stmtType.setInt(3, parameters_size);
            ResultSet resultSet = stmtType.executeQuery();
            if (parameters_size > 0) {    
                int i = 0;
                while (resultSet.next()) {
                    if (method_id != resultSet.getInt("m_id")) {
                        i = 0;
                        method_id = resultSet.getInt("m_id");
                    }
                    if ((parameters.get(i).getType().trim().equals(resultSet.getString("p_type").trim())) &&
                        (parameters.get(i).getName().trim().equals(resultSet.getString("p_name").trim())) &&
                        (parameters.get(i).getPosition() == resultSet.getInt("p_position"))) {
                        i++;
                        if (i == parameters.size()) {
                            break;
                        }
                    } else {
                        while (resultSet.next()) {
                            if (method_id != resultSet.getInt("m_id")) {
                                resultSet.previous();
                                break;
                            } 
                        }
                    }                
                }                
            } else {
                if (resultSet.next()) {
                    method_id = resultSet.getInt("m_id");
                }
            }
        } catch(SQLException | DAOGenericException e) {
            System.out.println("Error: MainWindowModel - getMethodId - " + e.toString());
        }
        return method_id;        
    }    

    public ClassDTO getClassDTO() {
        FileDTO fileDTO = null;
        ClassDTO classDTO = null;
        try {
            int fileId = this.connectionMySQL.getFileDAO().readIdByPath(this.pathFile);
            classDTO = this.connectionMySQL.getClassDAO().readClassDTOByFileName(fileId, this.className);
        } catch (DAOGenericException ex) {
            System.out.println("Error: MainWindowModel - getClassDTO - " + ex.toString());
        }
        return classDTO;
    }    

    public ManagerMySQLDAO getConnectionMySQL() {
        return connectionMySQL;
    }

    public synchronized void saveAmbFileModel(FileModel ambArchivoModel) {
        try {
            int package_id = this.connectionMySQL.getPackageDAO().readByName(ambArchivoModel.getPaquete());
            if (package_id == -1) {
                PackageDTO packageDTO = new PackageDTO(ambArchivoModel.getPaquete());
                package_id = this.connectionMySQL.getPackageDAO().create(packageDTO);
            }
            FileDTO fileDTO = new FileDTO(ambArchivoModel.getName(), package_id, ambArchivoModel.getPath(), ambArchivoModel.getTreePath(), ambArchivoModel.getLoc(), true, ambArchivoModel.getHtml(), 0);
            int file_id = this.connectionMySQL.getFileDAO().create(fileDTO);
            for (ClassModel ambClaseModel : ambArchivoModel.getClases()) {
                ClassDTO classDTO = new ClassDTO(ambClaseModel.getName(), file_id, ambClaseModel.getLoc(), ambClaseModel.getHtml());
                int class_id = this.connectionMySQL.getClassDAO().create(classDTO);
                for (ClassAttributeModel attribute : ambClaseModel.getAtributos()) {
                    ClassFieldDTO classFieldDTO = new ClassFieldDTO(attribute.getName(), class_id);
                    int class_field_id = this.connectionMySQL.getClassFieldDAO().create(classFieldDTO);
                    for (String scope : attribute.getScope()) {
                        int modifier_id = this.connectionMySQL.getModifierDAO().readByName(scope);
                        if (modifier_id != -1) {
                            ClassFieldModifierDTO classFieldModifierDTO = new ClassFieldModifierDTO(class_field_id, modifier_id);
                            int class_field_modifier_id = this.connectionMySQL.getClassFieldModifierDAO().create(classFieldModifierDTO);
                        }
                    }
                }
                for (MethodModel ambMetodoModel : ambClaseModel.getMetodos()) {
                    MethodDTO methodDTO = new MethodDTO(ambMetodoModel.getName(), class_id, ambMetodoModel.isOverride(), ambMetodoModel.getLoc(), ambMetodoModel.getHtml());
                    int method_id = this.connectionMySQL.getMethodDAO().create(methodDTO);
                    for (String scope : ambMetodoModel.getScope()) {
                        int modifier_id = this.connectionMySQL.getModifierDAO().readByName(scope);
                        if (modifier_id != -1) {
                            MethodModifierDTO methodModifierDTO = new MethodModifierDTO(method_id, modifier_id);
                            int method_modifier_id = this.connectionMySQL.getMethodModifierDAO().create(methodModifierDTO);
                        }
                    }
                    for (ParameterModel parameter : ambMetodoModel.getParametros()) {
                        ParameterDTO parameterDTO = new ParameterDTO(parameter.getType(), parameter.getName(), parameter.getPosition(), method_id);
                        this.connectionMySQL.getParameterDAO().create(parameterDTO);
                    }
                    for (MethodAttributeModel attribute : ambMetodoModel.getAtributos()) {
                        MethodLocalVariableDTO methodLocalVariableDTO = new MethodLocalVariableDTO(attribute.getName(), method_id);
                        int method_localvariable = this.connectionMySQL.getMethodLocalVariableDAO().create(methodLocalVariableDTO);
                    }
                }
            }
        } catch (DAOGenericException e) {
            System.out.println("Error: MainWindowModel - saveAmbFileModel - " + e.toString());
        }
    }
    
    public int deleteInformation() {
        String call = "";
        int result = -1;
        try {
            call = "{call delete_information()}";
            CallableStatement callableStatement = this.getConnectionMySQL().getConnection().prepareCall(call);
            callableStatement.executeQuery();
        } catch(SQLException ex) {
            System.out.println("Error: MainWindowModel - deleteInformation - " + ex.toString());
        } 
        return result;        
    }

    public void setConnectionMySQL(ManagerMySQLDAO connectionMySQL) {
        this.connectionMySQL = connectionMySQL;
    }
}