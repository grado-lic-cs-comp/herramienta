package models;

/**
 * Modelo Descripción de Métricas
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MetricDescriptionModel {
    
    private String nombre;
    private String descripcion;

    public MetricDescriptionModel(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescription() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}