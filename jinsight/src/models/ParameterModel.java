package models;

/**
 * Modelo Parámetro
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ParameterModel {
    private String type = "";
    private String name = "";
    private int position;

    public ParameterModel(String type, String name, int position) {
        this.type = type;
        this.name = name;
        this.position = position;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}