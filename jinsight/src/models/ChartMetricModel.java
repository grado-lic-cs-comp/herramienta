package models;

import config.ProjectProperties;
import exceptions.DAOGenericException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 * Modelo Métricas
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ChartMetricModel {
    ArrayList<MetricValueModel> arrayValuesMetrics;
    private MainWindowModel mainWindowModel;
    
    public ChartMetricModel(MainWindowModel mainWindowModel){
        this.arrayValuesMetrics = new ArrayList<MetricValueModel>();
        this.mainWindowModel = mainWindowModel;
    }
    
    public void loadArrayValuesMetricsFile(String filePath, String metricName) {
        String call = "";
        int file_id = -1;
        this.arrayValuesMetrics = new ArrayList<MetricValueModel>();
        try {
            file_id = this.mainWindowModel.getConnectionMySQL().getFileDAO().readIdByPath(filePath);
            if (metricName.trim() != "" && file_id != -1) {
                switch(metricName.trim()) {
                    case "loc":
                        call = "{call get_metric_class_loc(?)}";
                        break;
                    case "npria":
                        call = "{call get_metric_class_napri(?)}";
                        break;
                    case "nproa":
                        call = "{call get_metric_class_napro(?)}";
                        break;
                    case "npuba":
                        call = "{call get_metric_class_napub(?)}";
                        break;
                    case "nmo":
                        call = "{call get_metric_class_nmo(?)}";
                        break;
                    case "nprim":
                        call = "{call get_metric_class_nmpri(?)}";
                        break;                    
                    case "nprom":
                        call = "{call get_metric_class_nmpro(?)}";
                        break;                    
                    case "npm":
                        call = "{call get_metric_class_pim(?)}";
                        break;
                    case "noa":
                        call = "{call get_metric_class_wac(?)}";
                        break;
                    case "nom":
                        call = "{call get_metric_class_wmc(?)}";
                        break;
                }
                CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
                stmtType.setInt(1, file_id);
                ResultSet resultSet = stmtType.executeQuery();
                while (resultSet.next()) {
                    this.arrayValuesMetrics.add(new MetricValueModel(
                        resultSet.getString("name"),
                        resultSet.getInt("value")
                    ));
                }
            }
        } catch (SQLException | DAOGenericException e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsFile - " + e.toString());
        }
     
    }
    
    public void loadArrayValuesMetricsClass(String filePath, String className, String metricName) {
        String call = "";
        this.arrayValuesMetrics = new ArrayList<MetricValueModel>();
        try {
            int file_id = this.mainWindowModel.getConnectionMySQL().getFileDAO().readIdByPath(filePath);
            int class_id = this.mainWindowModel.getConnectionMySQL().getClassDAO().readIdByFileName(file_id, className);
            if (metricName != "") {
                switch(metricName) {
                    case "loc":
                        call = "{call get_metric_method_loc(?)}";
                        break;
                    case "nop":
                        call = "{call get_metric_method_nop(?)}";
                        break;
                    case "nlv":
                        call = "{call get_metric_method_nvl(?)}";
                        break;
                }
                CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
                stmtType.setInt(1, class_id);
                ResultSet resultSet = stmtType.executeQuery();
                while (resultSet.next()) {
                    this.arrayValuesMetrics.add(new MetricValueModel(
                        this.getNameMethodParameters(resultSet.getInt("id")),
                        resultSet.getInt("value")
                    ));
                }
            }
        } catch (SQLException | DAOGenericException e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsClass - " + e.toString());
        }    
    }

    public String getNameMethodParameters(int method_id) {
        String call = "";
        String header = "";
        try {
            call = "{call get_name_method_parameters_by_id(?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            stmtType.setInt(1, method_id);
            ResultSet resultSet = stmtType.executeQuery();
            int i = 0;
            while (resultSet.next()) {
                if (i == 0) {
                    header+= resultSet.getString("m_name") + "(";
                } else {
                    header+= ", ";
                }
                if (resultSet.getString("p_type") != null && resultSet.getString("p_name") != null) {
                    header+= resultSet.getString("p_type");
                    header+= " " + resultSet.getString("p_name");
                    i++;
                } else {
                    break;
                }
            }
            header+= ")";
        } catch (Exception e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsClass - " + e.toString());
        }
        return header;
    }
    
    public ArrayList<MetricValueModel> getMetricsMethod(String filePath, String className, String methodName, ArrayList<ParameterModel> parameters, String language) {
        String call = "";
        ArrayList<MetricValueModel> values = new ArrayList<MetricValueModel>();
        try {
            int file_id = this.mainWindowModel.getConnectionMySQL().getFileDAO().readIdByPath(filePath);
            int class_id = this.mainWindowModel.getConnectionMySQL().getClassDAO().readIdByFileName(file_id, className);
            call = "{call get_methods_by_methodName_classId(?,?,?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            stmtType.setInt(1, class_id);
            stmtType.setString(2, methodName);
            int parameters_size = 0;
            if (parameters != null) {
                parameters_size = parameters.size();
            }
            stmtType.setInt(3, parameters_size);
            ResultSet resultSet = stmtType.executeQuery();
            int method_id = -1;
            if (parameters_size > 0) {    
                int i = 0;
                while (resultSet.next()) {
                    if (method_id != resultSet.getInt("m_id")) {
                        i = 0;
                        method_id = resultSet.getInt("m_id");
                    }
                    if ((parameters.get(i).getType().trim().equals(resultSet.getString("p_type").trim())) &&
                        (parameters.get(i).getName().trim().equals(resultSet.getString("p_name").trim())) &&
                        (parameters.get(i).getPosition() == resultSet.getInt("p_position"))) {
                        i++;
                        if (i == parameters.size()) {
                            break;
                        }
                    } else {
                        while (resultSet.next()) {
                            if (method_id != resultSet.getInt("m_id")) {
                                resultSet.previous();
                                break;
                            } 
                        }
                    }                
                }                
            } else {
                if (resultSet.next()) {
                    method_id = resultSet.getInt("m_id");
                }
            }
            values.add(this.getMetricMethodNop(method_id, language));
            values.add(this.getMetricMethodLoc(method_id, language));
            values.add(this.getMetricMethodNlv(method_id, language));
        } catch(SQLException | DAOGenericException e) {
            System.out.println("Error: ChartMetricModel - getMetricsMethod - " + e.toString());
        }
        return values;        
    }
    
    public MetricValueModel getMetricMethodNop(int method_id, String language) {
        String call = "";
        MetricValueModel metricValueModel = null;
        try {
            call = "{call get_metric_method_nop_by_id(?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            stmtType.setInt(1, method_id);
            ResultSet resultSet = stmtType.executeQuery();
            if (resultSet.next()) {
                metricValueModel = new MetricValueModel(
                    this.getDescriptionMetric("nop", ProjectProperties.CLASS, language),
                    resultSet.getInt("value")
                );
            }
        } catch (Exception e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsClass - " + e.toString());
        } 
        return metricValueModel;
    }

    public MetricValueModel getMetricMethodLoc(int method_id, String language) {
        String call = "";
        MetricValueModel metricValueModel = null;
        try {
            call = "{call get_metric_method_loc_by_id(?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            stmtType.setInt(1, method_id);
            ResultSet resultSet = stmtType.executeQuery();
            if (resultSet.next()) {
                metricValueModel = new MetricValueModel(
                    this.getDescriptionMetric("loc", ProjectProperties.CLASS, language),
                    resultSet.getInt("value")
                );
            }
        } catch (Exception e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsClass - " + e.toString());
        } 
        return metricValueModel;    
    }

    public MetricValueModel getMetricMethodNlv(int method_id, String language) {
        String call = "";
        MetricValueModel metricValueModel = null;
        try {
            call = "{call get_metric_method_nlv_by_id(?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(call);
            stmtType.setInt(1, method_id);
            ResultSet resultSet = stmtType.executeQuery();
            if (resultSet.next()) {
                metricValueModel = new MetricValueModel(
                   this.getDescriptionMetric("nlv", ProjectProperties.CLASS, language),
                    resultSet.getInt("value")
                );
            }
        } catch (Exception e) {
            System.out.println("Error en CLassMetricsModel - loadArrayValuesMetricsClass - " + e.toString());
        } 
        return metricValueModel;        
    }
    
    public DefaultCategoryDataset getDataSetBarMetrics(String axisY) {
        DefaultCategoryDataset dataSet = null;
        try {
            dataSet = new DefaultCategoryDataset();
            for (MetricValueModel data : this.arrayValuesMetrics) {
                dataSet.setValue(data.getCantidad(), axisY, data.getNombre());
            }
                      
        } catch (Exception e) {
            System.out.println("Error: ChartMetricsModel - getDataSetBarMetrics - " + e.toString());
        }
        return dataSet; 
    }
                
    public DefaultPieDataset getDataSetPieMetrics() {
        DefaultPieDataset dataSet = new DefaultPieDataset();
        for (MetricValueModel data : this.arrayValuesMetrics) {
            dataSet.setValue(data.getNombre(), data.getCantidad());
        }
        return dataSet;    
    }
        
    public String getDescriptionMetric(String metric, String type, String _language) {
        String response = "";
        String typeMetrics = "";
        int columnId = 0;
        String columnDescription = "";
        String callDescription = "";
        try {
            switch(_language) {
                case "espanol":
                    columnDescription = "description_spanish";
                    callDescription = "{call get_name_description_spanish_by_type_name(?,?)}";
                    break;
                case "ingles":
                    columnDescription = "description_english";
                    callDescription = "{call get_name_description_english_by_type_name(?,?)}";
                    break;
            }
            switch(type.toUpperCase()) {
                case ProjectProperties.FILE:
                    typeMetrics = "class";
                    break;
                case ProjectProperties.CLASS:
                    typeMetrics = "method";
                    break;                    
            }
            String callType ="{call get_id_type_metrics_by_name(?)}";
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(callType);
            stmtType.setString(1, typeMetrics);
            ResultSet resultSetType = stmtType.executeQuery();
            if (resultSetType.next()) {
                columnId = resultSetType.getInt("id");
            }

            CallableStatement stmtDescription = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(callDescription);
            stmtDescription.setInt(1, columnId);
            stmtDescription.setString(2, metric);
            ResultSet resultSetDescription = stmtDescription.executeQuery();
            if (resultSetDescription.next()) {
                response = resultSetDescription.getString(columnDescription);
            }
        } catch(SQLException e) {
            System.out.println("Error en CLassMetricsModel - getDescriptionMetric - " + e.toString());
        }
        return response;
    }
    
    public String getInterpretationMetric(String metric, String nameType, String _language) {
        String columnSuggestion = "";
        String callSugestions = "";
        String typeMetrics = "";
        String concatSuggestions = "";
        String callTypeMetric = "{call get_id_type_metrics_by_name(?)}";
        try {
            switch(_language) {
                case "espanol":
                    columnSuggestion = "suggestion_spanish";
                    callSugestions = "{call get_name_suggestion_spanish_by_type_name(?,?)}";
                    break;
                case "ingles":
                    columnSuggestion = "suggestion_english";
                    callSugestions = "{call get_name_suggestion_english_by_type_name(?,?)}";
                    break;
            }
            switch(nameType.toUpperCase()) {
                case ProjectProperties.FILE :
                    typeMetrics = "class";
                    break;
                case ProjectProperties.CLASS:
                    typeMetrics = "method";
                    break;                    
            }
            CallableStatement stmtTypeMetric = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(callTypeMetric);
            stmtTypeMetric.setString(1, typeMetrics);
            ResultSet resultSetType = stmtTypeMetric.executeQuery();
            if (resultSetType.next()) {
                CallableStatement stmtSuggestions = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(callSugestions);
                stmtSuggestions.setInt(1, resultSetType.getInt("id"));
                stmtSuggestions.setString(2, metric);
                ResultSet resultSetSuggestions = stmtSuggestions.executeQuery();
                while (resultSetSuggestions.next()) {
                    concatSuggestions += "* " + resultSetSuggestions.getString(columnSuggestion) + "\n";
                }
            }
        } catch(SQLException e) {
            System.out.println("Error en CLassMetricsModel - setClassMetricsModel - " + e.toString());
        }
        return concatSuggestions;
    }   
    
    public ArrayList<MetricDescriptionModel> getDescriptionMetrics(String _language, String nameType) {
        ArrayList<MetricDescriptionModel> arrayDescriptionsMetrics = new ArrayList<MetricDescriptionModel>();
        String columnDescription = "";
        String callDescription = "";
        String typeMetrics = "";
        try {
            switch(_language) {
                case "espanol":
                    columnDescription = "description_spanish";
                    callDescription = "{call get_name_description_spanish_by_type(?)}";
                    break;
                case "ingles":
                    columnDescription = "description_english";
                    callDescription = "{call get_name_description_english_by_type(?)}";
                    break;
            }
            switch(nameType) {
                case ProjectProperties.FILE :
                    typeMetrics = "class";
                    break;
                case ProjectProperties.CLASS:
                    typeMetrics = "method";
                    break;                    
            }
            CallableStatement stmtType = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall("{call get_id_type_metrics_by_name(?)}");
            stmtType.setString(1, typeMetrics);
            ResultSet resultSetType = stmtType.executeQuery();
            if (resultSetType.next()) {
                CallableStatement stmtDescriptions = this.mainWindowModel.getConnectionMySQL().getConnection().prepareCall(callDescription);
                stmtDescriptions.setInt(1, resultSetType.getInt("id"));
                ResultSet resultSetDescriptions = stmtDescriptions.executeQuery();
                while (resultSetDescriptions.next()) {
                    arrayDescriptionsMetrics.add(new MetricDescriptionModel(
                        resultSetDescriptions.getString("name"),
                        resultSetDescriptions.getString(columnDescription)
                    ));
                }
            }
        } catch(SQLException e) {
            System.out.println("Error en CLassMetricsModel - setClassMetricsModel - " + e.toString());
        }
        return arrayDescriptionsMetrics;
    }
}
