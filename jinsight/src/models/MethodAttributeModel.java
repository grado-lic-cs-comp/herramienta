package models;

/**
 * Modelo Variable local de Método
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MethodAttributeModel {
    private String name;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }    
}
