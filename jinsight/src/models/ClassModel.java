package models;

import java.util.ArrayList;

/**
 * Modelo Ambiente Clase
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ClassModel extends Environment {
    private ArrayList<ClassAttributeModel> atributos = null;
    private ArrayList<MethodModel> metodos = null;
    private String scope;

    public ClassModel() {
        super();
        atributos = new ArrayList<ClassAttributeModel>();
        metodos = new ArrayList<MethodModel>();
    }

    public ArrayList<ClassAttributeModel> getAtributos() {
        return atributos;
    }

    public void setAtributos(ArrayList<ClassAttributeModel> atributos) {
        this.atributos = (ArrayList) atributos.clone();
    }

    public void addAttribute(ClassAttributeModel classAttributeModel) {
        this.atributos.add(classAttributeModel);
    }
    
    public ArrayList<MethodModel> getMetodos() {
        return metodos;
    }

    public void addMetodos(MethodModel metodo) {
        this.metodos.add(metodo);
    }
    
    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }  
}