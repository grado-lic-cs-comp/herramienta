package utils;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * Manejador de Estilos
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class CellRenderer extends JLabel implements ListCellRenderer {
       String color;
    
    public CellRenderer(String color) {
        setOpaque(true);
        this.color =color;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        // Assumes the stuff in the list has a pretty toString
        setText(value.toString());
        // based on the index you set the color.  This produces the every other effect.
        if (index % 2 == 0) setBackground(new Color(Integer.valueOf( this.color.substring( 1, 3 ), 16 ),
            Integer.valueOf( this.color.substring( 3, 5 ), 16 ),
            Integer.valueOf( this.color.substring( 5, 7 ), 16 ) ));
        else setBackground(new Color(255, 255, 224));
        return this;
    }
}