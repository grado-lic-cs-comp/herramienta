package utils;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * Manejador de Estilos
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MyListCell extends JLabel implements ListCellRenderer {
    public MyListCell() {
        setOpaque(true);
    }

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        // Assumes the stuff in the list has a pretty toString
        setText(value.toString());
        // based on the index you set the color.  This produces the every other effect.
        if (index % 2 == 0) setBackground(Color.LIGHT_GRAY);
        else setBackground(Color.WHITE);
        return this;
    }    
}
