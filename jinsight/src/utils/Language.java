package utils;

import java.io.IOException;
import java.util.Properties;

/**
 * Controlador de Idiomas
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class Language extends Properties{

    private String actualLanguage;
    
    public Language(String idioma) {  
        try {
            this.actualLanguage = idioma;
            switch (idioma) {
                case "espanol":
                    getProperties("spanish.properties");
                    break;
                case "ingles":
                    getProperties("english.properties");
                    break;
                default:
                    getProperties("spanish.properties");
            }
        } catch (Exception e) {
            System.out.println("Error Language - Language " + e.toString());
        }
    }
    
    private void getProperties(String nameFile) {
        try {
            this.load(getClass().getResourceAsStream("/config/" + nameFile) );
        } catch (IOException ex) {
            System.out.println("Error Language - getProperties " + ex);
        }
    }

    public String getActualLanguage() {
        return actualLanguage;
    }

    public void setActualLanguage(String actualLanguage) {
        this.actualLanguage = actualLanguage;
    }
}