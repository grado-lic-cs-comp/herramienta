package utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * Utilidades para Archivos
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class FilesUtil {
    
    public static void listFilesAndFolders(String directoryName){
        File directory = new File(directoryName);
        File[] fList = directory.listFiles();
        for (File file : fList){
            System.out.println(file.getName());
        }
    }
    
    public static void listFiles(String directoryName){
        File directory = new File(directoryName);
        File[] fList = directory.listFiles();
        for (File file : fList){
            if (file.isFile()){
                System.out.println(file.getName());
            }
        }
    }
    
    public static void listFolders(String directoryName){
        File directory = new File(directoryName);
        File[] fList = directory.listFiles();
        for (File file : fList){
            if (file.isDirectory()){
                System.out.println(file.getName());
            }
        }
    }
    
    public static void listFilesAndFilesSubDirectories(String directoryName){
        File directory = new File(directoryName);
        File[] fList = directory.listFiles();
        for (File file : fList){
            if (file.isFile() && getExtensionOfFile(file).equals("java")){
                System.out.println(file.getAbsolutePath());
            } else if (file.isDirectory()){
                listFilesAndFilesSubDirectories(file.getAbsolutePath());
            }
        }
    }
        
    public static String getExtensionOfFile(File file) {
	String fileExtension="";
	String fileName=file.getName();	
	if(fileName.contains(".") && fileName.lastIndexOf(".")!= 0) {
            fileExtension=fileName.substring(fileName.lastIndexOf(".")+1);
	}	
	return fileExtension;
    }
    
    public static String getNameWithoutExtension(String nameFile, String extension) {
        return nameFile.substring(0, nameFile.length() - extension.length() - 1);
    }
 
    public static int getJavaFilesCount(File file) {
        File[] files = file.listFiles();
        int count = 0;
        for (File f : files) {
            if (f.isDirectory()) {
                count += getJavaFilesCount(f);
            } else {
                if (getExtensionOfFile(f).equals("java")) {
                    count++;        
                }
            }
        }
        return count;
    }     

    public static String readFile(String path) {
        String response = "";
        FileInputStream fileInputStream;
        DataInputStream dataInputStream;
        BufferedReader bufferedReader;
        try {
            fileInputStream = new FileInputStream(path);
            dataInputStream = new DataInputStream(fileInputStream);
            bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                response+= line + "\n";
            }
            dataInputStream.close();
        } catch (Exception e) {
            System.err.println("Error en ArchivoController - leerArchivo: " + e.getMessage());
        }
        return response;
    }

    public static void writeFile(String content, String absolutePath) {
        File newFile;
        FileWriter fw;
        try {
            newFile = new File(absolutePath);
            fw = new FileWriter(newFile);
            fw.write(content);
            fw.close();
        }
        catch (Exception e) {
            System.out.println("Error en ArchivoController - escribirArchivo " + e);
        }    
    }

    public static String indentation(String sourceCode) {
        String result = "";
        try {
            ScriptEngine engine = new ScriptEngineManager(null).getEngineByName("nashorn");
            if (engine != null) {
                engine.eval(new BufferedReader(new InputStreamReader(FilesUtil.class.getResourceAsStream("beautify.js"))));
                Invocable invocable = (Invocable) engine;
                result = (String) invocable.invokeFunction("js_beautify", sourceCode);
            } else {
                System.out.println("Error al recuperar Nashorn");
            }
        } catch (ScriptException | NoSuchMethodException ex) {
            System.out.println("Error en CodigoFuenteMejorado - indentador " + ex);
        }
        return result;
    }
    
    public static int countLinesHtml(String html) {
        int length = 0;
        for (String line : html.split("<br>")) {
            if (!line.replaceAll("&nbsp;","").equals("")) {
                length++;
            }
        }
        return length;
    }

    public void copyFile(String sourceFile, String destinationFile) {
        try {
            Path from = Paths.get(sourceFile);
            Path to = Paths.get(destinationFile);
            CopyOption[] options = new CopyOption[] {
                StandardCopyOption.REPLACE_EXISTING,
                StandardCopyOption.COPY_ATTRIBUTES
            };
            Files.copy(from, to, options);
        } catch (Exception ex) {
            System.out.println("Error en ArchivoController - copiarRuta " + ex);
        }
    }

    public static String deleteComments(String sourceCode) {
        String result = "";
        try {
            ScriptEngine engine = new ScriptEngineManager(null).getEngineByName("nashorn");
            if (engine != null) {
                engine.eval(new BufferedReader(new InputStreamReader(FilesUtil.class.getResourceAsStream("fulljsmin.js"))));
                Invocable invocable = (Invocable) engine;
                result = (String) invocable.invokeFunction("jsmin", sourceCode);
            } else {
                System.out.println("Error al recuperar Nashorn");
            }
        } catch (ScriptException | NoSuchMethodException ex) {
            System.out.println("Error en CodigoFuenteMejorado - removerComentarios " + ex);
        }
        return result;
    }
}