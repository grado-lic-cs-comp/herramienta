package utils;

import config.ProjectProperties;
import interfaces.ClassDAO;
import interfaces.ClassExtendDAO;
import interfaces.ClassFieldDAO;
import interfaces.ClassFieldModifierDAO;
import interfaces.ClassImplementDAO;
import interfaces.DAOManager;
import interfaces.FileDAO;
import interfaces.MethodDAO;
import interfaces.MethodLocalVariableDAO;
import interfaces.MethodModifierDAO;
import interfaces.MetricDAO;
import interfaces.MetricDescriptionDAO;
import interfaces.MetricSuggestionDAO;
import interfaces.MetricTypeDAO;
import interfaces.ModifierDAO;
import interfaces.PackageDAO;
import interfaces.ParameterDAO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import models.dao.mysqldao.ClassExtendMySQLDAO;
import models.dao.mysqldao.ClassFieldModifierMySQLDAO;
import models.dao.mysqldao.ClassFieldMySQLDAO;
import models.dao.mysqldao.ClassImplementMySQLDAO;
import models.dao.mysqldao.ClassMySQLDAO;
import models.dao.mysqldao.FileMySQLDAO;
import models.dao.mysqldao.MethodLocalVariableMySQLDAO;
import models.dao.mysqldao.MethodModifierMySQLDAO;
import models.dao.mysqldao.MethodMySQLDAO;
import models.dao.mysqldao.MetricDescriptionMySQLDAO;
import models.dao.mysqldao.MetricMySQLDAO;
import models.dao.mysqldao.MetricSuggestionMySQLDAO;
import models.dao.mysqldao.MetricTypeMySQLDAO;
import models.dao.mysqldao.ModifierMySQLDAO;
import models.dao.mysqldao.PackageMySQLDAO;
import models.dao.mysqldao.ParameterMySQLDAO;

/**
 * Controlador para el Gestor de Base de Datos
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ManagerMySQLDAO implements DAOManager {

    public static void setInstance(ManagerMySQLDAO aInstance) {
        instance = aInstance;
    }
    private ClassDAO classDAO;
    private ClassExtendDAO classExtendDAO;
    private ClassFieldDAO classFieldDAO;
    private ClassFieldModifierDAO classFieldModifierDAO;
    private ClassImplementDAO classImplementDAO;
    private FileDAO fileDAO;
    private MethodDAO methodDAO;
    private MethodLocalVariableDAO methodLocalVariableDAO;
    private MethodModifierDAO methodModifierDAO;
    private MetricDAO metricDAO;
    private MetricDescriptionDAO metricDescriptionDAO;
    private MetricSuggestionDAO metricSuggestionDAO;
    private MetricTypeDAO metricTypeDAO;
    private ModifierDAO modifierDAO;
    private PackageDAO packageDAO;
    private ParameterDAO parameterDAO;
    
    private Connection connection;
    private static ManagerMySQLDAO instance = null;
    private static String host= "";
    private static String port= "";
    private static String user= "";
    private static String pass= "";
    
    private ManagerMySQLDAO() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");  
            Properties properties = new Properties();
            properties.setProperty("user", user);
            properties.setProperty("password", pass);
            properties.setProperty("useSSL", "false");
            String url = "jdbc:mysql://" + host +":" + port + "/" + ProjectProperties.DATABASE;
            this.connection = DriverManager.getConnection(url, properties);
        } catch (ClassNotFoundException ex) {
            System.out.println("Error: ConnectionMySQL - ConnectionMySQL - " + ex.toString());
        }
    }
    
    public static ManagerMySQLDAO getInstance() throws SQLException {
        if (instance == null) {
            instance = new ManagerMySQLDAO();
        }
        return instance;
    }
    
    @Override
    public ClassDAO getClassDAO() {
        if (this.classDAO == null) {
            this.classDAO = new ClassMySQLDAO(connection);
        }
        return classDAO;
    }

    @Override
    public ClassExtendDAO getClassExtendDAO() {
        if (this.classExtendDAO == null) {
            this.classExtendDAO = new ClassExtendMySQLDAO(connection);
        }
        return this.classExtendDAO;
    }

    @Override
    public ClassFieldDAO getClassFieldDAO() {
        if (this.classFieldDAO == null) {
            this.classFieldDAO = new ClassFieldMySQLDAO(connection);
        }
        return this.classFieldDAO;
    }

    @Override
    public ClassFieldModifierDAO getClassFieldModifierDAO() {
        if (this.classFieldModifierDAO == null) {
            this.classFieldModifierDAO = new ClassFieldModifierMySQLDAO(connection);
        }
        return this.classFieldModifierDAO;
    }

    @Override
    public ClassImplementDAO getClassImplementDAO() {
        if (this.classImplementDAO == null) {
            this.classImplementDAO = new ClassImplementMySQLDAO(connection);
        }
        return classImplementDAO;
    }

    @Override
    public FileDAO getFileDAO() {
        if (this.fileDAO == null) {
            this.fileDAO = new FileMySQLDAO(connection);
        }
        return fileDAO;
    }

    @Override
    public MethodDAO getMethodDAO() {
        if (this.methodDAO == null) {
            this.methodDAO = new MethodMySQLDAO(connection);
        }
        return methodDAO;
    }

    @Override
    public MethodLocalVariableDAO getMethodLocalVariableDAO() {
        if (this.methodLocalVariableDAO == null) {
            this.methodLocalVariableDAO = new MethodLocalVariableMySQLDAO(connection);
        }
        return methodLocalVariableDAO;
    }

    @Override
    public MethodModifierDAO getMethodModifierDAO() {
        if (this.methodModifierDAO == null) {
            this.methodModifierDAO = new MethodModifierMySQLDAO(connection);
        }
        return methodModifierDAO;
    }

    @Override
    public MetricDAO getMetricDAO() {
        if (this.metricDAO == null) {
            this.metricDAO = new MetricMySQLDAO(connection);
        }
        return metricDAO;
    }

    @Override
    public MetricDescriptionDAO getMetricDescriptionDAO() {
        if (this.metricDescriptionDAO == null) {
            this.metricDescriptionDAO = new MetricDescriptionMySQLDAO(connection);
        }
        return metricDescriptionDAO;
    }

    @Override
    public MetricSuggestionDAO getMetricSuggestionDAO() {
        if (this.metricSuggestionDAO == null) {
            this.metricSuggestionDAO = new MetricSuggestionMySQLDAO(connection);
        }
        return metricSuggestionDAO;
    }

    @Override
    public MetricTypeDAO getMetricTypeDAO() {
        if (this.metricTypeDAO == null) {
            this.metricTypeDAO = new MetricTypeMySQLDAO(connection);
        }
        return metricTypeDAO;
    }

    @Override
    public ModifierDAO getModifierDAO() {
        if (this.modifierDAO == null) {
            this.modifierDAO = new ModifierMySQLDAO(connection);
        }
        return modifierDAO;
    }

    @Override
    public PackageDAO getPackageDAO() {
        if (this.packageDAO == null) {
            this.packageDAO = new PackageMySQLDAO(connection);
        }
        return packageDAO;
    }

    @Override
    public ParameterDAO getParameterDAO() {
        if (this.parameterDAO == null) {
            this.parameterDAO = new ParameterMySQLDAO(connection);
        }
        return parameterDAO;
    }

    public Connection getConnection() {
        return connection;
    }

    public static String getHost() {
        return host;
    }

    public static void setHost(String host) {
        ManagerMySQLDAO.host = host;
    }

    public static String getPort() {
        return port;
    }

    public static void setPort(String port) {
        ManagerMySQLDAO.port = port;
    }

    public static String getUser() {
        return user;
    }

    public static void setUser(String user) {
        ManagerMySQLDAO.user = user;
    }

    public static String getPass() {
        return pass;
    }

    public static void setPass(String pass) {
        ManagerMySQLDAO.pass = pass;
    }

    
    
}
