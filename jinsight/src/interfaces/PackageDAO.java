package interfaces;

import exceptions.DAOGenericException;
import models.dto.PackageDTO;

/**
 * Interface PackageDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface PackageDAO extends DAOGeneric<PackageDTO, Integer> {
    public int readByName(String name) throws DAOGenericException;
}
