package interfaces;

import models.dto.ClassExtendDTO;

/**
 * Interface ClassExtendDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface ClassExtendDAO extends DAOGeneric<ClassExtendDTO, Integer> {}
