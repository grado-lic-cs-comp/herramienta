package interfaces;

import models.dto.MetricDTO;

/**
 * Interface MetricDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface MetricDAO extends DAOGeneric<MetricDTO, Integer> {}
