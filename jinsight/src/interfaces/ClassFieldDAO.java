package interfaces;

import models.dto.ClassFieldDTO;

/**
 * Interface ClassFieldDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface ClassFieldDAO  extends DAOGeneric<ClassFieldDTO, Integer> {}
