package interfaces;

import models.dto.ParameterDTO;

/**
 * Interface ParameterDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface ParameterDAO extends DAOGeneric<ParameterDTO, Integer> {}