package interfaces;

import models.dto.MetricTypeDTO;

/**
 * Interface MetricTypeDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface MetricTypeDAO extends DAOGeneric<MetricTypeDTO, Integer> {}