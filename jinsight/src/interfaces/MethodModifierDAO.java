package interfaces;

import models.dto.MethodModifierDTO;

/**
 * Interface MethodModifierDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface MethodModifierDAO extends DAOGeneric<MethodModifierDTO, Integer> {}
