package interfaces;

import models.dto.ClassFieldModifierDTO;

/**
 * Interface ClassFieldModifierDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface ClassFieldModifierDAO extends DAOGeneric<ClassFieldModifierDTO, Integer> {}

