package interfaces;

import exceptions.DAOGenericException;
import models.dto.ModifierDTO;

/**
 * Interface ModifierDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface ModifierDAO extends DAOGeneric<ModifierDTO, Integer> {
    public int readByName(String name) throws DAOGenericException;
}