package interfaces;

import models.dto.ClassImplementDTO;

/**
 * Interface ClassImplementDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface ClassImplementDAO extends DAOGeneric<ClassImplementDTO, Integer> {}
