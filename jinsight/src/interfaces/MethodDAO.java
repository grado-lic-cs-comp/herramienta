package interfaces;

import models.dto.MethodDTO;

/**
 * Interface MethodDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface MethodDAO extends DAOGeneric<MethodDTO, Integer> {}
