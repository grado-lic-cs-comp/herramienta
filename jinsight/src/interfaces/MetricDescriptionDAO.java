package interfaces;

import models.dto.MetricDescriptionDTO;

/**
 * Interface MetricDescriptionDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface MetricDescriptionDAO extends DAOGeneric<MetricDescriptionDTO, Integer> {}