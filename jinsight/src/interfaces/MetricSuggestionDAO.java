package interfaces;

import models.dto.MetricSuggestionDTO;

/**
 * Interface MetricSuggestionDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface MetricSuggestionDAO extends DAOGeneric<MetricSuggestionDTO, Integer> {}