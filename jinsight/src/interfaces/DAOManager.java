package interfaces;

/**
 * Interface DAOManager
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface DAOManager {
    ClassDAO getClassDAO();
    ClassExtendDAO getClassExtendDAO();
    ClassFieldDAO getClassFieldDAO();
    ClassFieldModifierDAO getClassFieldModifierDAO();
    ClassImplementDAO getClassImplementDAO();
    FileDAO getFileDAO();
    MethodDAO getMethodDAO();
    MethodLocalVariableDAO getMethodLocalVariableDAO();
    MethodModifierDAO getMethodModifierDAO();
    MetricDAO getMetricDAO();
    MetricDescriptionDAO getMetricDescriptionDAO();
    MetricSuggestionDAO getMetricSuggestionDAO();
    MetricTypeDAO getMetricTypeDAO();
    ModifierDAO getModifierDAO();
    PackageDAO getPackageDAO();
    ParameterDAO getParameterDAO();
}
