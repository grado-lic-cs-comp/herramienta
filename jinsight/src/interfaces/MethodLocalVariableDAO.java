package interfaces;

import models.dto.MethodLocalVariableDTO;

/**
 * Interface MethodLocalVariableDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface MethodLocalVariableDAO extends DAOGeneric<MethodLocalVariableDTO, Integer> {}
