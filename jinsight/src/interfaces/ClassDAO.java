package interfaces;

import exceptions.DAOGenericException;
import models.dto.ClassDTO;

/**
 * Interface ClassDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface ClassDAO extends DAOGeneric<ClassDTO, Integer> {
    public int readIdByFileName(int file_id, String className) throws DAOGenericException;
    public ClassDTO readClassDTOByFileName(int file_id, String className) throws DAOGenericException;
}
