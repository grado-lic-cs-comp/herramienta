package interfaces;

import exceptions.DAOGenericException;
import models.dto.FileDTO;

/**
 * Interface FileDAO
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface FileDAO extends DAOGeneric<FileDTO, Integer> {
    public int readIdByPath(String path) throws DAOGenericException;
    public FileDTO readFileDTOByPath(String path) throws DAOGenericException;
    public boolean updateActiveAll() throws DAOGenericException;
    public boolean incrementScreenShot(String path)  throws DAOGenericException;
    
}
