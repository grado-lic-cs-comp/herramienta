package interfaces;

import java.util.ArrayList;
import exceptions.DAOGenericException;

/**
 * Interface DAOGeneric
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public interface DAOGeneric <O,T> {
    public T create(O object) throws DAOGenericException;
    public O read(T key) throws DAOGenericException;
    public ArrayList<O> readAll() throws DAOGenericException;
    public boolean deleteAll() throws DAOGenericException;
}
