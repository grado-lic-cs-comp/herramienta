package parallelizer;

import java.io.File;

/**
 * Procesador de Archivo
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class FileProcess extends FilesProcessTask {
    private String path;

        
    public FileProcess(File file, String treePath, ProcessingParallelizer processFolderController) {
        super(file, treePath, processFolderController);
        super.init();
        this.path = file.getPath();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
