package parallelizer;

import compiler.Java6Lex;
import compiler.Java6Parse;
import config.ProjectProperties;
import java.io.File;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressIndicator;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import models.FileModel;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonToken;
import org.antlr.runtime.CommonTokenStream;
import org.jboss.forge.roaster.Roaster;
import utils.FilesUtil;

/**
 * Tareas del Procesador de Archivo
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class FilesProcessTask extends Task<Void> {

    private static int count = 0;
    private String treePath;
    private int totalFiles; 
    private String informationDialog;
    private boolean errorFound;
    private boolean ignoreFound;
    private boolean continueAnalysis;
    private static int fileWithError;
    private static int fileSuccess;
    private static int fileIgnore;
    private static String pathFileInit;
    private File fileToProcess;
    public static final int NUM_STEP = 5;
    private final ProcessingParallelizer processingParallelizer;
    DefaultTableModel table;
    private String descriptionFailFound;
    
    FilesProcessTask(File file, String treePath, ProcessingParallelizer processingParallelizer) {
        this.treePath = treePath;
        this.informationDialog = "";
        this.continueAnalysis = true;
        this.errorFound = false;
        this.ignoreFound = false;        
        this.fileToProcess = file;
        fileWithError = 0;
        fileSuccess = 0;
        fileIgnore = 0;
        this.processingParallelizer = processingParallelizer;
    }

    public void init() {
        table = new DefaultTableModel(5, 2);
        this.processingParallelizer.getDialog().getTblInformation().setModel(table);
        this.setDescriptionColumns();
        this.setDescriptionsTable();
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        ((DefaultTableCellRenderer)this.processingParallelizer.getDialog().getTblInformation().getTableHeader().getDefaultRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
        centerRenderer.setHorizontalAlignment( SwingConstants.CENTER );        
        for (int x = 1; x < 2; x++) {
            this.processingParallelizer.getDialog().getTblInformation().getColumnModel().getColumn(x).setCellRenderer( centerRenderer );
        }
        this.updateValuesTable();
        this.updateMessage(this.processingParallelizer.getMainWindowController().getLanguage().getProperty("waiting") + "...");
    }
    
    public static void clearConters() {
        fileWithError = 0;
        fileSuccess = 0;
        fileIgnore = 0;    
    }
    
    @Override
    protected Void call() {
        try {
            ++count;
            this.updateProgress(ProgressIndicator.INDETERMINATE_PROGRESS, 1);
            this.updateMessage(this.processingParallelizer.getMainWindowController().getLanguage().getProperty("waiting") + "...");
            this.processFile();
            --count;
            if (count <= 0) {
                this.finalizeProcessForSuccess();
                this.processingParallelizer.createTree();
                this.processingParallelizer.getDialog().getBtnAccept().setEnabled(true);
                this.processingParallelizer.getDialog().getLblArrow().setVisible(true);
            }
            this.updateValuesTable();
        } catch (Exception e) {
            System.out.println("Error: FilesProcessTask - call - " + e.toString());
        }
        return null;
    }
    
    public void setDescriptionColumns() {
        this.processingParallelizer.getDialog().getTblInformation().getColumnModel().getColumn(0).setHeaderValue(this.processingParallelizer.getMainWindowController().getLanguage().getProperty("description"));
        this.processingParallelizer.getDialog().getTblInformation().getColumnModel().getColumn(1).setHeaderValue(this.processingParallelizer.getMainWindowController().getLanguage().getProperty("result"));
    }
    
    public void setDescriptionsTable() {
        this.processingParallelizer.getDialog().getTblInformation().getModel().setValueAt((String)this.processingParallelizer.getMainWindowController().getLanguage().getProperty("totalFileProcess"), 0, 0);
        this.processingParallelizer.getDialog().getTblInformation().getModel().setValueAt("      " + (String)this.processingParallelizer.getMainWindowController().getLanguage().getProperty("totalFileProcessSuccess"), 1, 0);
        this.processingParallelizer.getDialog().getTblInformation().getModel().setValueAt("      " + (String)this.processingParallelizer.getMainWindowController().getLanguage().getProperty("totalFileIgnore"), 2, 0);
        this.processingParallelizer.getDialog().getTblInformation().getModel().setValueAt("      " + (String)this.processingParallelizer.getMainWindowController().getLanguage().getProperty("totalFileProcessError"), 3, 0);
        this.processingParallelizer.getDialog().getTblInformation().getModel().setValueAt((String)this.processingParallelizer.getMainWindowController().getLanguage().getProperty("timeProcessing"), 4, 0);
    }
    
    public void updateValuesTable() {
        int numerator = fileSuccess + fileIgnore + fileWithError;
        int denominator = this.processingParallelizer.getListPathJavaFile().size() + fileIgnore + fileWithError;
        int percentage = (int)(((float)numerator / denominator) * 100);
        String totalPercent = numerator + " / " + denominator + " (" + percentage + "%)";
        this.processingParallelizer.getDialog().getTblInformation().getModel().setValueAt((String)totalPercent, 0, 1);
        this.processingParallelizer.getDialog().getTblInformation().getModel().setValueAt((Integer)fileSuccess, 1, 1);
        this.processingParallelizer.getDialog().getTblInformation().getModel().setValueAt((Integer)fileIgnore, 2, 1);
        this.processingParallelizer.getDialog().getTblInformation().getModel().setValueAt((Integer)fileWithError, 3, 1);
        String time = 0 + " " + this.processingParallelizer.getMainWindowController().getLanguage().getProperty("seconds");
        if (this.processingParallelizer.getStartTime() != 0) {
            time = (System.currentTimeMillis() - this.processingParallelizer.getStartTime()) / 1000 + " " + this.processingParallelizer.getMainWindowController().getLanguage().getProperty("seconds");
        }
        this.processingParallelizer.getDialog().getTblInformation().getModel().setValueAt((String)time, 4, 1);
    }     
         
    public void processFile() {
        try {
            this.informationDialog = this.processingParallelizer.getMainWindowController().getLanguage().getProperty("descriptionFile") + ":\n\n " + fileToProcess.getName() + "...";
            FileModel ambArchivoModel = this.extractionOfInformation(fileToProcess, treePath);
            this.updateProgress(4.0 / NUM_STEP, 1);            
            if (!this.errorFound && !this.ignoreFound) {
                this.processingParallelizer.getMainWindowController().getMainWindowModel().saveAmbFileModel(ambArchivoModel);
                this.processingParallelizer.getMainWindowController().getTreeDirectoryController().updateTotalLoc();
                fileSuccess++;
                if (count == 1) {
                    pathFileInit = fileToProcess.getPath();
                }
                this.updateProgress(5.0 / NUM_STEP, 1);
                this.updateMessage(this.processingParallelizer.getMainWindowController().getLanguage().getProperty("done"));
                this.done();
            } 
            else {
                String message = "";
                if (errorFound) {
                    fileWithError++;
                    message = this.processingParallelizer.getMainWindowController().getLanguage().getProperty("fail");
                    String type = this.processingParallelizer.getMainWindowController().getLanguage().getProperty("typeErrorMessage");
                    String lineDescription = this.processingParallelizer.getMainWindowController().getLanguage().getProperty("lineDescription");
                    String[] elementsDescriptionFailFound = this.descriptionFailFound.split(":",2);
                    message += " -> " + type + ": " + elementsDescriptionFailFound[0] + " - " + lineDescription + ": " + elementsDescriptionFailFound[1];
                }
                if (ignoreFound) {
                    fileIgnore++;
                    message = this.processingParallelizer.getMainWindowController().getLanguage().getProperty("ignore");
                    message += " -> " + this.descriptionFailFound;
                }
                int position = this.processingParallelizer.checkContainListJava(fileToProcess.getPath());
                this.processingParallelizer.getListPathJavaFile().remove(position);
                this.updateMessage(message);
                this.failed();
            } 
        } catch(Exception e) {
            System.out.println("Error: ProcessFolderController - processFile - " + e.toString());
        }
    }
    
    public void finalizeProccessForFailure() {
        try {
            this.processingParallelizer.getMainWindowController().getMainWindowModel().clearEnvironment();
            this.processingParallelizer.getMainWindowController().getMainWindowView().file.setEnabled(true);
            this.processingParallelizer.getMainWindowController().getMainWindowView().help.setEnabled(true);
            JOptionPane.showMessageDialog(new JFrame(),
                this.processingParallelizer.getMainWindowController().getLanguage().getProperty("descriptionProcessFolderFailure") + 
                this.processingParallelizer.getMainWindowController().getLanguage().getProperty("messageProcessFolderFailure"), 
                this.processingParallelizer.getMainWindowController().getLanguage().getProperty("titleProcessFolderFailure"), 
                JOptionPane.INFORMATION_MESSAGE
            );
        } catch(Exception e) {
            System.out.println("Error: ProcessFolderController - finalizeProccessForFailure - " + e.getMessage());
        }
    }
    
    public void finalizeProcessForSuccess() {
        try {
            this.processingParallelizer.getMainWindowController().getMainWindowModel().setAmbitoActual(ProjectProperties.FILE);
            this.processingParallelizer.getMainWindowController().getMainWindowModel().setPathFile(pathFileInit);
        } catch (Exception ex) {
            System.out.println("Error: ProcessFolderController - finalizeProcessForSuccess - " + ex.getMessage());
        }
    }    

    public FileModel extractionOfInformation(File fileSeleccionado, String treePath) {
        FileModel ambArchivoModelAntlr = new FileModel();
        Java6Lex lexer;
        try {
            String codigo = FilesUtil.readFile(fileSeleccionado.getPath());
            this.updateMessage(this.processingParallelizer.getMainWindowController().getLanguage().getProperty("running") + "...");
            this.updateProgress(1.0 / NUM_STEP, 1);
            codigo = FilesUtil.deleteComments(codigo);
            codigo = Roaster.format(codigo);
            lexer = new Java6Lex(new ANTLRStringStream(codigo));
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            tokens.fill();
            for (Object o : tokens.getTokens()) {
                CommonToken token = (CommonToken) o;
            }
            this.updateProgress(2.0 / NUM_STEP, 1);
            Java6Parse parser = new Java6Parse(tokens);
            parser.compilationUnit(ambArchivoModelAntlr);
            this.updateProgress(3.0 / NUM_STEP, 1);            
            ambArchivoModelAntlr.setName(fileSeleccionado.getName());
            ambArchivoModelAntlr.setTreePath(treePath);
            ambArchivoModelAntlr.setPath(fileSeleccionado.getPath());
        } catch (Exception ex) {
            if (ex.toString().contains("interfaceIgnore")) {
                this.ignoreFound = true;
                this.descriptionFailFound = this.processingParallelizer.getMainWindowController().getLanguage().getProperty("interfaceIgnore");
            } else if (ex.toString().contains("enumIgnore")) {
                this.ignoreFound = true;
                this.descriptionFailFound = this.processingParallelizer.getMainWindowController().getLanguage().getProperty("enumIgnore");
            } else {
                this.errorFound = true;
                this.descriptionFailFound = ex.toString();
                System.out.println("Error:  MainWindowController - extractionOfInformation - " + fileSeleccionado.getName() + ": " + ex.toString());
            }
        }
        return ambArchivoModelAntlr;
    }
}