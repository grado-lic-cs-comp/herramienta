package parallelizer;

import controllers.MainWindowController;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;
import models.TreeDirectoryModel;
import models.TreeValueModel;
import utils.FilesUtil;

/**
 * Manejador del Procesador de Archivos
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ProcessingParallelizer implements WindowListener, ActionListener {

    private MainWindowController mainWindowController;
    private File directory;
    private DefaultMutableTreeNode root;   
    private ArrayList<TreeValueModel> listPathJavaFile;
    private final JFXPanel fxPanel;
    private Scene scene;
    private TableView<FileProcess> table;
    private long startTime;
    private ListDialog dialog;
    ExecutorService executor;
    
    public ProcessingParallelizer(MainWindowController mainWindowController) {
        this.fxPanel = new JFXPanel();
        this.mainWindowController = mainWindowController;
        this.listPathJavaFile = new ArrayList<TreeValueModel>();
        this.table = new TableView<FileProcess>();
        this.dialog = new ListDialog(this.mainWindowController.getMainWindowView(), false);
        this.dialog.getPnlContent().add(fxPanel, BorderLayout.CENTER);
    }
    
    public void setConfigurationDialog() {
        dialog.addWindowListener(this);
        dialog.getBtnAccept().addActionListener(this);
        dialog.setSize(1100, 600);
    }

    public void init() {
        try {
            this.mainWindowController.clearAnalysis();
            this.mainWindowController.getMainWindowModel().updateFilesActiveAll();
            this.createListFileJava();
            this.startTime = 0;
            this.loadDialog();
            this.parallelize();
            this.showDialog();
            FilesProcessTask.clearConters();
        } catch(Exception e) {
            System.out.println("Error: ProcessFolderController - init - " + e.getMessage());
        }    
    }
    
    public void createListFileJava() {
        this.listPathJavaFile = new ArrayList<TreeValueModel>();
        DefaultMutableTreeNode rootFake = new DefaultMutableTreeNode(new TreeDirectoryModel(this.directory.getName(), this.directory.getPath()));
        File[] listFiles = this.directory.listFiles();
        for (File file : listFiles) {
            this.createFile(rootFake, file);
        }   
    }
    
    public void createFile(DefaultMutableTreeNode node, File f) {
        if(!f.isDirectory()) {
            if (FilesUtil.getExtensionOfFile(f).equals("java")) {
                DefaultMutableTreeNode child = new DefaultMutableTreeNode(new TreeDirectoryModel(f.getName(), f.getPath()));
                String treePath = "";
                for (Object element : node.getPath()) {
                    treePath+= File.separator + element.toString();
                }
                this.listPathJavaFile.add(new TreeValueModel(f.getPath(), treePath));
                node.add(child);
            }
        } else {
            if (FilesUtil.getJavaFilesCount(f) != 0) {
                DefaultMutableTreeNode child = new DefaultMutableTreeNode(new TreeDirectoryModel(f.getName(), f.getPath()));
                node.add(child);
                File[] listFiles = f.listFiles();
                for (File file : listFiles) {
                    this.createFile(child, file);
                }
            }  
        }
    }
    
    public void loadDialog() {
        this.table = this.createTable();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                scene = createScene();
                initFX(fxPanel);
            }
        });
        Platform.setImplicitExit(false);
    }

    private void initFX(JFXPanel fxPanel) {
        // This method is invoked on the JavaFX thread
        fxPanel.setScene(this.scene);
        this.dialog.getLblMessage().setVisible(false);
        dialog.getPnlContent().add(fxPanel, BorderLayout.CENTER);   
    } 
    
    public Scene createScene(){
        AnchorPane anchorPane = new AnchorPane();
        //Set Layout Constraint
        AnchorPane.setTopAnchor(table, 0.0);
        AnchorPane.setBottomAnchor(table, 0.2);
        AnchorPane.setLeftAnchor(table, 0.0);
        AnchorPane.setRightAnchor(table, 0.0);
        //Add WebView to AnchorPane
        anchorPane.getChildren().add(table);
        Scene newscene = new Scene(anchorPane, 1100, 600);
        return newscene;    
    }
    
    public TableView<FileProcess> createTable() {
        TableView newTable = new TableView<FileProcess>();

        TableColumn pathCol = new TableColumn(this.mainWindowController.getLanguage().getProperty("path"));
        pathCol.setCellValueFactory(new PropertyValueFactory<FileProcess, String>("path"));
        pathCol.setPrefWidth(600);

        TableColumn<FileProcess, Double> progressCol = new TableColumn(this.mainWindowController.getLanguage().getProperty("progress"));
        progressCol.setCellValueFactory(new PropertyValueFactory<FileProcess, Double>("progress"));
        progressCol.setCellFactory(ProgressBarTableCell.<FileProcess>forTableColumn());
        progressCol.setPrefWidth(200);

        TableColumn<FileProcess, String> statusCol = new TableColumn(this.mainWindowController.getLanguage().getProperty("status"));
        statusCol.setCellValueFactory(new PropertyValueFactory<FileProcess, String>("message"));
        statusCol.setPrefWidth(300);

        newTable.setItems(this.getFiles());
        newTable.getColumns().addAll(pathCol, progressCol, statusCol);
        return newTable;
    }
     
    public void createTree() {
        this.root = new DefaultMutableTreeNode(new TreeDirectoryModel(this.directory.getName(), this.directory.getPath()));
        this.mainWindowController.getTreeDirectoryController().setTree(new JTree(this.root));
        this.mainWindowController.getTreeDirectoryController().getTree().getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        this.mainWindowController.getTreeDirectoryController().getTree().addTreeSelectionListener(this.mainWindowController.getTreeDirectoryController());
        this.mainWindowController.getTreeDirectoryController().getTreeDirectoryView().getTreeDirectoryScrollPane().setViewportView(this.mainWindowController.getTreeDirectoryController().getTree());
        File[] listFiles = this.directory.listFiles();
        for (File file : listFiles) {
            this.createNode(this.root, file);
        }    
    }

    public void createNode(DefaultMutableTreeNode node, File f) {
        if(!f.isDirectory()) {
            if (FilesUtil.getExtensionOfFile(f).equals("java") && (checkContainListJava(f.getPath())!= -1)) {
                DefaultMutableTreeNode child = new DefaultMutableTreeNode(new TreeDirectoryModel(f.getName(), f.getPath()));
                String path = "";
                for (Object element : node.getPath()) {
                    path+= File.separator + element.toString();
                }
                String treePath = path + File.separator + FilesUtil.getNameWithoutExtension(f.getName(), "java");
                node.add(child);
            }
        } else {
            if (FilesUtil.getJavaFilesCount(f) != 0 && this.isParent(f.getPath())) {
                DefaultMutableTreeNode child = new DefaultMutableTreeNode(new TreeDirectoryModel(f.getName(), f.getPath()));
                node.add(child);
                File[] listFiles = f.listFiles();
                for (File file : listFiles) {
                    this.createNode(child, file);
                }
            }  
        }
    }
    
    public int checkContainListJava(String path) {
        for (int i = 0; i < listPathJavaFile.size(); i++) {
            if (listPathJavaFile.get(i).getPath().equals(path)) {
                return i;
            }
        }
        return -1;
    }

    public boolean isParent(String pathFolder) {
        for (int i = 0; i < listPathJavaFile.size(); i++) {
            if (listPathJavaFile.get(i).getPath().startsWith(pathFolder)) {
                return true;
            }
        }
        return false;
    }
    
    public void parallelize() {
        try {
            this.startTime = System.currentTimeMillis();
            this.executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(), (Runnable r) -> {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            });
            table.getItems().stream().forEach((task) -> {
                this.executor.execute(task);
            });
        } catch (Exception e) {
        }
    }
    
    public void setLanguage() {
        dialog.setTitle(this.mainWindowController.getLanguage().getProperty("titleProcessingFiles") + " - " + this.mainWindowController.getNameProject());
        dialog.getBtnAccept().setText(this.mainWindowController.getLanguage().getProperty("continueButton"));
        dialog.getLblMessage().setText(this.mainWindowController.getLanguage().getProperty("lblWait"));
    }
    
    public void showDialog() {
        this.setLanguage();
        this.dialog.getLblMessage().setVisible(true);
        this.dialog.setLocationRelativeTo(this.mainWindowController.getMainWindowView());
        this.dialog.pack();
        this.dialog.setResizable(false);
        this.mainWindowController.getMainWindowView().setEnabled(false);
        this.dialog.toFront();
        this.dialog.getBtnAccept().setEnabled(false);
        this.dialog.getLblArrow().setVisible(false);
        table.setVisible(true);
        this.dialog.setVisible(true);         
    }    
        
    public ObservableList<FileProcess> getFiles() {
        ObservableList<FileProcess> files = FXCollections.observableArrayList();
        FilesProcessTask.clearConters();
        this.listPathJavaFile.stream().forEach((pathJavaFile) -> {
            File file = new File(pathJavaFile.getPath());
            files.add(new FileProcess(file, pathJavaFile.getTreePath(), this));
        });
        return files;
    }
    
    public MainWindowController getMainWindowController() {
        return mainWindowController;
    }

    public long getStartTime() {
        return startTime;
    }

    @Override
    public void windowOpened(WindowEvent e) {
        this.mainWindowController.getMainWindowView().toFront();
        this.mainWindowController.getMainWindowView().setEnabled(false);
    }

    @Override
    public void windowClosing(WindowEvent e) {
        this.executor.shutdownNow();
    }

    @Override
    public void windowClosed(WindowEvent e) {
        this.executor.shutdownNow();
        this.mainWindowController.getMainWindowView().toFront();                    
        this.mainWindowController.getMainWindowView().setEnabled(true);
        this.mainWindowController.getBarController().initView();
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
        this.dialog.toFront();
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    public ListDialog getDialog() {
        return dialog;
    }

    public DefaultMutableTreeNode getRoot() {
        return root;
    }

    public ArrayList<TreeValueModel> getListPathJavaFile() {
        return listPathJavaFile;
    }

    public File getDirectory() {
        return directory;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object source = e.getSource();
            if (source instanceof JButton) {
                switch (((JComponent) e.getSource()).getName()) {
                    case "btnAccept":
                        dialog.dispose();
                        this.mainWindowController.getMainWindowView().setEnabled(true);
                        this.mainWindowController.getMainWindowView().toFront();
                        this.mainWindowController.getBarController().setEnabledToolBarItems(true, true, false, true, true);
                        this.mainWindowController.showInfoInit();
                        break;
                }
            }
        } catch (Exception ex) {
            System.out.println("Error: ProcessingParallelizer - actionPerformed - " + ex.getMessage());
        }
    }

    public void setDirectory(File directory) {
        this.directory = directory;
    }
}