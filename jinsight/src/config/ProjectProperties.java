package config;

/**
 * Propiedades del proyecto
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ProjectProperties {
    public static final String HOST = ":3306";
    public static final String USER = "root";
    public static final String PASSWORD = "almirante";
    public static final String DATABASE = "jinsight";
    public static final String FILE = "FILE";
    public static final String ARCHIVO = "ARCHIVO";
    public static final String CLASS = "CLASS";
    public static final String CLASE = "CLASE";
    public static final String METHOD = "METHOD";
    public static final String METODO = "METODO";
    public static final String PARAMETROS = "PARAMETROS";
    public static final String ALUMN = "Arnaldo Raúl Ceballos";
    public static final String REPORT = "https://drive.google.com/file/d/1MSfY_X69RDvHjBK3bN5k_i8xckTAUxzT/view";
}
