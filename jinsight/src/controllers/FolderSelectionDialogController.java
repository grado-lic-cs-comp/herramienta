package controllers;

import parallelizer.ProcessingParallelizer;
import dialogs.FolderSelectionDialog;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.sql.SQLException;
import java.util.Locale;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.border.TitledBorder;
import utils.FilesUtil;
import utils.ManagerMySQLDAO;

/**
 * Controlador del diálogo FolderSelectionDialog
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class FolderSelectionDialogController implements WindowListener, ActionListener, MouseListener {
    private MainWindowController mainWindowController;
    private FolderSelectionDialog folderSelectionDialog;
    private String rutaCarpetaDestino;
    private String rutaCarpetaFuente;
    private boolean flagStartFolderProcessing;
    ProcessingParallelizer processingParallelizer;

    public FolderSelectionDialogController(MainWindowController mainWindowController, FolderSelectionDialog folderSelectionDialog) {
        this.mainWindowController = mainWindowController;
        this.folderSelectionDialog = folderSelectionDialog;
        processingParallelizer = new ProcessingParallelizer(this.mainWindowController);
        processingParallelizer.setConfigurationDialog();
        this.flagStartFolderProcessing = false;
    }
    
    public void setLanguage() {
        try {
            TitledBorder title = BorderFactory.createTitledBorder(this.mainWindowController.getLanguage().getProperty("connectionPnlBorder"));
            this.folderSelectionDialog.getConnectionPnl().setBorder(title);
            this.folderSelectionDialog.getHostsListLbl().setText(this.mainWindowController.getLanguage().getProperty("hostListLabel"));
            this.folderSelectionDialog.getServerLbl().setText(this.mainWindowController.getLanguage().getProperty("serverLabel"));
            this.folderSelectionDialog.getPortLbl().setText(this.mainWindowController.getLanguage().getProperty("portLabel"));
            this.folderSelectionDialog.getUserLbl().setText(this.mainWindowController.getLanguage().getProperty("userLabel"));
            this.folderSelectionDialog.getPassLbl().setText(this.mainWindowController.getLanguage().getProperty("passLabel"));
            title = BorderFactory.createTitledBorder(this.mainWindowController.getLanguage().getProperty("directoriesPnlBorder"));
            this.folderSelectionDialog.getDirectoriesPnl().setBorder(title);
            this.folderSelectionDialog.getDescriptionSourceFolder().setText(this.mainWindowController.getLanguage().getProperty("sourceFolderDialogLabel"));
            this.folderSelectionDialog.getDescriptionDestinationFolder().setText(this.mainWindowController.getLanguage().getProperty("destinationFolderDialogLabel"));
            this.folderSelectionDialog.getAcceptBtn().setText(this.mainWindowController.getLanguage().getProperty("acceptButton"));
            this.folderSelectionDialog.getCancelBtn().setText(this.mainWindowController.getLanguage().getProperty("cancelButton"));
        } catch (Exception e) {
            System.out.println("Error: FolderSelectionDialogController - setLanguage " + e.toString());
        }
    }
        
    public void showDialog() {
        this.setLanguage();
        folderSelectionDialog.setTitle(this.mainWindowController.getLanguage().getProperty("menuOpenFolder"));
        this.folderSelectionDialog.setLocationRelativeTo(this.mainWindowController.getMainWindowView());
        folderSelectionDialog.pack();
        folderSelectionDialog.setResizable(false);
        this.mainWindowController.getMainWindowView().setEnabled(false);
        folderSelectionDialog.setVisible(true);    
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
        this.mainWindowController.getMainWindowView().toFront();
        this.mainWindowController.getMainWindowView().setEnabled(true);
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object source = e.getSource();
            if (source instanceof JButton || source instanceof JComboBox) {
                switch (((JComponent)e.getSource()).getName()) {
                    case "hostsList":
                        JOptionPane.showMessageDialog(
                            new JFrame(),
                            this.mainWindowController.getLanguage().getProperty("extension") + "\n" + 
                            this.mainWindowController.getLanguage().getProperty("featureToAdd"), 
                            this.mainWindowController.getLanguage().getProperty("futureWork"), 
                            JOptionPane.INFORMATION_MESSAGE
                        );
                        break;
                    case "addSourceFolderBtn":
                        this.seleccionarCarpetaFuente();
                        break;
                    case "addDestinationFolderBtn":
                        this.dialogFolderselectedFolderDestination();
                        break;
                    case "acceptBtn":
                        this.controlSelectionFolders();
                        break;
                    case "cancelBtn":
                        this.cerrarDialogoSeleccionCarpeta();
                        break;
                    case "sourceInfoBtn":
                        String titleDialogSource = "description";
                        String titleSource = this.mainWindowController.getLanguage().getProperty("titleSourceInfo");
                        String messageSource = this.mainWindowController.getLanguage().getProperty("messageSourceInfo");
                        this.showSuggestions(titleDialogSource, titleSource, messageSource);
                        break;
                    case "destinationInfoBtn":
                        String titleDialogDestination = "description";
                        String titleDestination = this.mainWindowController.getLanguage().getProperty("titleDestinationInfo");
                        String messageDestination = this.mainWindowController.getLanguage().getProperty("messageDestinationInfo");
                        this.showSuggestions(titleDialogDestination, titleDestination, messageDestination);
                        break;
                }
            }
        } catch(Exception ex) {
            System.out.println("Error: FolderSelectionDialogController - actionPerformed " + ex.getMessage());
        }
    }

    public void showSuggestions(String titleDialog, String title, String message) {
        this.mainWindowController.getMessageDialog().setTitle(this.mainWindowController.getLanguage().getProperty(titleDialog));
        this.mainWindowController.getMessageDialog().getLblTitle().setText(title);
        this.mainWindowController.getMessageDialog().getTxtAreaContent().setText(message);
        this.mainWindowController.getMessageDialog().getIcon().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/64_info.png")));
        this.mainWindowController.getMessageDialogController().showDialog();
    }


    public void seleccionarCarpetaFuente() {        
        try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle(this.mainWindowController.getLanguage().getProperty("sourceFolderDialogTitle"));
            switch(this.mainWindowController.getLanguage().getActualLanguage()) {
                case "ingles":
                    fileChooser.setLocale(Locale.ENGLISH);
                    break;
                case "espanol":
                    fileChooser.setLocale(Locale.getDefault());
                    break;
            }
            fileChooser.updateUI();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);                    
            fileChooser.setMultiSelectionEnabled(false);
            this.mainWindowController.setButtonCursor(fileChooser, new Cursor(Cursor.HAND_CURSOR));
            int ret = fileChooser.showDialog(null, this.mainWindowController.getLanguage().getProperty("acceptButton"));
            if (ret == JFileChooser.APPROVE_OPTION) {
                this.setRutaCarpetaFuente(fileChooser.getSelectedFile().getPath());
                this.mainWindowController.setFolderSelectedParent(fileChooser.getSelectedFile().getParent());
            }
        } catch(Exception e) {
            System.out.println("Error: FolderSelectionDialogController - seleccionarCarpetaFuente - " + e);
        }
    }

    public void dialogFolderselectedFolderDestination() {        
        try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle(this.mainWindowController.getLanguage().getProperty("destinationFolderFolderDialog"));
            switch(this.mainWindowController.getLanguage().getActualLanguage()) {
                case "ingles":
                    fileChooser.setLocale(Locale.ENGLISH);
                    break;
                case "espanol":
                    fileChooser.setLocale(Locale.getDefault());
                    break;
            }
            fileChooser.updateUI();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            fileChooser.setMultiSelectionEnabled(false);
            this.mainWindowController.setButtonCursor(fileChooser, new Cursor(Cursor.HAND_CURSOR));
            int ret= fileChooser.showDialog(null, this.mainWindowController.getLanguage().getProperty("acceptButton"));
            if (ret == JFileChooser.APPROVE_OPTION) {
                this.setDestinationFolder(fileChooser.getSelectedFile().getPath());
            }
        } catch(Exception e) {
            System.out.println("Error en FolderSelectionDialogController - dialogFolderselectedFolderDestination - " + e);
        }
    }

    public void controlSelectionFolders() {
        try {
            ManagerMySQLDAO.setInstance(null);
            this.setEmptyCredintials();
            String serverValue = this.folderSelectionDialog.getServer().getText();
            if (serverValue.length() == 0) {
                JOptionPane.showMessageDialog(
                    new JFrame(),
                    this.mainWindowController.getLanguage().getProperty("descriptionProcessFolderFailure") + "\n" + 
                    this.mainWindowController.getLanguage().getProperty("serverEmpty"), 
                    this.mainWindowController.getLanguage().getProperty("control"), 
                    JOptionPane.INFORMATION_MESSAGE
                );
                return;                
            } else {
                ManagerMySQLDAO.setHost(serverValue);
            }

            String portValue = this.folderSelectionDialog.getPort().getText();
            if (portValue.length() == 0) {
                JOptionPane.showMessageDialog(
                    new JFrame(),
                    this.mainWindowController.getLanguage().getProperty("descriptionProcessFolderFailure") + "\n" + 
                    this.mainWindowController.getLanguage().getProperty("portEmpty"), 
                    this.mainWindowController.getLanguage().getProperty("control"), 
                    JOptionPane.INFORMATION_MESSAGE
                );
                return;
            } else {
                ManagerMySQLDAO.setPort(portValue);
            }

            String userValue = this.folderSelectionDialog.getUser().getText();
            if (userValue.length() == 0) {
                JOptionPane.showMessageDialog(
                    new JFrame(),
                    this.mainWindowController.getLanguage().getProperty("descriptionProcessFolderFailure") + "\n" + 
                    this.mainWindowController.getLanguage().getProperty("userEmpty"), 
                    this.mainWindowController.getLanguage().getProperty("control"), 
                    JOptionPane.INFORMATION_MESSAGE
                );
                return;
            } else {
                ManagerMySQLDAO.setUser(userValue);
            }
            ManagerMySQLDAO.setPass(new String(this.folderSelectionDialog.getPass().getPassword()));

            String pathSourceFolder = this.folderSelectionDialog.getPathSourceFolder().getText();
            if (pathSourceFolder.length() == 0) {
                JOptionPane.showMessageDialog(
                    null,
                    this.mainWindowController.getLanguage().getProperty("sourceFolderSelectionError"), 
                    this.mainWindowController.getLanguage().getProperty("errorFolderDialogTitle"),
                    JOptionPane.INFORMATION_MESSAGE
                );
                return;
            }

            String pathDestinationFolder = this.folderSelectionDialog.getPathDestinationFolder().getText();
            if (pathDestinationFolder.length() == 0) {
                JOptionPane.showMessageDialog(
                    null, 
                    this.mainWindowController.getLanguage().getProperty("destinationFolderAndFilesSelectionError"),
                    this.mainWindowController.getLanguage().getProperty("errorFolderDialogTitle"), 
                    JOptionPane.INFORMATION_MESSAGE
                );
                return;
            }
            
            try {
                this.mainWindowController.getMainWindowModel().setConnectionMySQL(ManagerMySQLDAO.getInstance());
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(
                    new JFrame(),
                    this.mainWindowController.getLanguage().getProperty("descriptionProcessFolderFailure") + "\n" + 
                    this.mainWindowController.getLanguage().getProperty("connectionFail"), 
                    this.mainWindowController.getLanguage().getProperty("control"), 
                    JOptionPane.INFORMATION_MESSAGE
                );
                return;                
            }
            
            File newFile = new File(pathSourceFolder);
            if (FilesUtil.getJavaFilesCount(newFile) == 0) {
                JOptionPane.showMessageDialog(
                    new JFrame(),
                    this.mainWindowController.getLanguage().getProperty("descriptionProcessFolderFailure") + "\n" + 
                    this.mainWindowController.getLanguage().getProperty("messageProcessFolderFailure"), 
                    this.mainWindowController.getLanguage().getProperty("titleProcessFolderFailure"), 
                    JOptionPane.INFORMATION_MESSAGE
                );            
            } else {
                this.mainWindowController.setNameProject(newFile.getName());
                this.cerrarDialogoSeleccionCarpeta();
                this.mainWindowController.getMainWindowView().getMainPanel().setVisible(false);
                this.mainWindowController.getMainWindowModel().clearEnvironment();
                this.mainWindowController.getBarController().cleanIndicators();
                this.mainWindowController.getMainWindowModel().updateFilesActiveAll();
                this.mainWindowController.getBarController().setEnabledMenuBarItems(false, false, false, false);
                this.mainWindowController.enableComponents(this.mainWindowController.getMainWindowView().getToolBar() ,false);
                this.startFolderProcessing(newFile);
            }
        } catch(Exception e) {
            System.out.println("Error: FolderSelectionDialogController - controlSelectionFolders " + e);        
        }
    }
    
    public void setEmptyCredintials() {
        ManagerMySQLDAO.setHost("");
        ManagerMySQLDAO.setPort("");
        ManagerMySQLDAO.setUser("");
        ManagerMySQLDAO.setPass("");
    }

    public void cerrarDialogoSeleccionCarpeta() {
        try {
            this.folderSelectionDialog.getPathSourceFolder().setText("");
            this.folderSelectionDialog.getPathDestinationFolder().setText("");
            this.folderSelectionDialog.dispose();
            this.mainWindowController.getMainWindowView().toFront();

        } catch(Exception e) {
            System.out.println("Error: FolderSelectionDialogController - cerrarDialogoSeleccionCarpeta - " + e);
        }
    }

    public void setRutaCarpetaFuente(String rutaCarpetaFuente) {
        this.rutaCarpetaFuente = rutaCarpetaFuente + File.separator;
        this.folderSelectionDialog.getPathSourceFolder().setText(this.rutaCarpetaFuente);
    }

    public void setDestinationFolder(String rutaCarpetaDestino) {
        this.rutaCarpetaDestino = rutaCarpetaDestino + File.separator;
        this.folderSelectionDialog.getPathDestinationFolder().setText(this.rutaCarpetaDestino);
    }

    public String getRutaCarpetaDestino() {
        return rutaCarpetaDestino;
    }

    public String getRutaCarpetaFuente() {
        return rutaCarpetaFuente;
    }
        
    public boolean isFlagStartFolderProcessing() {
        return flagStartFolderProcessing;
    }

    public void setFlagStartFolderProcessing(boolean flag) {
        this.flagStartFolderProcessing = flag;
    }    

    public void startFolderProcessing(File file) {
        try {
            this.setFlagStartFolderProcessing(true);
            processingParallelizer.setDirectory(file);
            processingParallelizer.init();
    } catch (Exception e) {
            System.out.println("Error: FolderSelectionDialogController - startFolderProcessing - " + e);
        }
    }    

    public MainWindowController getMainWindowController() {
        return mainWindowController;
    }

    public void setMainWindowController(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
    }

    public FolderSelectionDialog getFolderSelectionDialog() {
        return folderSelectionDialog;
    }

    public void setFolderSelectionDialog(FolderSelectionDialog folderSelectionDialog) {
        this.folderSelectionDialog = folderSelectionDialog;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        try {
            switch (((JComponent)e.getSource()).getName()) {
                case "pathSourceFolder":
                    this.seleccionarCarpetaFuente();
                    break;
                case "pathDestinationFolder":
                    this.dialogFolderselectedFolderDestination();
                    break;
            }                  
        } catch(Exception ex) {
            System.out.println("Error: FolderSelectionDialogController - actionPerformed " + ex.getMessage());
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}