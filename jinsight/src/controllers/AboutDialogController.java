package controllers;

import dialogs.AboutDialog;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

/**
 * Controlador del diálogo About
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class AboutDialogController implements ActionListener, HyperlinkListener, WindowListener {
    private MainWindowController mainController = null;
    private AboutDialog aboutDialog;
    
    public AboutDialogController(MainWindowController mainController, AboutDialog aboutDialog) {
        this.mainController = mainController;
        this.aboutDialog = aboutDialog;
    }
    
    public void setLanguageAboutDialog() {
        this.aboutDialog.setTitle(this.mainController.getLanguage().getProperty("aboutDialogTitle"));
        this.aboutDialog.getAboutInformation().setText(this.mainController.getLanguage().getProperty("aboutInformation"));
        this.aboutDialog.getBtnClose().setText(this.mainController.getLanguage().getProperty("closeButton"));
    }

    public void showDialog() {
        try {
            this.setLanguageAboutDialog();
            this.aboutDialog.setLocationRelativeTo(this.mainController.getMainWindowView());
            this.aboutDialog.pack();
            this.aboutDialog.setResizable(false);
            this.mainController.getMainWindowView().setEnabled(false);
            aboutDialog.setVisible(true);            
        } catch (Exception e) {
            System.out.println("Error: AboutDialogController - showDialog - " + e.getMessage());
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object source = e.getSource();
            if (source instanceof JButton) {
                switch (((JComponent)e.getSource()).getName()) {
                    case "btnClose": 
                        aboutDialog.dispose();
                        this.mainController.getMainWindowView().setEnabled(true);
                        this.mainController.getMainWindowView().toFront();
                        break;
                }
            }
        } catch(Exception ex) {
            System.out.println("Error: AboutDialogController - actionPerformed - " + ex.getMessage());
        }
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        try {
            if ( e.getEventType() == HyperlinkEvent.EventType.ACTIVATED ) {
                switch(((JComponent)e.getSource()).getName()) {
                    case "aboutInformation":  
                        if (Desktop.isDesktopSupported()) {
                            try {
                                Desktop.getDesktop().browse(e.getURL().toURI());
                            } catch (IOException | URISyntaxException e1) {
                                System.out.println("No sorpota URI");
                            }
                        }
                        break;
                }
            }
        } catch(Exception ex) {
            System.out.println("Error: AboutDialogController - hyperlinkUpdate - " + ex);
        }
    }    

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
        this.mainController.getMainWindowView().toFront();
        this.mainController.getMainWindowView().setEnabled(true);
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }
}