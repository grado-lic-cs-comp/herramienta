package controllers;

import config.ProjectProperties;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.ToolTipManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;
import models.PackageMetricValueModel;
import models.TreeDirectoryModel;
import views.TreeDirectoryView;

/**
 * Controlador de la vista TreeDirectoryView
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class TreeDirectoryController extends DefaultTreeCellRenderer implements TreeSelectionListener, ActionListener {
    private MainWindowController mainWindowController;
    private TreeDirectoryModel treeDirectoryModel;
    private TreeDirectoryView treeDirectoryView;
    private JTree tree;
    private DefaultTreeCellRenderer renderer;
    private Object[][] metricsPackages = null;
    private boolean checkInit;
    
    public TreeDirectoryController(TreeDirectoryModel treeDirectoryModel, TreeDirectoryView treeDirectoryView, MainWindowController mainWindowController) {
        this.treeDirectoryModel = treeDirectoryModel;
        this.mainWindowController = mainWindowController;
        this.treeDirectoryView = treeDirectoryView;
        this.renderer = new DefaultTreeCellRenderer();
        this.checkInit = false;
    }
    
    public void setLanguage() {
        try {
            TitledBorder title = BorderFactory.createTitledBorder(this.mainWindowController.getLanguage().getProperty("titleTreeDirectoryArea"));
            this.treeDirectoryView.setBorder(title);
            this.treeDirectoryView.getExpandBtn().setToolTipText(this.mainWindowController.getLanguage().getProperty("expandBtn"));
            this.treeDirectoryView.getCollapseBtn().setToolTipText(this.mainWindowController.getLanguage().getProperty("collapseBtn"));
            this.treeDirectoryView.getStatisticsBtn().setToolTipText(this.mainWindowController.getLanguage().getProperty("statisticsBtn"));
            this.updateActualFileLoc();
        } catch (Exception e) {
                System.out.println("Error: TreeDirectoryController - setLanguage - " + e.toString());
        }
    }
    
    public void initView() {
        this.expandAll();
        this.checkInit = true;
        DefaultMutableTreeNode firstLeaf = ((DefaultMutableTreeNode)this.getTree().getModel().getRoot()).getFirstLeaf();
        this.getTree().setSelectionPath(new TreePath(firstLeaf.getPath()));
        this.metricsPackages = this.getValuesTable();
        this.checkInit = false;
    }
    
    public MainWindowController getMainWindowController() {
        return mainWindowController;
    }

    public void setMainWindowController(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
    }

    public JTree getTree() {
        return tree;
    }

    public void setTree(JTree tree) {
        this.tree = tree;
        this.tree.setCellRenderer(this);
        ToolTipManager.sharedInstance().registerComponent(this.tree);
        DefaultTreeCellRenderer render= (DefaultTreeCellRenderer)this.tree.getCellRenderer();
        render.setLeafIcon(new javax.swing.ImageIcon(getClass().getResource("/images/16_file.png")));
        render.setOpenIcon(new javax.swing.ImageIcon(getClass().getResource("/images/16_folder-open.png")));
        render.setClosedIcon(new javax.swing.ImageIcon(getClass().getResource("/images/16_folder.png")));
    }    
    
    @Override
    public void valueChanged(TreeSelectionEvent e) {
        String value = "";
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)this.tree.getLastSelectedPathComponent();
        if (node == null) return;
        if (node.isLeaf()) {
            try {
                TreePath treepath = e.getPath();
                Object elements[] = treepath.getPath();
                int i , n;
                for (i = 0, n = elements.length; i < n-1; i++) {
                    value+= elements[i] + File.separator;
                }   
                value+= elements[i];
                String fileSelectedTree = this.mainWindowController.getFolderSelectedParent() + File.separator + value;
                this.mainWindowController.getMainWindowModel().clearEnvironment();
                this.mainWindowController.getMainWindowModel().setAmbitoActual(ProjectProperties.FILE);
                this.mainWindowController.getMainWindowModel().setPathFile(fileSelectedTree);
                this.mainWindowController.showInfo();
            } catch (Exception ex) {
                System.out.println("Error: TreeDirectoryController - valueChanged - " + ex.toString());
            }
        }
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        if (isTreeDirectoryModal(value)) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
            TreeDirectoryModel nodeInfo = (TreeDirectoryModel)(node.getUserObject());
            setToolTipText(nodeInfo.getPath());
            tree.addMouseMotionListener(new MouseMotionAdapter() {
                @Override
                public void mouseMoved(MouseEvent e) {
                    int x = (int) e.getPoint().getX();
                    int y = (int) e.getPoint().getY();
                    TreePath path = tree.getPathForLocation(x, y);
                    if (path == null) {
                        tree.setCursor(Cursor.getDefaultCursor());
                    } else {
                        tree.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                    }
                }
            });            
        } else {
            setToolTipText(null);
        }
        return this;
    }

    protected boolean isTreeDirectoryModal(Object value) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
        return node.getUserObject() instanceof TreeDirectoryModel;
    }

    public TreeDirectoryView getTreeDirectoryView() {
        return treeDirectoryView;
    }

    public void setTreeDirectoryView(TreeDirectoryView treeDirectoryView) {
        this.treeDirectoryView = treeDirectoryView;
    }
    
    public void expandAll() {
        int row = 0;
        while (row < this.tree.getRowCount()) {
            this.tree.expandRow(row);
            row++;
        }
    }

    public void collapseAll() {
        int row = this.tree.getRowCount() - 1;
        while (row >= 0) {
            this.tree.collapseRow(row);
            row--;
        }
    }

    public void loadMetricsPackages() {
        DefaultTableModel table = new DefaultTableModel(this.metricsPackages, this.getColumnsTable());
        this.mainWindowController.getTableDialog().getTable().setModel(table);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        ((DefaultTableCellRenderer)this.mainWindowController.getTableDialog().getTable().getTableHeader().getDefaultRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
        centerRenderer.setHorizontalAlignment( SwingConstants.CENTER );        
        for(int x = 1; x < this.getColumnsTable().length; x++){
            this.mainWindowController.getTableDialog().getTable().getColumnModel().getColumn(x).setCellRenderer( centerRenderer );
        }
        this.mainWindowController.getTableDialog().getLblTotalPackage().setText(this.mainWindowController.getLanguage().getProperty("totalPackage"));
        this.mainWindowController.getTableDialog().getLblValueTotalPackage().setText(Integer.toString(table.getRowCount()-1));
        this.mainWindowController.getTableDialog().getLblValueTotalPackage().setToolTipText(Integer.toString(table.getRowCount()-1));
    }

    public String[] getColumnsTable() {
        String[] columnNames = { 
            this.mainWindowController.getLanguage().getProperty("package_names"),
            this.mainWindowController.getLanguage().getProperty("files_size"),
            this.mainWindowController.getLanguage().getProperty("classes_size"),
            this.mainWindowController.getLanguage().getProperty("methods_size"),
            this.mainWindowController.getLanguage().getProperty("lines_size")            
        };
        return columnNames;
    }
    
    public Object[][] getValuesTable() {
        ArrayList<PackageMetricValueModel> packageMetricValueModel = this.treeDirectoryModel.getValuesPackagesMetrics();
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("totalPackages", 0);
        map.put("totalFiles", 0);
        map.put("totalClasses", 0);
        map.put("totalMethods", 0);
        map.put("totalLines", 0);
        Object[][] data = new Object[packageMetricValueModel.size()+1][5];
        for(int i = 0; i < packageMetricValueModel.size(); i++) {
            data[i] = new Object[]{
                packageMetricValueModel.get(i).getPackage_name(),
                packageMetricValueModel.get(i).getFile_size(),
                packageMetricValueModel.get(i).getClass_size(),
                packageMetricValueModel.get(i).getMethod_size(),
                packageMetricValueModel.get(i).getLine_size()
            };
            map.put("totalPackages", map.get("totalPackages") + 1);		
            map.put("totalFiles", map.get("totalFiles") + packageMetricValueModel.get(i).getFile_size());		
            map.put("totalClasses", map.get("totalClasses") + packageMetricValueModel.get(i).getClass_size());		
            map.put("totalMethods", map.get("totalMethods") + packageMetricValueModel.get(i).getMethod_size());
            map.put("totalLines", map.get("totalLines") + packageMetricValueModel.get(i).getLine_size());
        }
        data[packageMetricValueModel.size()] = new Object[]{
            "Total",
            map.get("totalFiles"),
            map.get("totalClasses"),
            map.get("totalMethods"),
            map.get("totalLines")
        };
        return data;
    }
        
    public void showStatistics() {
        this.mainWindowController.getTableDialog().setTitle(this.mainWindowController.getLanguage().getProperty("statisticsPackages"));
        this.mainWindowController.getTableDialog().getTitleLbl().setText(this.mainWindowController.getLanguage().getProperty("statisticsPackages"));
        this.loadMetricsPackages();
        this.mainWindowController.getTableDialogController().showDialog();
    }

    public void updateTotalLoc() {
        this.treeDirectoryView.getLblTotalLoc().setText(this.mainWindowController.getLanguage().getProperty("totalLoc") + treeDirectoryModel.getLocTotal());    
    }
    
    public void updateActualFileLoc() {
        try {
            int fileLoc = treeDirectoryModel.getLocFile(this.mainWindowController.getMainWindowModel().getPathFile());
            this.treeDirectoryView.getLblFileLoc().setText(this.mainWindowController.getLanguage().getProperty("file") + " LOC: " + fileLoc);
        } catch (Exception e) {
            System.out.println("Error: TreeDirectoryController - updateActualFileLoc - " + e.toString());
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object source = e.getSource();
            if (source instanceof JButton) {
                switch (((JComponent)e.getSource()).getName()) {
                    case "expand": 
                        this.expandAll();
                        break; 
                    case "collapse": 
                        this.collapseAll();
                        break; 
                    case "statistics": 
                        this.showStatistics();
                        break; 
                }
            }
        } catch(Exception ex) {
            System.out.println("Error: TreeDirectoryController - actionPerformed - " + ex.getMessage());
        }
    }
}