package controllers;

import com.macrofocus.treemap.Algorithm;
import com.macrofocus.treemap.AlgorithmFactory;
import com.macrofocus.treemap.DefaultTreeMapController;
import com.macrofocus.treemap.DefaultTreeMapField;
import com.macrofocus.treemap.TreeMap;
import config.ProjectProperties;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import models.WeightedElementsModel;
import models.MetricValueModel;
import models.ParameterModel;
import views.WeightedElementsView;

/**
 * Controlador de la vista WeightedElementsView
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class WeightedElementsController implements PropertyChangeListener, ActionListener {
    private WeightedElementsView weightedElementsView;
    private WeightedElementsModel weightedElementsModel;
    private MainWindowController mainWindowController;
    private TreeMap treeMap;
    private DefaultTreeMapController defaultTreeMapController;
    private Algorithm oldAlgorithmSelected;
    private DefaultTreeMapField oldColorSelected = null;
    private boolean isExpanded;
        
    public WeightedElementsController(models.WeightedElementsModel weightedElementsModel, WeightedElementsView weightedElementsView, MainWindowController mainController) {
        this.weightedElementsModel = weightedElementsModel;
        this.weightedElementsView = weightedElementsView;
        this.mainWindowController = mainController;
        this.defaultTreeMapController = new DefaultTreeMapController();
        this.oldAlgorithmSelected = AlgorithmFactory.SQUARIFIED;
        this.isExpanded = false;
    }    

    public void setLanguage() {
        TitledBorder title = BorderFactory.createTitledBorder(this.mainWindowController.getLanguage().getProperty("titleWeightedElementsArea"));
        this.weightedElementsView.setBorder(title);
        this.weightedElementsView.getLblColor().setText(this.mainWindowController.getLanguage().getProperty("colorWeightedElements"));
        this.weightedElementsView.getLblAlgorithm().setText(this.mainWindowController.getLanguage().getProperty("algorithmWeightedElements"));
        if (isExpanded) {
            this.weightedElementsView.getBtnExpandFile().setToolTipText(this.mainWindowController.getLanguage().getProperty("collapseArea"));
        } else {
            this.weightedElementsView.getBtnExpandFile().setToolTipText(this.mainWindowController.getLanguage().getProperty("expandArea"));
        }
        if (this.mainWindowController.getMainWindowModel().getAmbitoActual().equals(ProjectProperties.METHOD)) {
            this.show();
        }
    }
    
    public void init() {
        this.setModelAlgorithm();
    }
    
    public void show() {
        try {
            String pathFile;
            ArrayList<MetricValueModel> values = null;
            Object[][] data = null;
            switch(this.mainWindowController.getMainWindowModel().getAmbitoActual()) {
                case ProjectProperties.FILE:
                    pathFile = this.mainWindowController.getMainWindowModel().getPathFile();
                    values = this.weightedElementsModel.getValuesTreeMapFile(pathFile);
                    if (values.size() > 0) {
                        data = new Object[values.size()][2];
                        for(int i = 0; i < values.size(); i++) {
                            data[i] = new Object[]{
                                values.get(i).getNombre(),
                                values.get(i).getCantidad()
                            };
                        }
                    }
                    break;
                case ProjectProperties.CLASS:
                    pathFile = this.mainWindowController.getMainWindowModel().getPathFile();
                    String className = this.mainWindowController.getMainWindowModel().getClassName();
                    values = this.weightedElementsModel.getValuesTreeMapClass(pathFile, className);
                    if (values.size() > 0) {
                        data = new Object[values.size()][2];
                        for(int i = 0; i < values.size(); i++) {
                            data[i] = new Object[]{
                                values.get(i).getNombre(),
                                values.get(i).getCantidad()
                            };
                        }
                    }
                    break;
                case ProjectProperties.METHOD:
                    int method_id = this.mainWindowController.getMainWindowModel().getMethodId();
                    values = this.weightedElementsModel.getMethodLocalvariables(method_id);
                    if (values.size() > 0) {
                        data = new Object[values.size()][2];
                        for(int i = 0; i < values.size(); i++) {
                            data[i] = new Object[]{
                               values.get(i).getNombre(),
                                1
                            };
                        }
                    }
                    break;
            }
            if (values != null && values.size() > 0) {
                this.weightedElementsModel.setData(data);
                this.weightedElementsModel.loadTableModel();
                this.initView();
            } else {
                this.setEmptyWeightedElements();
            }
        } catch(Exception e) {
            System.out.println("Error: TreemapController - show - " + e.getMessage());
        }    
    }
        
    public void initView() {
        try {
            this.weightedElementsView.removeAll();
            this.treeMap = new TreeMap(this.weightedElementsModel.getTableModel());
            this.treeMap.setController(defaultTreeMapController);
            this.treeMap.setAlgorithm(oldAlgorithmSelected);
            this.weightedElementsView.getCmbBoxAlgorithm().setSelectedItem(oldAlgorithmSelected);
            this.treeMap.setSizeByName("Value");
            if (oldColorSelected != null) {
                this.treeMap.setColor(oldColorSelected.getIndex());
            } else {
                this.treeMap.setColor(0);
            }
            this.treeMap.setBackgroundByName("Name");
            this.treeMap.setLabels();
            this.treeMap.getModel().getSettings().getDefaultFieldSettings().setLabelingVerticalAlignment(SwingConstants.CENTER);
            this.treeMap.getModel().getSettings().getDefaultFieldSettings().setLabelingHorizontalAlignment(SwingConstants.CENTER);
            this.treeMap.getModel().addPropertyChangeListener(this);
            if (!this.mainWindowController.getMainWindowModel().getAmbitoActual().equals(ProjectProperties.METHOD)) {
                this.treeMap.setCursor(new Cursor(Cursor.HAND_CURSOR));            
            } else {
                this.treeMap.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));        
            }
            this.setModelsToolBar();
            this.setPopupMenu();
            this.weightedElementsView.getPnlGraphic().removeAll();
            this.weightedElementsView.getPnlGraphic().add(this.weightedElementsView.getToolBar(), BorderLayout.NORTH);
            this.weightedElementsView.getPnlGraphic().add(this.treeMap, BorderLayout.CENTER);
            this.weightedElementsView.add(this.weightedElementsView.getPnlGraphic(), BorderLayout.CENTER);
        } catch(Exception e){
            System.out.println("Error WeightedElementsController - initFileView - " + e.getMessage());
        }
    }
    
    public void setPopupMenu() {
        defaultTreeMapController.setPopupMenu(new JPopupMenu());
//        defaultTreeMapController.getPopupMenu().insert(new AbstractAction("File 1") {
//            public void actionPerformed(ActionEvent e) {
//                JOptionPane.showMessageDialog(new JPanel(), " File 1");
//            }
//        }, 0);
//        defaultTreeMapController.getPopupMenu().insert(new AbstractAction("File 3") {
//            public void actionPerformed(ActionEvent e) {
//                JOptionPane.showMessageDialog(new JPanel(), " File 3");
//            }
//        }, 2);
    }

    public void setModelAlgorithm() {
        this.weightedElementsView.getCmbBoxAlgorithm().removeAllItems();
        java.util.List<Algorithm> algorithms = AlgorithmFactory.getInstance().getAlgorithms();
        String[] a= {AlgorithmFactory.SQUARIFIED.toString(), AlgorithmFactory.CIRCULAR.toString(),AlgorithmFactory.PIE.toString()};
        for (Algorithm algorithm : algorithms) {
            if (Arrays.asList(a).contains(algorithm.toString())) {
                this.weightedElementsView.getCmbBoxAlgorithm().addItem(algorithm);
            }
        }
    }
    
    public void setModelsToolBar() {
        this.weightedElementsView.getCmbBoxColor().setModel(this.treeMap.getModel().getColorModel());
        this.weightedElementsView.getCmbBoxColor().removeItemAt(0);
        this.weightedElementsView.getCmbBoxColor().addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    oldColorSelected = (DefaultTreeMapField) e.getItem();
                }
            }
        });
        this.weightedElementsView.getCmbBoxAlgorithm().addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    treeMap.setAlgorithm((Algorithm) e.getItem());
                    oldAlgorithmSelected = (Algorithm) e.getItem();
                    treeMap.setAlgorithm(oldAlgorithmSelected);
                    weightedElementsView.getCmbBoxAlgorithm().setSelectedItem(oldAlgorithmSelected);
                }
            }
        });
    }
                
    public void setEmptyWeightedElements() {
        try {
            String propertyMessage = "";
            this.weightedElementsView.removeAll();
            switch (this.mainWindowController.getMainWindowModel().getAmbitoActual()) {
                case ProjectProperties.FILE:
                    propertyMessage = "fileWithoutClasses";
                    break;
                case ProjectProperties.CLASS:
                    propertyMessage = "classWithoutMethods";
                    break;
                case ProjectProperties.METHOD:
                    propertyMessage = "methodWithoutLocalVariables";
                    break;
            }
            this.weightedElementsView.getMessageLbl().setText(this.mainWindowController.getLanguage().getProperty(propertyMessage));
            this.weightedElementsView.add(this.weightedElementsView.getMessagePnl(), BorderLayout.CENTER);
            this.weightedElementsView.repaint();
        } catch (Exception e) {
            System.out.println("Error: WeightedElementsController - setEmptyTreeMap - " + e.getMessage());
        }
    }
          
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        try {
            if (evt.getPropertyName().equals("selection")) {
                if (evt.getNewValue() != null) {
                    if ((evt.getNewValue() instanceof ArrayList)) {                        
                        ArrayList elementSelected = (ArrayList) evt.getNewValue();                        
                        if (elementSelected.size() > 0) {
                            switch (mainWindowController.getMainWindowModel().getAmbitoActual()) {
                                case ProjectProperties.FILE:
                                    String nombreClase = elementSelected.get(0).toString();
                                    mainWindowController.getMainWindowModel().setAmbitoActual(ProjectProperties.CLASS);
                                    mainWindowController.getMainWindowModel().setClassName(nombreClase);
                                    mainWindowController.showInfo();
                                    break;
                                case ProjectProperties.CLASS:
                                    String nombreMetodoParametros = elementSelected.get(0).toString();                                    
                                    String nombreMetodo = "";
                                    String parametros = "";
                                    int i;
                                    for (i = 0; i < nombreMetodoParametros.length() && nombreMetodoParametros.charAt(i) != '('; i++) {
                                        nombreMetodo += nombreMetodoParametros.charAt(i);
                                    }
                                    for (; i < nombreMetodoParametros.length(); i++) {
                                        parametros += nombreMetodoParametros.charAt(i);
                                    }
                                    mainWindowController.getMainWindowModel().setAmbitoActual(ProjectProperties.METHOD);
                                    mainWindowController.getMainWindowModel().setMethodName(nombreMetodo);
                                    parametros = parametros.replace("(", "").replace(")", "");
                                    ArrayList<ParameterModel> paramatersModel = new ArrayList<ParameterModel>();
                                    if (parametros.trim().length() > 0) {
                                        paramatersModel = this.mainWindowController.getArrayParameters(parametros);
                                    }
                                    mainWindowController.getMainWindowModel().setParameters(paramatersModel);
                                    mainWindowController.showInfo();
                                    break;
                            }
                        }
                    }
                }                
            }
        } catch (Exception e) {
            System.out.println("Error: WeightedElementsController - propertyChange - " + e.getMessage());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            switch (((JComponent)e.getSource()).getName()) {
                case "btnExpand":
                    if (isExpanded) {
                        this.collapse();
                    } else {
                        this.expand();                    
                    }
                    break;
            }
        } catch(Exception ex) {
            System.out.println("Error: DocumentationController - actionPerformed - " + ex.getMessage());
        }
    }

    public void expand() {
        this.isExpanded = true;
        this.mainWindowController.getMainWindowView().getSplitRight().setDividerLocation(0.0);
        this.mainWindowController.getMainWindowView().getRightComponent().setDividerLocation(1.0);
        this.weightedElementsView.getBtnExpandFile().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_shrink.png")));
        this.weightedElementsView.getBtnExpandFile().setToolTipText(this.mainWindowController.getLanguage().getProperty("collapseArea"));
        this.mainWindowController.getMainWindowView().repaint();
        this.mainWindowController.getMainWindowView().revalidate();
    }

    public void collapse() {
        this.isExpanded = false;
        this.mainWindowController.getMainWindowView().getSplitRight().setDividerLocation(this.mainWindowController.getMainWindowView().getSplitRight().getLastDividerLocation());
        this.mainWindowController.getMainWindowView().getRightComponent().setDividerLocation(this.mainWindowController.getMainWindowView().getLeftComponent().getLastDividerLocation());
        this.weightedElementsView.getBtnExpandFile().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_enlarge.png")));
        this.weightedElementsView.getBtnExpandFile().setToolTipText(this.mainWindowController.getLanguage().getProperty("expandArea"));
        this.mainWindowController.getMainWindowView().repaint();
        this.mainWindowController.getMainWindowView().revalidate();
    }
}