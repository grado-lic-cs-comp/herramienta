package controllers;

import config.ProjectProperties;
import dialogs.AboutDialog;
import dialogs.InfoApplicationDialog;
import dialogs.FolderSelectionDialog;
import displayGallery.DisplayShelf;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import models.ParameterModel;
import utils.FilesUtil;

/**
 * Controlador de la barra de diálogo
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class BarController implements ActionListener {
    private MainWindowController mainWindowController = null;
    
    private AboutDialog aboutDialog;
    private AboutDialogController aboutDialogController;
    
    private InfoApplicationDialog infoApplicationDialog;
    private InfoApplicationDialogController infoApplicationDialogController;
    
    private FolderSelectionDialog folderSelectionDialog;
    private FolderSelectionDialogController folderSelectionDialogController;
            
    public BarController(MainWindowController mainController) {
        this.mainWindowController = mainController;
    }
    
    public void init() {
        try {
            this.setConfigurationAboutDialog();
            this.setConfigurationInfoApplicationDialog();
            this.setConfigurationFolderSelectionDialog();
        } catch (Exception e) {
            System.out.println("Error: BarController - init - " + e.toString());
        }
    }
        
    public void initView() {
        this.setEnabledMenuBarItems(true, true, true, true);
        this.getFolderSelectionDialogController().setFlagStartFolderProcessing(false);
    }
    
    public void viewActive(String environment) {
        try {
            Border loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
            switch (environment) {
                case ProjectProperties.FILE:
                    this.mainWindowController.getMainWindowView().getFileSelectedLbl().setBorder(BorderFactory.createLineBorder(Color.GREEN, 1));
                    this.mainWindowController.getMainWindowView().getFileSelectedLbl().setOpaque(true);
                    this.mainWindowController.getMainWindowView().getClassSelectedLbl().setBorder(loweredetched);
                    this.mainWindowController.getMainWindowView().getClassSelectedLbl().setOpaque(false);
                    this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setBorder(loweredetched);
                    this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setOpaque(false);
                    this.mainWindowController.getMainWindowView().goBack.setEnabled(false);
                    this.setTextEnviromentActive();
                    break;
                case ProjectProperties.CLASS:
                    this.mainWindowController.getMainWindowView().getFileSelectedLbl().setBorder(BorderFactory.createLineBorder(Color.BLUE, 1));
                    this.mainWindowController.getMainWindowView().getFileSelectedLbl().setOpaque(true);
                    this.mainWindowController.getMainWindowView().getClassSelectedLbl().setBorder(BorderFactory.createLineBorder(Color.GREEN, 1));
                    this.mainWindowController.getMainWindowView().getClassSelectedLbl().setOpaque(true);
                    this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setBorder(loweredetched);
                    this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setOpaque(false);
                    this.mainWindowController.getMainWindowView().goBack.setEnabled(true);                    
                    this.setTextEnviromentActive();
                    break;
                case ProjectProperties.METHOD:
                    this.mainWindowController.getMainWindowView().getFileSelectedLbl().setBorder(BorderFactory.createLineBorder(Color.BLUE, 1));
                    this.mainWindowController.getMainWindowView().getFileSelectedLbl().setOpaque(true);
                    this.mainWindowController.getMainWindowView().getClassSelectedLbl().setBorder(BorderFactory.createLineBorder(Color.BLUE, 1));
                    this.mainWindowController.getMainWindowView().getClassSelectedLbl().setOpaque(true);
                    this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setBorder(BorderFactory.createLineBorder(Color.GREEN, 1));
                    this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setOpaque(true);
                    this.mainWindowController.getMainWindowView().goBack.setEnabled(true);
                    this.setTextEnviromentActive();
                    break;
            }
        } catch (Exception e) {
            System.out.println("Error: BarController - viewActive - " + e.toString());
        }
    }
    
    public String getNameEnvironmentActive() {
        String name = "";
        switch(this.mainWindowController.getMainWindowModel().getAmbitoActual()) {
            case ProjectProperties.FILE:
                name = this.mainWindowController.getMainWindowModel().getFileDTO().getName();
                break;
            case ProjectProperties.CLASS:
                name = this.mainWindowController.getMainWindowModel().getClassName();
                break;
            case ProjectProperties.METHOD:
                name = this.mainWindowController.getMainWindowModel().getMethodNameParameters();
                break;
        }
        return name;
    }
    
    public void setTextEnviromentActive() {
        try {
            if (this.mainWindowController.isProcessingProject()) {
                if (this.mainWindowController.getMainWindowModel().getFileDTO() != null) {
                    this.mainWindowController.getMainWindowView().getFileSelectedLbl().setText(" " + this.mainWindowController.getMainWindowModel().getFileDTO().getName());
                    this.mainWindowController.getMainWindowView().getFileSelectedLbl().setToolTipText(this.mainWindowController.getMainWindowModel().getFileDTO().getName());
                } else {
                    this.mainWindowController.getMainWindowView().getFileSelectedLbl().setToolTipText(this.mainWindowController.getLanguage().getProperty("valueEmptyFile"));
                }
                this.mainWindowController.getMainWindowView().getClassSelectedLbl().setText(" " + this.mainWindowController.getMainWindowModel().getClassName());
                if (!this.mainWindowController.getMainWindowModel().getClassName().equals("")) {
                    this.mainWindowController.getMainWindowView().getClassSelectedLbl().setToolTipText(this.mainWindowController.getMainWindowModel().getClassName());
                } else {
                    this.mainWindowController.getMainWindowView().getClassSelectedLbl().setToolTipText(this.mainWindowController.getLanguage().getProperty("valueEmptyClass"));
                }
                this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setText(" " + this.mainWindowController.getMainWindowModel().getMethodNameParameters());
                if (!this.mainWindowController.getMainWindowModel().getMethodName().equals("")) {
                    this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setToolTipText(this.mainWindowController.getMainWindowModel().getMethodNameParameters());
                } else {
                    this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setToolTipText(this.mainWindowController.getLanguage().getProperty("valueEmptyMethod"));
                }                
            }
        } catch (Exception e) {
            System.out.println("Error: BarController - setTextEnviromentActive - " + e.toString());
        }
    }
    
    public void setConfigurationAboutDialog() {
        this.aboutDialog = new AboutDialog(this.mainWindowController.getMainWindowView(), false);
        this.aboutDialogController = new AboutDialogController(this.mainWindowController, this.aboutDialog);
        this.aboutDialog.getAboutInformation().addHyperlinkListener(this.aboutDialogController);
        this.aboutDialog.addWindowListener(this.aboutDialogController);
        this.aboutDialog.getBtnClose().addActionListener(this.aboutDialogController);
    }
    
    public void setConfigurationInfoApplicationDialog() {
        this.infoApplicationDialog = new InfoApplicationDialog(this.mainWindowController.getMainWindowView(), false);
        this.infoApplicationDialogController = new InfoApplicationDialogController(this.mainWindowController, infoApplicationDialog);
        this.infoApplicationDialog.addWindowListener(this.infoApplicationDialogController);
        this.infoApplicationDialog.getBtnClose().addActionListener(this.infoApplicationDialogController);
    }
    
    public void setConfigurationFolderSelectionDialog() {
        try {
            this.folderSelectionDialog = new FolderSelectionDialog(this.mainWindowController.getMainWindowView(), false);
            this.folderSelectionDialogController = new FolderSelectionDialogController(this.mainWindowController, this.folderSelectionDialog);
            this.folderSelectionDialog.addWindowListener(this.folderSelectionDialogController);
            this.folderSelectionDialog.getHostsList().addActionListener(this.folderSelectionDialogController);
            this.folderSelectionDialog.getAddSourceFolderBtn().addActionListener(this.folderSelectionDialogController);
            this.folderSelectionDialog.getPathSourceFolder().addMouseListener(this.folderSelectionDialogController);
            this.folderSelectionDialog.getAddDestinationFolderBtn().addActionListener(this.folderSelectionDialogController);
            this.folderSelectionDialog.getPathDestinationFolder().addMouseListener(this.folderSelectionDialogController);
            this.folderSelectionDialog.getAcceptBtn().addActionListener(this.folderSelectionDialogController);
            this.folderSelectionDialog.getCancelBtn().addActionListener(this.folderSelectionDialogController);
            this.folderSelectionDialog.getSourceInfoBtn().addActionListener(this.folderSelectionDialogController);
            this.folderSelectionDialog.getDestinationInfoBtn().addActionListener(this.folderSelectionDialogController);
        } catch (Exception e) {
            System.out.println("Error: BarController - setConfigurationFolderSelectionDialog - " + e.toString());
        }
    }

    public void setLanguage() {
        try {
            this.setLanguageMenuBar();
            this.setLanguageToolBar();
        } catch (Exception e) {
            System.out.println("Error: BarController - setLanguage - " + e.toString());
        }
    }

    public void setLanguageMenuBar() {
        try {
            this.mainWindowController.getMainWindowView().getFile().setText(this.mainWindowController.getLanguage().getProperty("file"));
            this.mainWindowController.getMainWindowView().getMenuOpenFolder().setText(this.mainWindowController.getLanguage().getProperty("menuOpenFolder") + "...");
            this.mainWindowController.getMainWindowView().getMenuOldProject().setText(this.mainWindowController.getLanguage().getProperty("menuOldProject") + "...");
            this.mainWindowController.getMainWindowView().getMenuHostManager().setText(this.mainWindowController.getLanguage().getProperty("menuHostManager") + "...");
            this.mainWindowController.getMainWindowView().getMenuExit().setText(this.mainWindowController.getLanguage().getProperty("menuExit"));
            this.mainWindowController.getMainWindowView().getHelp().setText(this.mainWindowController.getLanguage().getProperty("help"));
            this.mainWindowController.getMainWindowView().getAnalysis().setText(this.mainWindowController.getLanguage().getProperty("analysis"));
            this.mainWindowController.getMainWindowView().getDocumentation().setText(this.mainWindowController.getLanguage().getProperty("documentation"));
            this.mainWindowController.getMainWindowView().getAbout().setText(this.mainWindowController.getLanguage().getProperty("about"));
            this.mainWindowController.getMainWindowView().getLanguages().setText(this.mainWindowController.getLanguage().getProperty("languages"));
            this.mainWindowController.getMainWindowView().getOptionSpanish().setText(this.mainWindowController.getLanguage().getProperty("spanish"));
            this.mainWindowController.getMainWindowView().getOptionEnglish().setText(this.mainWindowController.getLanguage().getProperty("english"));
            this.mainWindowController.getMainWindowView().getOptionPortuguese().setText(this.mainWindowController.getLanguage().getProperty("portuguese"));
        } catch (Exception e) {
            System.out.println("Error: BarController - setLanguageMenuBar - " + e.getMessage());
        }
    }
    
    public void setLanguageToolBar() {
        try {
            this.mainWindowController.getMainWindowView().getFileLbl().setText(this.mainWindowController.getLanguage().getProperty("environmentFile") + " ");
            this.mainWindowController.getMainWindowView().getClassLbl().setText(this.mainWindowController.getLanguage().getProperty("environmentClass") + " ");
            this.mainWindowController.getMainWindowView().getMethodLbl().setText(this.mainWindowController.getLanguage().getProperty("environmentMethod") + " ");
            if (this.mainWindowController.isProcessingProject()) {
                this.mainWindowController.getMainWindowView().getGoBack().setToolTipText(this.mainWindowController.getLanguage().getProperty("goBackToolTip"));
                this.mainWindowController.getMainWindowView().getCenter().setToolTipText(this.mainWindowController.getLanguage().getProperty("centerToolTip"));
                this.mainWindowController.getMainWindowView().getCamera().setToolTipText(this.mainWindowController.getLanguage().getProperty("screenshot"));
                this.mainWindowController.getMainWindowView().getCapturesDisplay().setToolTipText(this.mainWindowController.getLanguage().getProperty("captureDisplay"));
                this.setTextEnviromentActive();
            }
        } catch (Exception e) {
            System.out.println("Error: BarController - setLanguageToolBar - " + e.getMessage());
        }
    }
    
    public void setEnabledMenuBarItems(boolean enableFile, boolean enableLanguages, boolean enableHelp, boolean camera) {
        this.mainWindowController.getMainWindowView().getFile().setEnabled(enableFile);
        this.mainWindowController.getMainWindowView().getLanguages().setEnabled(enableLanguages);
        this.mainWindowController.getMainWindowView().getHelp().setEnabled(enableHelp);
    }

    public void setEnabledToolBarItems(boolean enableAddFile, boolean enableCurrentScope, boolean enableGoBack, boolean enableCenter, boolean enableCapturesDisplay) {
        this.mainWindowController.getMainWindowView().getFileLbl().setEnabled(enableCurrentScope);
        this.mainWindowController.getMainWindowView().getFileSelectedLbl().setEnabled(enableCurrentScope);
        this.mainWindowController.getMainWindowView().getClassLbl().setEnabled(enableCurrentScope);
        this.mainWindowController.getMainWindowView().getClassSelectedLbl().setEnabled(enableCurrentScope);
        this.mainWindowController.getMainWindowView().getMethodLbl().setEnabled(enableCurrentScope);
        this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setEnabled(enableCurrentScope);
        this.mainWindowController.getMainWindowView().getGoBack().setEnabled(enableGoBack);
        this.mainWindowController.getMainWindowView().getCenter().setEnabled(enableCenter);
        this.mainWindowController.getMainWindowView().getCamera().setEnabled(enableCapturesDisplay);
        this.mainWindowController.getMainWindowView().getCapturesDisplay().setEnabled(enableCapturesDisplay);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object source = e.getSource();
            if (source instanceof JButton || source instanceof JMenuItem) {            
                switch (((JComponent)e.getSource()).getName()) {
                    case "menuOpenFolder":
                        this.folderSelectionDialogController.showDialog();
                        break; 
                    case "menuOldProject":
                        JOptionPane.showMessageDialog(
                            new JFrame(),
                            this.mainWindowController.getLanguage().getProperty("extension") + "\n" + 
                            this.mainWindowController.getLanguage().getProperty("featureToAdd"), 
                            this.mainWindowController.getLanguage().getProperty("futureWork"), 
                            JOptionPane.INFORMATION_MESSAGE
                        );
                        break; 
                    case "menuHostManager":
                        JOptionPane.showMessageDialog(
                            new JFrame(),
                            this.mainWindowController.getLanguage().getProperty("extension") + "\n" + 
                            this.mainWindowController.getLanguage().getProperty("featureToAdd"), 
                            this.mainWindowController.getLanguage().getProperty("futureWork"), 
                            JOptionPane.INFORMATION_MESSAGE
                        );
                        break; 
                    case "menuExit": 
                        this.exitApplication();
                        break;
                    case "optionSpanish":
                        this.mainWindowController.setLanguage("espanol");
                        break;
                    case "optionPortuguese": 
                        JOptionPane.showMessageDialog(
                            new JFrame(),
                            this.mainWindowController.getLanguage().getProperty("extension") + "\n" + 
                            this.mainWindowController.getLanguage().getProperty("featureToAdd"), 
                            this.mainWindowController.getLanguage().getProperty("futureWork"), 
                            JOptionPane.INFORMATION_MESSAGE
                        );
                        break;
                    case "optionEnglish": 
                        this.mainWindowController.setLanguage("ingles");
                        break;
                    case "about": 
                        this.aboutDialogController.showDialog();
                        break;
                    case "analysis":
                        this.infoApplicationDialogController.showDialog();
                        break;
                    case "documentation":
                        if (Desktop.isDesktopSupported()) {
                            Desktop.getDesktop().browse(new URI(ProjectProperties.REPORT));
                        }    
                        break;
                    case "goBack": 
                        switch(this.mainWindowController.getMainWindowModel().getAmbitoActual()) {
                            case ProjectProperties.CLASS:
                                this.mainWindowController.getMainWindowModel().setAmbitoActual(ProjectProperties.FILE);
                                this.cleanClass();
                                this.cleanMethod();
                                break;
                            case ProjectProperties.METHOD:
                                this.mainWindowController.getMainWindowModel().setAmbitoActual(ProjectProperties.CLASS);
                                this.cleanMethod();
                                break;
                        }
                        this.mainWindowController.showInfo();
                        break;
                    case "center": 
                        this.mainWindowController.centerViews();
                        break;
                    case "camera": 
                        this.captureScreen();
                        break;
                    case "capturesDisplay": 
                        this.capturesDisplay();
                        break;
                }
            }
        } catch(Exception ex) {
            System.out.println("Error: BarController - actionPerformed - " + ex.getMessage());
        }
    }
    
    public void cleanIndicators() {
        Border loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
        this.mainWindowController.getMainWindowView().getFileSelectedLbl().setText("");
        this.mainWindowController.getMainWindowView().getFileSelectedLbl().setToolTipText(null);
        this.mainWindowController.getMainWindowView().getFileSelectedLbl().setBorder(loweredetched);
        this.mainWindowController.getMainWindowView().getFileSelectedLbl().setOpaque(false);
        this.mainWindowController.getMainWindowView().getClassSelectedLbl().setText("");
        this.mainWindowController.getMainWindowView().getClassSelectedLbl().setToolTipText(null);
        this.mainWindowController.getMainWindowView().getClassSelectedLbl().setBorder(loweredetched);
        this.mainWindowController.getMainWindowView().getClassSelectedLbl().setOpaque(false);
        this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setText("");
        this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setToolTipText(null);
        this.mainWindowController.getMainWindowModel().setParameters(new ArrayList<ParameterModel>());    
        this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setBorder(loweredetched);
        this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setOpaque(false);
    }

    public void cleanClass() {
        this.mainWindowController.getMainWindowModel().setClassName("");
        this.mainWindowController.getMainWindowView().getClassSelectedLbl().setToolTipText(this.mainWindowController.getLanguage().getProperty("valueEmptyClass"));
    }
    
    public void cleanMethod() {
        this.mainWindowController.getMainWindowModel().setMethodName("");
        this.mainWindowController.getMainWindowView().getMethodSelectedLbl().setToolTipText(this.mainWindowController.getLanguage().getProperty("valueEmptyMethod"));
        this.mainWindowController.getMainWindowModel().setParameters(new ArrayList<ParameterModel>());    
    }
    
    public void captureScreen() {
        try {
            Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
            Rectangle screenRectangle = new Rectangle(screenSize);
            Robot robot = new Robot();
            BufferedImage image = robot.createScreenCapture(screenRectangle);  
            String folder = this.mainWindowController.getBarController().getFolderSelectionDialogController().getRutaCarpetaDestino() + this.mainWindowController.getNameProject() + "/";
            File path = new File(folder);
            if (!path.exists()) {
                path.mkdirs();
            }
            this.mainWindowController.getMainWindowModel().updateFileCountScreenShot(this.mainWindowController.getMainWindowModel().getFileDTO().getPath());
            Date now = new Date();
            String format = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).format(now);
            String nameScreen = folder + File.separator + 
                                format + "_" + 
                                FilesUtil.getNameWithoutExtension(this.mainWindowController.getMainWindowModel().getFileDTO().getName(), "java") + 
                                "_" + this.mainWindowController.getMainWindowModel().getFileDTO().getCount_screenshot() + ".png";
            File fileScreen = new File(nameScreen);
            ImageIO.write(image, "png", fileScreen);
            this.mainWindowController.getMessageDialog().setTitle(this.mainWindowController.getLanguage().getProperty("captureTitle"));
            this.mainWindowController.getMessageDialog().getLblTitle().setText(this.mainWindowController.getLanguage().getProperty("captureSavedSuccessfully"));
            this.mainWindowController.getMessageDialog().getTxtAreaContent().setText(fileScreen.getAbsolutePath());
            BufferedImage  image1 = ImageIO.read(fileScreen);
            ImageIcon imageIcon1 = new ImageIcon(image1);
            Image redInput = imageIcon1.getImage().getScaledInstance(113, 144, Image.SCALE_SMOOTH);
            imageIcon1.setImage(redInput);
            this.mainWindowController.getMessageDialog().getIcon().setIcon(imageIcon1);
            this.mainWindowController.getMessageDialogController().showDialog();
        } catch (HeadlessException | AWTException | IOException ex) {
            System.out.println("Error: BarController - captureScreen - " + ex.getMessage());            
        }
    }
    
    public void capturesDisplay() {
        DisplayShelf displayShelf = new DisplayShelf();
        displayShelf.setMainWindowController(this.mainWindowController);
        displayShelf.show();
    }
    
    public void exitApplication() {
        try {
            System.exit(0);
        } catch (Exception ex) {
            System.out.println("Error: BarController - exitApplication - " + ex.getMessage());
        }    
    }
    
    public MainWindowController getMainWindowController() {
        return mainWindowController;
    }

    public void setMainWindowController(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
    }

    public AboutDialog getAboutDialog() {
        return aboutDialog;
    }

    public void setAboutDialog(AboutDialog aboutDialog) {
        this.aboutDialog = aboutDialog;
    }

    public AboutDialogController getAboutDialogController() {
        return aboutDialogController;
    }

    public void setAboutDialogController(AboutDialogController aboutDialogController) {
        this.aboutDialogController = aboutDialogController;
    }

    public InfoApplicationDialog getInfoApplicationDialog() {
        return infoApplicationDialog;
    }

    public void setInfoApplicationDialog(InfoApplicationDialog infoApplicationDialog) {
        this.infoApplicationDialog = infoApplicationDialog;
    }

    public InfoApplicationDialogController getInfoApplicationDialogController() {
        return infoApplicationDialogController;
    }

    public void setInfoApplicationDialogController(InfoApplicationDialogController infoApplicationDialogController) {
        this.infoApplicationDialogController = infoApplicationDialogController;
    }

    public FolderSelectionDialog getFolderSelectionDialog() {
        return folderSelectionDialog;
    }

    public void setFolderSelectionDialog(FolderSelectionDialog folderSelectionDialog) {
        this.folderSelectionDialog = folderSelectionDialog;
    }

    public FolderSelectionDialogController getFolderSelectionDialogController() {
        return folderSelectionDialogController;
    }

    public void setFolderSelectionDialogController(FolderSelectionDialogController folderSelectionDialogController) {
        this.folderSelectionDialogController = folderSelectionDialogController;
    }
}