package controllers;

import config.ProjectProperties;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import models.ChartMetricModel;
import models.MetricDescriptionModel;
import models.MetricValueModel;
import models.ParameterModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.TickUnitSource;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import views.ChartMetricView;

/**
 * Controlador de la vista ChartMetricView
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ChartMetricController implements ActionListener {
    private ChartMetricModel chartMetricModel;
    private ChartMetricView chartMetricView;
    private MainWindowController mainWindowController;
    private boolean isExpanded;

    public ChartMetricController(ChartMetricModel chartMetricModel, ChartMetricView chartMetricView, MainWindowController mainController) {
        this.chartMetricModel = chartMetricModel;
        this.chartMetricView = chartMetricView;
        this.mainWindowController = mainController;
        this.isExpanded = false;
    }
    
    public void setLanguage() {
        TitledBorder title = BorderFactory.createTitledBorder(this.mainWindowController.getLanguage().getProperty("titleMetricArea"));
        this.chartMetricView.setBorder(title);
        this.chartMetricView.getLblMetrics().setText(this.mainWindowController.getLanguage().getProperty("lblMetrics"));
        if (isExpanded) {
            this.chartMetricView.getExpandBtn().setToolTipText(this.mainWindowController.getLanguage().getProperty("collapseArea"));
        } else {
            this.chartMetricView.getExpandBtn().setToolTipText(this.mainWindowController.getLanguage().getProperty("expandArea"));
        }
        this.show();
    }
            
    public void show() {
        ChartPanel jFreeChart;
        switch(this.mainWindowController.getMainWindowModel().getAmbitoActual()) {
            case ProjectProperties.FILE:
            case ProjectProperties.CLASS:
                this.loadToolBar();
                jFreeChart = this.generateBarChart();
                this.loadViewBarChart(jFreeChart);
                break;
            case ProjectProperties.METHOD:
                this.loadMethodMetricsPnl();
                break;
        }
        this.updateView();
    }

    public void loadToolBar() {
        try {
            this.chartMetricView.getCmbBoxMetrics().removeAllItems();
            ArrayList<MetricDescriptionModel> arrayDescriptionsMetrics = this.chartMetricModel.getDescriptionMetrics(this.mainWindowController.getLanguage().getActualLanguage(), this.mainWindowController.getMainWindowModel().getAmbitoActual());
            for (MetricDescriptionModel metric : arrayDescriptionsMetrics) {
                this.chartMetricView.getCmbBoxMetrics().addItem(metric.getNombre().toUpperCase() + ": " + metric.getDescription());
            }
        } catch (Exception e) {
            System.out.println("Error: ChartMetricController - loadToolBar - " + e.getMessage());
        }
    }
            
    public ChartPanel generateBarChart() {
        JFreeChart chart = null;
        DefaultCategoryDataset dataSet = null;
        String axisX = "";
        String metric_description = "";
        String metricName = "";
        String description = "";
        try {
            metric_description = String.valueOf(this.chartMetricView.getCmbBoxMetrics().getSelectedItem());
            if (metric_description.split(":").length >= 2) {
                metricName = metric_description.split(":")[0];
                description = metric_description.split(":")[1];            
            }
            String pathFile;
            String className;
            switch(this.mainWindowController.getMainWindowModel().getAmbitoActual()) {
                case ProjectProperties.FILE:
                    pathFile = this.mainWindowController.getMainWindowModel().getPathFile();
                    this.chartMetricModel.loadArrayValuesMetricsFile(pathFile, metricName.toLowerCase());
                    axisX = this.mainWindowController.getLanguage().getProperty("classes");
                  break;  
                case ProjectProperties.CLASS:
                    pathFile = this.mainWindowController.getMainWindowModel().getPathFile();
                    className = this.mainWindowController.getMainWindowModel().getClassName();
                    this.chartMetricModel.loadArrayValuesMetricsClass(pathFile, className, metricName.toLowerCase());
                    axisX = this.mainWindowController.getLanguage().getProperty("methods");
                    break;  
            }
            dataSet = this.chartMetricModel.getDataSetBarMetrics(description);            
            chart = ChartFactory.createBarChart(metric_description, axisX, metricName.toUpperCase(), dataSet, PlotOrientation.VERTICAL, false, true, false);
            CategoryPlot plot = (CategoryPlot) chart.getPlot();
            TickUnitSource tickUnits = NumberAxis.createIntegerTickUnits();
            plot.getRangeAxis().setStandardTickUnits(tickUnits); 
            CategoryItemRenderer renderer = plot.getRenderer();
            renderer.setSeriesPaint(0, Color.orange);
            renderer.setSeriesPaint(1, Color.red);
            renderer.setSeriesPaint(2, Color.BLUE);
            renderer.setSeriesPaint(3, Color.CYAN);
            renderer.setSeriesPaint(4, Color.GRAY);
            renderer.setSeriesPaint(5, Color.GREEN);
            renderer.setSeriesPaint(35, Color.GREEN);
        } catch(Exception e) {
            System.out.println("Error: ChartMetricController - generateBarChart - " + e.toString());
        }
        ChartPanel jfc = new ChartPanel(chart);
        JPopupMenu pm = new JPopupMenu();
        jfc.setPopupMenu(pm);
        return jfc;
    }
    
    public void loadViewBarChart(ChartPanel chartPanel) {
        this.chartMetricView.getGraphicAreaPnl().removeAll();
        this.chartMetricView.getGraphicAreaPnl().setLayout(new BorderLayout());
        this.chartMetricView.getGraphicAreaPnl().add(chartPanel, BorderLayout.CENTER);
        this.chartMetricView.removeAll();
        this.chartMetricView.add(chartMetricView.getMetricsPnl(), BorderLayout.CENTER);
    }
                
    public void loadMethodMetricsPnl() {
        this.chartMetricView.removeAll();
        this.chartMetricView.add(chartMetricView.getMethodMetricsPnl(), BorderLayout.CENTER);  
        DefaultTableModel table = new DefaultTableModel(this.getValuesTable(), this.getColumnsTable());
        this.chartMetricView.getMetricsTable().setModel(table);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        ((DefaultTableCellRenderer)this.chartMetricView.getMetricsTable().getTableHeader().getDefaultRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
        centerRenderer.setHorizontalAlignment( SwingConstants.CENTER );        
        for(int x = 1; x < this.getColumnsTable().length; x++){
            this.chartMetricView.getMetricsTable().getColumnModel().getColumn(x).setCellRenderer( centerRenderer );
        }
        this.updateView();
    }

    public String[] getColumnsTable() {
        String[] columnNames = { 
            this.mainWindowController.getLanguage().getProperty("metric"),
            this.mainWindowController.getLanguage().getProperty("value")
        };
        return columnNames;
    }
    
    public Object[][] getValuesTable() {
        String pathFile = this.mainWindowController.getMainWindowModel().getPathFile();
        String className = this.mainWindowController.getMainWindowModel().getClassName();
        String nameMethod = this.mainWindowController.getMainWindowModel().getMethodName();
        ArrayList<ParameterModel> parameters = this.mainWindowController.getMainWindowModel().getParameters();
        ArrayList<MetricValueModel> values = this.chartMetricModel.getMetricsMethod(pathFile, className, nameMethod, parameters, this.mainWindowController.getLanguage().getActualLanguage());
        Object[][] data = new Object[3][2];
        for(int i = 0; i < 3; i++) {
            data[i] = new Object[]{
                values.get(i).getNombre(),
                values.get(i).getCantidad()
            };
        }
        return data;
    }
        
    public void showSuggestions(String environmentActual, String metric) {
        String[] metric_description = metric.split(":");
        this.mainWindowController.getMessageDialog().setTitle(this.mainWindowController.getLanguage().getProperty("suggestions"));
        this.mainWindowController.getMessageDialog().getLblTitle().setText(metric_description[0].toUpperCase() + ":" + metric_description[1]);
        this.mainWindowController.getMessageDialog().getTxtAreaContent().setText(this.chartMetricModel.getInterpretationMetric(metric_description[0], environmentActual, this.mainWindowController.getLanguage().getActualLanguage()));
        this.mainWindowController.getMessageDialog().getIcon().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/64_stats-bars.png")));
        this.mainWindowController.getMessageDialogController().showDialog();
    }
            
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object source = e.getSource();
            if (source instanceof JButton || source instanceof JComboBox) {            
                switch (((JComponent) e.getSource()).getName()) {
                    case "btnSuggestions":
                        String metric = (String) this.chartMetricView.getCmbBoxMetrics().getSelectedItem();
                        this.showSuggestions(this.mainWindowController.getMainWindowModel().getAmbitoActual(), metric);
                        break;
                    case "cmbBoxMetrics":
                        switch (this.mainWindowController.getMainWindowModel().getAmbitoActual()) {
                            case ProjectProperties.FILE:
                            case ProjectProperties.CLASS:
                                ChartPanel chartPanel = this.generateBarChart();
                                this.loadViewBarChart(chartPanel);
                                this.updateView();
                                break;
                        }
                        this.updateView();
                        break;
                    case "btnExpand":
                        if (isExpanded) {
                            this.collapse();
                        } else {
                            this.expand();                    
                        }
                        break;
                }
            }
        } catch (Exception ex) {
            System.out.println("Error: ChartMetricController - actionPerformed - " + ex.getMessage());
        }
    }
    
    public void expand() {
        this.isExpanded = true;
        this.mainWindowController.getMainWindowView().getSplitRight().setDividerLocation(1.0);
        this.mainWindowController.getMainWindowView().getLeftComponent().setDividerLocation(0.0);
        this.mainWindowController.getChartMetricView().getExpandBtn().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_shrink.png")));
        this.chartMetricView.getExpandBtn().setToolTipText(this.mainWindowController.getLanguage().getProperty("collapseArea"));
        this.mainWindowController.getMainWindowView().repaint();
        this.mainWindowController.getMainWindowView().revalidate();
    }

    public void collapse() {
        this.isExpanded = false;
        this.mainWindowController.getMainWindowView().getSplitRight().setDividerLocation(this.mainWindowController.getMainWindowView().getSplitRight().getLastDividerLocation());
        this.mainWindowController.getMainWindowView().getLeftComponent().setDividerLocation(this.mainWindowController.getMainWindowView().getLeftComponent().getLastDividerLocation());
        this.mainWindowController.getChartMetricView().getExpandBtn().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_enlarge.png")));
        this.chartMetricView.getExpandBtn().setToolTipText(this.mainWindowController.getLanguage().getProperty("expandArea"));
        this.mainWindowController.getMainWindowView().repaint();
        this.mainWindowController.getMainWindowView().revalidate();
    }

    public void updateView() {
        int lastDividerLocation = this.mainWindowController.getMainWindowView().getLeftComponent().getDividerLocation();
        this.mainWindowController.getMainWindowView().getLeftComponent().setRightComponent(this.chartMetricView);
        this.mainWindowController.getMainWindowView().getLeftComponent().setDividerLocation(lastDividerLocation);
    }
}