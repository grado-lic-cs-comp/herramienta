package controllers;

import config.ProjectProperties;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.border.TitledBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Element;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import models.EnhancedSourceCodeModel;
import models.ParameterModel;
import views.EnhancedSourceCodeView;

/**
 * Controlador de la vista EnhancedSourceCodeView
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class EnhancedSourceCodeController implements HyperlinkListener, ActionListener {
    private EnhancedSourceCodeModel enhancedSourceCodeModel;
    private EnhancedSourceCodeView enhancedSourceCodeView;
    private MainWindowController mainWindowController;
    private HTMLEditorKit editorKit = null;
    private StyleSheet styleSheet = null;
    private String[] stylesValues;
    private int oldStyleIndex;
    private String rule;
    private boolean isExpanded;

    public EnhancedSourceCodeController(EnhancedSourceCodeModel enhancedSourceCodeModel, EnhancedSourceCodeView enhancedSourceCodeView, MainWindowController mainController) {
        this.enhancedSourceCodeModel = enhancedSourceCodeModel;
        this.enhancedSourceCodeView = enhancedSourceCodeView;
        this.editorKit = new HTMLEditorKit();
        this.styleSheet = editorKit.getStyleSheet();
        this.enhancedSourceCodeView.getEditorPaneEnhancedSourceCode().setEditorKit(this.editorKit);
        this.mainWindowController = mainController;
        this.oldStyleIndex = 0;
        this.rule = "";
        this.isExpanded = false;        
    }
    
    public void setLanguage() {
        try {
            TitledBorder title = BorderFactory.createTitledBorder(this.mainWindowController.getLanguage().getProperty("titleEnhancedSourceCodeArea"));
            this.enhancedSourceCodeView.setBorder(title);
            this.enhancedSourceCodeView.getLblFilter().setText(this.mainWindowController.getLanguage().getProperty("nameFilter"));
            this.enhancedSourceCodeView.getBtnFilter().setToolTipText(this.mainWindowController.getLanguage().getProperty("filter"));
            this.enhancedSourceCodeView.getBtnClearFilter().setToolTipText(this.mainWindowController.getLanguage().getProperty("clearFilter"));
            this.enhancedSourceCodeView.getLblStyle().setText(this.mainWindowController.getLanguage().getProperty("styles"));
            this.stylesValues = new String[]{this.mainWindowController.getLanguage().getProperty("light"), this.mainWindowController.getLanguage().getProperty("thin"), this.mainWindowController.getLanguage().getProperty("dark")};
            this.enhancedSourceCodeView.getStylesCmb().removeAllItems();
            for (String value : stylesValues) {
                this.enhancedSourceCodeView.getStylesCmb().addItem(value);        
            }
            if (isExpanded) {
                this.enhancedSourceCodeView.getExpandBtn().setToolTipText(this.mainWindowController.getLanguage().getProperty("collapseArea"));
            } else {
                this.enhancedSourceCodeView.getExpandBtn().setToolTipText(this.mainWindowController.getLanguage().getProperty("expandArea"));
            }
            this.enhancedSourceCodeView.getStylesCmb().setSelectedIndex(this.getOldStyleIndex());
        } catch (Exception e) {
            System.out.println("Error: EnhancedSourceCodeController - setLanguage - "+ e.toString());
        }
    }

    public void show() {
        try {
            if (this.enhancedSourceCodeView.getStylesCmb().getSelectedItem() != null) {
                switch(this.enhancedSourceCodeView.getStylesCmb().getSelectedItem().toString()) {
                    case "Light":
                    case "Claro":
                        this.styleSheet.importStyleSheet(EnhancedSourceCodeController.class.getResource("/css/light.css"));
                        this.styleSheet.removeStyle("a {color:#ff0000;}");
                        this.styleSheet.addRule("a {color:#070be5;}");
                        this.enhancedSourceCodeView.getEditorPaneEnhancedSourceCode().setForeground(Color.BLACK);
                        break;
                    case "Thin":
                    case "Tenue":
                        this.styleSheet.importStyleSheet(EnhancedSourceCodeController.class.getResource("/css/thin.css"));
                        this.styleSheet.removeStyle("a {color:#ff0000;}");
                        this.styleSheet.addRule("a {color:#070be5;}");
                        this.enhancedSourceCodeView.getEditorPaneEnhancedSourceCode().setForeground(Color.BLACK);
                        break;
                    case "Dark":
                    case "Oscuro":
                        this.styleSheet.importStyleSheet(EnhancedSourceCodeController.class.getResource("/css/dark.css"));
                        this.styleSheet.removeStyle("a {color:#070be5;}");
                        this.styleSheet.addRule("a {color:#ff0000;}");
                        this.enhancedSourceCodeView.getEditorPaneEnhancedSourceCode().setForeground(Color.WHITE);
                        break;
                }
                String pathFile;
                String codeHtml;
                String className;
                switch(this.mainWindowController.getMainWindowModel().getAmbitoActual()) {
                    case ProjectProperties.FILE:
                        pathFile = this.mainWindowController.getMainWindowModel().getPathFile();
                        codeHtml = this.enhancedSourceCodeModel.getFileCodeHtml(pathFile);
                        this.enhancedSourceCodeView.getEditorPaneEnhancedSourceCode().setText(codeHtml);
                        editorKit.setLinkCursor(new Cursor((Cursor.HAND_CURSOR)));
                        break;
                    case ProjectProperties.CLASS:
                        pathFile = this.mainWindowController.getMainWindowModel().getPathFile();
                        className = this.mainWindowController.getMainWindowModel().getClassName();
                        codeHtml = this.enhancedSourceCodeModel.getClassCodeHtml(pathFile, className);
                        this.enhancedSourceCodeView.getEditorPaneEnhancedSourceCode().setText(codeHtml);
                        editorKit.setLinkCursor(new Cursor((Cursor.HAND_CURSOR)));
                        break;
                    case ProjectProperties.METHOD:
                        pathFile = this.mainWindowController.getMainWindowModel().getPathFile();
                        className = this.mainWindowController.getMainWindowModel().getClassName();
                        String nameMethod = this.mainWindowController.getMainWindowModel().getMethodName();
                        ArrayList<ParameterModel> parameters = this.mainWindowController.getMainWindowModel().getParameters();
                        codeHtml = this.enhancedSourceCodeModel.getMethodCodeHtml(pathFile, className, nameMethod, parameters);
                        this.enhancedSourceCodeView.getEditorPaneEnhancedSourceCode().setText(codeHtml);
                        editorKit.setLinkCursor(new Cursor((Cursor.DEFAULT_CURSOR)));
                        break;            
                }
                this.enhancedSourceCodeView.getEditorPaneEnhancedSourceCode().setCaretPosition(0);
            }
        } catch (Exception e) {
            System.out.println("Error: EnhancedSourceCodeController - show - "+ e.toString());
        }
    }
    
    public void showStyles() {
        Enumeration rules = styleSheet.getStyleNames();
        while (rules.hasMoreElements()) {
            String name = (String) rules.nextElement();
            System.out.println(styleSheet.getStyle(name).toString());
        }    
    }
        
    public void setStyle() {
        styleSheet.removeStyle(rule);
        String filter = this.enhancedSourceCodeView.getTxtFieldFilter().getText().toLowerCase();
        filter = filter.replaceAll("\\s+","");
        filter = filter.replaceAll(" ", "");        
        if (filter.contains("(")) {
            String[] possibleMethod = filter.split("\\(");
            filter = possibleMethod[0];
        }
        rule = ".".concat(filter);
        filter = ".".concat(filter.concat("{background-color:yellow;}"));
        styleSheet.addRule(filter);
        this.enhancedSourceCodeView.repaint();
        this.enhancedSourceCodeView.revalidate();
    }
    
    public void clearFilter() {
        this.enhancedSourceCodeView.getTxtFieldFilter().setText("");
        styleSheet.removeStyle(rule);    
        this.enhancedSourceCodeView.repaint();
        this.enhancedSourceCodeView.revalidate();
    }
    
    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        try {
            if ( e.getEventType() == HyperlinkEvent.EventType.ACTIVATED ) {
                Element elem = e.getSourceElement();
                if (elem != null) {
                    AttributeSet attr = elem.getAttributes();
                    AttributeSet a = (AttributeSet) attr.getAttribute(HTML.Tag.A);
                    if (a != null) {
                        switch(a.getAttribute(HTML.Attribute.HREF).toString()) {
                            case "class":
                                this.mainWindowController.getMainWindowModel().setAmbitoActual(ProjectProperties.CLASS);
                                this.mainWindowController.getMainWindowModel().setClassName(a.getAttribute(HTML.Attribute.NAME).toString());
                                this.mainWindowController.showInfo();
                                break;
                            case "method":
                                this.mainWindowController.getMainWindowModel().setAmbitoActual(ProjectProperties.METHOD);
                                String[] names = a.getAttribute(HTML.Attribute.NAME).toString().split("!");
                                this.mainWindowController.getMainWindowModel().setClassName(names[0]);
                                this.mainWindowController.getMainWindowModel().setMethodName(names[1]);
                                names[2] = names[2].replace("(", "").replace(")", "");
                                ArrayList<ParameterModel> paramatersModel = new ArrayList<ParameterModel>();
                                if (names[2].trim().length() > 0) {
                                    paramatersModel = this.mainWindowController.getArrayParameters(names[2]);
                                }
                                this.mainWindowController.getMainWindowModel().setParameters(paramatersModel);
                                this.mainWindowController.showInfo();
                                break;
                        }
                    }
                }
            }
        } catch(Exception ex) {
            System.out.println("Error en EnhancedSourceCodeController - hyperlinkUpdate - " + ex);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object source = e.getSource();
            if (source instanceof JButton || source instanceof JComboBox) {
                switch (((JComponent) e.getSource()).getName()) {
                    case "stylesCmb":
                        this.oldStyleIndex = this.enhancedSourceCodeView.getStylesCmb().getSelectedIndex();
                        this.show();
                        break;
                    case "btnFilter":
                        this.show();
                        this.setStyle();
                        break;
                    case "btnClearFilter":
                        this.clearFilter();
                        this.show();
                        break;
                    case "btnExpand":
                        if (isExpanded) {
                            this.collapse();
                        } else {
                            this.expand();                    
                        }
                        break;
                }
            }
        } catch (Exception ex) {
            System.out.println("Error: EnhancedSourceCodeController - actionPerformed - " + ex.getMessage());
        }
    }
    
    public int getOldStyleIndex() {
        return oldStyleIndex;
    }

    public void setOldStyleIndex(int oldStyleIndex) {
        this.oldStyleIndex = oldStyleIndex;
    }
    
    public void expand() {
        this.isExpanded = true;
        this.mainWindowController.getMainWindowView().getSplitRight().setDividerLocation(1.0);
        this.mainWindowController.getMainWindowView().getLeftComponent().setDividerLocation(1.0);
        this.enhancedSourceCodeView.getExpandBtn().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_shrink.png")));
        this.enhancedSourceCodeView.getExpandBtn().setToolTipText(this.mainWindowController.getLanguage().getProperty("collapseArea"));
        this.mainWindowController.getMainWindowView().repaint();
        this.mainWindowController.getMainWindowView().revalidate();
    }

    public void collapse() {
        this.isExpanded = false;
        this.mainWindowController.getMainWindowView().getSplitRight().setDividerLocation(this.mainWindowController.getMainWindowView().getSplitRight().getLastDividerLocation());
        this.mainWindowController.getMainWindowView().getLeftComponent().setDividerLocation(this.mainWindowController.getMainWindowView().getLeftComponent().getLastDividerLocation());
        this.enhancedSourceCodeView.getExpandBtn().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_enlarge.png")));
        this.enhancedSourceCodeView.getExpandBtn().setToolTipText(this.mainWindowController.getLanguage().getProperty("expandArea"));
        this.mainWindowController.getMainWindowView().repaint();
        this.mainWindowController.getMainWindowView().revalidate();
    }
}