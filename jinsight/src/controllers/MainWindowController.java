package controllers;

import config.ProjectProperties;
import dialogs.MessageDialog;
import dialogs.TableDialog;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComponent;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JToggleButton;
import models.FileModel;
import models.ChartMetricModel;
import models.DocumentationModel;
import models.EnhancedSourceCodeModel;
import models.ListElementsModel;
import models.MainWindowModel;
import models.ParameterModel;
import models.WeightedElementsModel;
import models.TreeDirectoryModel;
import utils.Language;
import views.ChartMetricView;
import views.DocumentationView;
import views.EnhancedSourceCodeView;
import views.ListElementsView;
import views.WeightedElementsView;
import views.MainWindowView;
import views.TreeDirectoryView;

/**
 * Controlador de la vista MainWindowView
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MainWindowController implements WindowListener {
    private MainWindowModel mainWindowModel;
    private MainWindowView mainWindowView;
    
    private TreeDirectoryModel treeDirectoryModel;
    private TreeDirectoryView treeDirectoryView;
    private TreeDirectoryController treeDirectoryController;
    
    private ListElementsModel listElementsModel;
    private ListElementsController listElementsController;
    private ListElementsView listElementsView;
    
    private EnhancedSourceCodeModel enhancedSourceCodeModel;
    private EnhancedSourceCodeView enhancedSourceCodeView;
    private EnhancedSourceCodeController enhancedSourceCodeController;
    
    private WeightedElementsModel weightedElementsModel;
    private WeightedElementsView weightedElementsView;
    private WeightedElementsController weightedElementsController;
    
    private ChartMetricModel chartMetricModel;
    private ChartMetricView chartMetricView;
    private ChartMetricController chartMetricController;
    
    private DocumentationModel documentationModel;
    private DocumentationView documentationView;
    private DocumentationController documentationController;
    
    private MessageDialog messageDialog;
    private MessageDialogController messageDialogController;
    
    private TableDialog tableDialog;
    private TableDialogController tableDialogController;

    private BarController barController = null;
    
    private Language language = null;
    
    private FileModel ambArchivoModel = null;
    
    private boolean processingProject = false;
    private String folderSelectedParent;
    private String nameProject;
    
    
    public MainWindowController(MainWindowModel mainModel, MainWindowView mainView) {
        this.mainWindowModel = mainModel;
        this.mainWindowView = mainView;
    }
    
    public void init() {
        this.setConfigurationView();
        this.language = new Language("espanol");
        Image logo = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/32_eye.png"));
        this.mainWindowView.setTitle(this.language.getProperty("applicationName"));
        this.mainWindowView.setIconImage(logo);
        this.mainWindowModel.setAmbitoActual(ProjectProperties.FILE);
        this.setConfigurationBar();
        this.setLanguage("espanol");
        this.setConfigurationTreeDirectory();
        this.setConfigurationListElements();
        this.setConfigurationEnhancedSourceCode();
        this.setConfigurationWeightedElements();
        this.setConfigurationMetrics();
        this.setConfigurationDocumentation();
        this.setConfigurationMessageDialog();
        this.setConfigurationTableDialog();
        this.centerViews();
        this.centerScreen();
        this.barController.setEnabledMenuBarItems(true, true, true, true);
        this.enableComponents(this.mainWindowView.getToolBar() ,false);
        this.ambArchivoModel = new FileModel();        
        this.mainWindowView.getMainPanel().setVisible(false);
        this.mainWindowView.setVisible(true);
    }
        
    public void setConfigurationView() {
        this.mainWindowView.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.mainWindowView.getContentPane().setLayout(new BorderLayout());
        this.mainWindowView.getContentPane().add(this.mainWindowView.getToolBar(), BorderLayout.NORTH);
        this.mainWindowView.getContentPane().add(this.mainWindowView.getMainPanel(), BorderLayout.CENTER);
        this.mainWindowView.getContentPane().add(this.mainWindowView.getStatusBar(), BorderLayout.SOUTH);
        this.mainWindowView.addWindowListener(this);
    }
    
    public void setConfigurationBar() {
        try {
            this.barController = new BarController(this);
            this.mainWindowView.getMenuOpenFolder().addActionListener(this.barController);
            this.mainWindowView.getMenuOldProject().addActionListener(this.barController);
            this.mainWindowView.getMenuHostManager().addActionListener(this.barController);
            this.mainWindowView.getMenuExit().addActionListener(this.barController);
            this.mainWindowView.getOptionEnglish().addActionListener(this.barController);
            this.mainWindowView.getOptionSpanish().addActionListener(this.barController);
            this.mainWindowView.getOptionPortuguese().addActionListener(this.barController);
            this.mainWindowView.getAbout().addActionListener(this.barController);
            this.mainWindowView.getAnalysis().addActionListener(this.barController);
            this.mainWindowView.getDocumentation().addActionListener(this.barController);
            this.mainWindowView.getGoBack().addActionListener(this.barController);
            this.mainWindowView. getCenter().addActionListener(this.barController);
            this.mainWindowView. getCamera().addActionListener(this.barController);            
            this.mainWindowView. getCapturesDisplay().addActionListener(this.barController);
            this.barController.init();
        } catch(Exception e) {
            System.out.println("Error: MainWindowController - setConfigurationMenuBar - " + e);
        }
    }
    
    public void setConfigurationTreeDirectory() {
        this.treeDirectoryModel = new TreeDirectoryModel(this.mainWindowModel);
        this.treeDirectoryView = new TreeDirectoryView();
        this.treeDirectoryController = new TreeDirectoryController(this.treeDirectoryModel, this.treeDirectoryView, this);
        this.treeDirectoryView.getExpandBtn().addActionListener(this.treeDirectoryController);
        this.treeDirectoryView.getCollapseBtn().addActionListener(this.treeDirectoryController);
        this.treeDirectoryView.getStatisticsBtn().addActionListener(this.treeDirectoryController);
        this.mainWindowView.getSplitLeft().setLeftComponent(this.treeDirectoryView);
    }
    
    public void setConfigurationListElements() {
        this.listElementsModel = new ListElementsModel(this.mainWindowModel);
        this.listElementsView = new ListElementsView();
        this.listElementsController = new ListElementsController(this.listElementsModel, this.listElementsView, this);
        this.listElementsView.getListElements().addListSelectionListener(this.listElementsController);
        this.listElementsView.getListElements().addMouseMotionListener(this.listElementsController);
        this.listElementsView.getFilterElements().getDocument().addDocumentListener(this.listElementsController);
        this.listElementsView.getBtnClearFilter().addActionListener(this.listElementsController);
        this.mainWindowView.getSplitLeft().setRightComponent(this.listElementsView);
    }

    public void setConfigurationEnhancedSourceCode() {
        try {
            this.enhancedSourceCodeModel = new EnhancedSourceCodeModel(this.mainWindowModel);
            this.enhancedSourceCodeView = new EnhancedSourceCodeView();
            this.enhancedSourceCodeController = new EnhancedSourceCodeController(this.enhancedSourceCodeModel, this.enhancedSourceCodeView, this);
            this.enhancedSourceCodeView.getEditorPaneEnhancedSourceCode().addHyperlinkListener(this.enhancedSourceCodeController);
            this.enhancedSourceCodeView.getStylesCmb().addActionListener(this.enhancedSourceCodeController);
            this.enhancedSourceCodeView.getBtnFilter().addActionListener(this.enhancedSourceCodeController);
            this.enhancedSourceCodeView.getBtnClearFilter().addActionListener(this.enhancedSourceCodeController);
            this.enhancedSourceCodeView.getExpandBtn().addActionListener(this.enhancedSourceCodeController);
            this.mainWindowView.getLeftComponent().setLeftComponent(this.enhancedSourceCodeView);
        } catch(Exception e){
            System.out.println("Error: MainWindowController - setConfigurationEnhancedSourceCode - " + e);
        }
    }

    public void setConfigurationMetrics() {
        try {
            this.chartMetricModel = new ChartMetricModel(this.mainWindowModel);
            this.chartMetricView = new ChartMetricView();
            this.chartMetricController = new ChartMetricController(this.chartMetricModel, this.chartMetricView, this);
            this.chartMetricView.getCmbBoxMetrics().addActionListener(this.chartMetricController);
            this.chartMetricView.getExpandBtn().addActionListener(this.chartMetricController);
            this.mainWindowView.getLeftComponent().setRightComponent(this.chartMetricView);
        } catch(Exception e){
            System.out.println("Error: MainWindowController - setConfigurationMetrics - " + e);
        }
    }
    
    public void setConfigurationWeightedElements() {
        try {
            this.weightedElementsModel = new WeightedElementsModel(this.mainWindowModel);
            this.weightedElementsView = new WeightedElementsView();
            this.weightedElementsController = new WeightedElementsController(weightedElementsModel, weightedElementsView, this);
            this.mainWindowView.getRightComponent().setLeftComponent(this.weightedElementsView);
            this.weightedElementsView.getBtnExpandFile().addActionListener(this.weightedElementsController);
            this.weightedElementsController.init();
        } catch(Exception e){
            System.out.println("Error: MainWindowController - setConfigurationWeightedElements - " + e);
        }
    }
        
    public void setConfigurationDocumentation() {
        try {
            this.documentationModel = new DocumentationModel();
            this.documentationView = new DocumentationView();
            this.documentationController = new DocumentationController(this.documentationModel, this.documentationView, this);    
            this.documentationView.getBtnAcceptConclusions().addActionListener(this.documentationController);
            this.documentationView.getBtnDeleteConclusions().addActionListener(this.documentationController);
            this.documentationView.getBtnReadConclusions().addActionListener(this.documentationController);
            this.documentationView.getBtnAcceptQuestions().addActionListener(this.documentationController);
            this.documentationView.getBtnDeleteQuestions().addActionListener(this.documentationController);
            this.documentationView.getBtnReadQuestions().addActionListener(this.documentationController);
            this.documentationView.getBtnExpandConclusions().addActionListener(this.documentationController);
            this.documentationView.getBtnExpandQuestions().addActionListener(this.documentationController);
            this.documentationView.getTxtPaneConclusions().getDocument().addDocumentListener(this.getDocumentationController());
            this.documentationView.getTxtPaneConclusions().getDocument().putProperty("owner", "conclusiones"); 
            this.documentationView.getTxtPaneQuestions().getDocument().addDocumentListener(this.getDocumentationController());
            this.documentationView.getTxtPaneQuestions().getDocument().putProperty("owner", "dudas");
            this.mainWindowView.getRightComponent().setRightComponent(this.documentationView);
            this.documentationController.init();
        } catch(Exception e){
            System.out.println("Error: MainWindowController - setConfigurationDocumentation - " + e);
        }
    }
        
    public void setConfigurationMessageDialog() {
        this.messageDialog = new MessageDialog(this.getMainWindowView(), false);
        this.messageDialogController = new MessageDialogController(this.messageDialog, this);
        this.messageDialog.getBtnAccept().addActionListener(this.messageDialogController);
    }
        
    public void setConfigurationTableDialog() {
        this.tableDialog = new TableDialog(this.getMainWindowView(), false);
        this.tableDialogController = new TableDialogController(this.tableDialog, this);
        this.tableDialog.getBtnAccept().addActionListener(this.tableDialogController);
        this.tableDialog.addWindowListener(this.tableDialogController);
    }

    public void enableComponents(Container container, boolean enable) {
        Component[] components = container.getComponents();
        for (Component component : components) {
            component.setEnabled(enable);
            if (component instanceof Container) {
                enableComponents((Container)component, enable);
            }
        }
    }
    
    public void setLanguage(String nameLanguage){
        try {
            String project = "";
            if (processingProject) {
                 project += " - " + this.getNameProject();
            }
            this.mainWindowView.setTitle(this.language.getProperty("applicationName") + project);
            this.language = new Language(nameLanguage);
            this.getMainWindowView().getStatusBar().setText(this.getLanguage().getProperty("statusBar") + " - " + ProjectProperties.ALUMN);
            this.barController.setLanguage();
            if (this.isProcessingProject()) {
                this.treeDirectoryController.setLanguage();
                this.listElementsController.setLanguage();
                this.enhancedSourceCodeController.setLanguage();
                this.weightedElementsController.setLanguage();
                this.documentationController.setLanguage();
                this.chartMetricController.setLanguage();
            }
        } catch (Exception e) {
            System.out.println("Error: MainWindowController - setLanguage - " + e.toString());
        }
    }
                     
    public void centerScreen() {
        //Toolkit toolkit = this.mainWindowView.getToolkit();      
        //Dimension size = toolkit.getScreenSize();
        //this.mainWindowView.setLocation(size.width / 2 - this.mainWindowView.getWidth() / 2, size.height / 2 - this.mainWindowView.getHeight() / 2);
        this.mainWindowView.setLocationRelativeTo(null);
    }

    public void centerViews() {  
        try {
            this.mainWindowView.getMainPanel().setDividerLocation(0.25);
            this.mainWindowView.getSplitLeft().setDividerLocation(0.6);
            this.mainWindowView.getSplitRight().setDividerLocation(0.5);
            this.mainWindowView.getLeftComponent().setDividerLocation(0.5);
            this.mainWindowView.getRightComponent().setDividerLocation(0.5);
            this.enhancedSourceCodeView.getExpandBtn().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_enlarge.png")));
            switch (this.getMainWindowModel().getAmbitoActual()) {
                case ProjectProperties.FILE:
                    this.weightedElementsView.getBtnExpandFile().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_enlarge.png")));          
                    break;
                case ProjectProperties.CLASS:
                    this.weightedElementsView.getBtnExpandFile().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_enlarge.png")));          
                    break;
                case ProjectProperties.METHOD:
                    this.weightedElementsView.getBtnExpandFile().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_enlarge.png")));
                    break;
            }
            this.chartMetricView.getExpandBtn().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_enlarge.png")));
            this.documentationView.getBtnExpandConclusions().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_enlarge.png")));
            this.documentationView.getBtnExpandQuestions().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_enlarge.png")));
        } catch(Exception e) {
            System.out.println("Error: MainWindowController - centerViews - " + e.getMessage());
        }    
    }
        
    public void showInfoInit() {
        try {
            this.setProcessingProject(true);
            this.setLanguage(this.getLanguage().getActualLanguage());
            this.showInfo();
            this.treeDirectoryController.initView();
            this.barController.initView();
            this.getMainWindowView().requestFocus();
            this.mainWindowView.getMainPanel().setVisible(true);
            this.getMainWindowView().setEnabled(true);            
        } catch (Exception e) {
            System.out.println("Error: MainWindowController - showInfoInit - " + e.getMessage());
        }
    }
    
    public void showInfo() {
        try {
            this.treeDirectoryController.updateActualFileLoc();            
            this.barController.viewActive(mainWindowModel.getAmbitoActual());
            this.listElementsController.show();
            this.enhancedSourceCodeController.show();
            this.weightedElementsController.show();
            this.chartMetricController.show();
        } catch(Exception e) {
            System.out.println("Error: MainWindowController - showInfo - " + e.getMessage());
        }
    }

    public ArrayList<ParameterModel> getArrayParameters(String parameters) {
        ArrayList<ParameterModel> paramatersModel = new ArrayList<ParameterModel>();
        try {
            String[] splitParameters = parameters.split(",");
            int i = 1;
            for (String splitParameter : splitParameters) {
                splitParameter = splitParameter.trim();
                String[] param;
                String matcherBoth = ".*\\[\\].*\\[\\]";
                String matcherMiddle = "[^\\[\\]]*\\[\\][^\\[\\]]+";
                if (splitParameter.matches(matcherBoth) || splitParameter.matches(matcherMiddle)) {
                    param = splitParameter.split("\\[]", 2);
                    param[0] = param[0].replaceAll("\\s+", "") + "[]";
                    param[1] = param[1].replaceAll("\\s+", "");
                } else {
                    param = splitParameter.split(" ", 2);
                    param[0] = param[0].replaceAll("\\s+", "");
                    param[1] = param[1].replaceAll("\\s+", "");                
                }
                paramatersModel.add(new ParameterModel(param[0], param[1].replaceAll("\\s+", ""), i++));
            }
        } catch (Exception e) {
            System.out.println("Error: MainWindowController - getArrayParameters - " + e.getMessage());
        }
        return paramatersModel;
    }
        
    public void clearAnalysis() {
        this.mainWindowModel.clearEnvironment();
        this.mainWindowModel.setAmbitoActual(ProjectProperties.FILE);
    }
    
    public void setButtonCursor(JComponent component, Cursor cursor) {
        for (Component comp : component.getComponents()) {
            if (comp instanceof JButton || comp instanceof JToggleButton) {
                comp.setCursor(cursor);
            } else if (comp instanceof JComponent) {
                setButtonCursor((JComponent)comp, cursor);
            }
        }
    }        

public EnhancedSourceCodeModel getEnhancedSourceCodeModel() {
        return enhancedSourceCodeModel;
    }

    public void setEnhancedSourceCodeModel(EnhancedSourceCodeModel enhancedSourceCodeModel) {
        this.enhancedSourceCodeModel = enhancedSourceCodeModel;
    }

    public EnhancedSourceCodeView getEnhancedSourceCodeView() {
        return enhancedSourceCodeView;
    }

    public void setEnhancedSourceCodeView(EnhancedSourceCodeView enhancedSourceCodeView) {
        this.enhancedSourceCodeView = enhancedSourceCodeView;
    }

    public EnhancedSourceCodeController getEnhancedSourceCodeController() {
        return enhancedSourceCodeController;
    }

    public void setEnhancedSourceCodeController(EnhancedSourceCodeController enhancedSourceCodeController) {
        this.enhancedSourceCodeController = enhancedSourceCodeController;
    }

    public WeightedElementsModel getWeightedElementsModel() {
        return weightedElementsModel;
    }

    public void setWeightedElementsModel(WeightedElementsModel weightedElementsModel) {
        this.weightedElementsModel = weightedElementsModel;
    }

    public WeightedElementsView getWeightedElementsView() {
        return weightedElementsView;
    }

    public void setWeightedElementsView(WeightedElementsView weightedElementsView) {
        this.weightedElementsView = weightedElementsView;
    }

    public ChartMetricModel getChartMetricModel() {
        return chartMetricModel;
    }

    public void setChartMetricModel(ChartMetricModel chartMetricModel) {
        this.chartMetricModel = chartMetricModel;
    }

    public ChartMetricView getChartMetricView() {
        return chartMetricView;
    }

    public void setChartMetricView(ChartMetricView chartMetricView) {
        this.chartMetricView = chartMetricView;
    }

    public ChartMetricController getChartMetricController() {
        return chartMetricController;
    }

    public void setChartMetricController(ChartMetricController chartMetricController) {
        this.chartMetricController = chartMetricController;
    }

    public DocumentationModel getDocumentationModel() {
        return documentationModel;
    }

    public void setDocumentationModel(DocumentationModel documentationModel) {
        this.documentationModel = documentationModel;
    }

    public DocumentationView getDocumentationView() {
        return documentationView;
    }

    public void setDocumentationView(DocumentationView documentationView) {
        this.documentationView = documentationView;
    }

    public DocumentationController getDocumentationController() {
        return documentationController;
    }

    public void setDocumentationController(DocumentationController documentationController) {
        this.documentationController = documentationController;
    }

    public BarController getBarController() {
        return barController;
    }

    public void setBarController(BarController barController) {
        this.barController = barController;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public FileModel getAmbArchivoModel() {
        return ambArchivoModel;
    }

    public boolean isProcessingProject() {
        return processingProject;
    }

    public void setProcessingProject(boolean processingProject) {
        this.processingProject = processingProject;
    }

    public MainWindowModel getMainWindowModel() {
        return mainWindowModel;
    }

    public MainWindowView getMainWindowView() {
        return mainWindowView;
    }

    public TreeDirectoryController getTreeDirectoryController() {
        return treeDirectoryController;
    }

    public String getFolderSelectedParent() {
        return folderSelectedParent;
    }

    public void setFolderSelectedParent(String folderSelectedParent) {
        this.folderSelectedParent = folderSelectedParent;
    }
    
    public MessageDialog getMessageDialog() {
        return messageDialog;
    }

    public MessageDialogController getMessageDialogController() {
        return messageDialogController;
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    public TableDialog getTableDialog() {
        return tableDialog;
    }

    public TableDialogController getTableDialogController() {
        return tableDialogController;
    }    

    public String getNameProject() {
        return nameProject;
    }

    public void setNameProject(String nameProject) {
        this.nameProject = nameProject;
    }
}