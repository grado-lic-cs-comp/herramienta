package controllers;

import dialogs.InfoApplicationDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JButton;
import javax.swing.JComponent;

/**
 * Controlador del diálogo InfoApplicationDialog
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class InfoApplicationDialogController implements ActionListener, WindowListener {
    private MainWindowController mainWindowController = null;
    private InfoApplicationDialog infoApplicationDialog;
    
    public InfoApplicationDialogController(MainWindowController mainWindowController, InfoApplicationDialog infoApplicationDialog) {
        this.mainWindowController = mainWindowController;
        this.infoApplicationDialog = infoApplicationDialog;
    }
    
    public void setLanguageInfoApplicationDialog() {
        this.infoApplicationDialog.setTitle(this.mainWindowController.getLanguage().getProperty("documentationDialogTitle"));
        this.infoApplicationDialog.getDocumentationDialogHeader().setText(this.mainWindowController.getLanguage().getProperty("titleHeader"));
        this.infoApplicationDialog.getEditorPaneDocumentationContent().setText(this.mainWindowController.getLanguage().getProperty("documentationAppContent"));
        this.infoApplicationDialog.getBtnClose().setText(this.mainWindowController.getLanguage().getProperty("closeButton"));
    }

    public void showDialog() {
        this.setLanguageInfoApplicationDialog();
        this.infoApplicationDialog.setLocationRelativeTo(this.mainWindowController.getMainWindowView());
        this.infoApplicationDialog.pack();
        this.infoApplicationDialog.setResizable(false);
        this.mainWindowController.getMainWindowView().setEnabled(false);
        this.infoApplicationDialog.setVisible(true);    
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
        this.mainWindowController.getMainWindowView().toFront();
        this.mainWindowController.getMainWindowView().setEnabled(true);
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object source = e.getSource();
            if (source instanceof JButton) {
                switch (((JComponent)e.getSource()).getName()) {
                    case "btnClose": 
                        this.infoApplicationDialog.dispose();
                        this.mainWindowController.getMainWindowView().setEnabled(true);
                        this.mainWindowController.getMainWindowView().toFront();
                        break;
                }
            }
        } catch(Exception ex) {
            System.out.println("Error: InfoApplicationDialogController - actionPerformed - " + ex.getMessage());
        }
    }    
}