package controllers;
import dialogs.TableDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JButton;
import javax.swing.JComponent;

/**
 * Controlador del diálogo TableDialog
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class TableDialogController implements ActionListener, WindowListener {
    private TableDialog tableDialog;
    private MainWindowController mainWindowController;

    public TableDialogController(TableDialog tableDialog, MainWindowController mainWindowController) {
        this.tableDialog = tableDialog;
        this.mainWindowController = mainWindowController;
    }
    
    public void setLanguageMessageDialog() {
        this.tableDialog.getBtnAccept().setText(this.mainWindowController.getLanguage().getProperty("acceptButton"));
    }
    
    public void showDialog() {
        this.setLanguageMessageDialog();
        this.tableDialog.setLocationRelativeTo(this.mainWindowController.getMainWindowView());
        this.tableDialog.pack();
        this.tableDialog.setResizable(false);
        this.mainWindowController.getMainWindowView().setEnabled(false);
        this.tableDialog.setVisible(true);    
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object source = e.getSource();
            if (source instanceof JButton) {
                switch (((JComponent)e.getSource()).getName()) {
                    case "btnAccept":
                        this.tableDialog.dispose();
                        this.mainWindowController.getMainWindowView().setEnabled(true);
                        this.mainWindowController.getMainWindowView().requestFocus();
                        break;
                }
            }
        } catch(Exception ex) {
            System.out.println("Error en ErrorProcessDialogController - actionPerformed - " + ex.getMessage());
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
        this.mainWindowController.getMainWindowView().toFront();
        this.mainWindowController.getMainWindowView().setEnabled(true);
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }
}