package controllers;

import config.ProjectProperties;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import models.ListElementsModel;
import models.ParameterModel;
import utils.MyListCell;
import views.ListElementsView;

/**
 * Controlador de la vista ListElementsView
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class ListElementsController implements ListSelectionListener, DocumentListener, ActionListener, MouseMotionListener {
    private MainWindowController mainWindowController;
    private ListElementsModel listElementsModel;
    private ListElementsView listElementsView;
    private String type;
    private int size;
    private ArrayList<String> values;

    public ListElementsController(ListElementsModel listElementsModel, ListElementsView listElementsView, MainWindowController mainWindowController) {
        this.listElementsModel = listElementsModel;
        this.listElementsView = listElementsView;
        this.mainWindowController = mainWindowController;
        this.listElementsView.getListElements().setModel(this.listElementsModel.getModel());
        this.listElementsView.getListElements().setCellRenderer(new MyListCell());        
        this.listElementsView.getListElements().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.type = "";
        this.size = 0;
        this.values = new ArrayList<>();
    }    
    
    public void setLanguage() {
        try {
            TitledBorder title = BorderFactory.createTitledBorder(this.mainWindowController.getLanguage().getProperty("titleListElementsArea"));
            this.listElementsView.setBorder(title);
            this.listElementsView.getLblSizeElements().setText(this.mainWindowController.getLanguage().getProperty(type));
            this.listElementsView.getLblTypeElements().setText(this.mainWindowController.getLanguage().getProperty("sizeElements") + ": " + size);
            this.listElementsView.getLblFilterElements().setText(this.mainWindowController.getLanguage().getProperty("lblFilter"));
            this.listElementsView.getBtnClearFilter().setToolTipText(this.mainWindowController.getLanguage().getProperty("clearFilter"));
        } catch (Exception e) {
                System.out.println("Error: ListElementsController - setLanguage - " + e.toString());
        }
    }

    public void updateModel(DefaultListModel model, ArrayList<String> values) {
        if (values.size() > 0) {
            for (String value : values) {
                model.addElement(value);
            }
            this.listElementsView.getListElements().setMinimumSize(new Dimension(0, 0));
            this.listElementsView.getScrollElements().setAutoscrolls(true);
        }    
    }
    
    public void show() {
        try {
            this.clearFilter();
            this.listElementsModel.clearModel();
            String pathFile;
            switch(this.mainWindowController.getMainWindowModel().getAmbitoActual()) {
                case ProjectProperties.FILE:
                    pathFile = this.mainWindowController.getMainWindowModel().getPathFile();
                    values = this.listElementsModel.getListElementsFile(pathFile);
                    this.updateModel(this.listElementsModel.getModel(), values);
                    this.listElementsView.getLblSizeElements().setText(this.mainWindowController.getLanguage().getProperty("elementsClasses"));
                    this.type = "elementsClasses";
                    this.listElementsView.getLblTypeElements().setText(this.mainWindowController.getLanguage().getProperty("sizeElements") + ": " + values.size());
                    this.size = values.size();
                    break;
                case ProjectProperties.CLASS:
                    pathFile = this.mainWindowController.getMainWindowModel().getPathFile();
                    String className = this.mainWindowController.getMainWindowModel().getClassName();
                    values = this.listElementsModel.getListElementsClass(pathFile, className);
                    this.updateModel(this.listElementsModel.getModel(), values);
                    this.listElementsView.getLblSizeElements().setText(this.mainWindowController.getLanguage().getProperty("elementsMethods"));    
                    this.type = "elementsMethods";
                    this.listElementsView.getLblTypeElements().setText(this.mainWindowController.getLanguage().getProperty("sizeElements") + ": " + values.size());    
                    this.size = values.size();
                    break;
                case ProjectProperties.METHOD:
                    int method_id = this.mainWindowController.getMainWindowModel().getMethodId();
                    values = this.listElementsModel.getMethodLocalvariables(method_id);
                    this.updateModel(this.listElementsModel.getModel(), values);
                    this.listElementsView.getLblSizeElements().setText(this.mainWindowController.getLanguage().getProperty("elementsLocalVariables"));
                    this.type = "elementsLocalVariables";
                    this.listElementsView.getLblTypeElements().setText(this.mainWindowController.getLanguage().getProperty("sizeElements") + ": " + values.size());
                    this.size = values.size();
                    break;
            }
        } catch(Exception e) {
            System.out.println("Error: ListElementsController - show - " + e.getMessage());
        }    
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        try {
            int idx = this.listElementsView.getListElements().getSelectedIndex();
            if (idx >= 0) {
            switch (mainWindowController.getMainWindowModel().getAmbitoActual()) {
                case ProjectProperties.FILE:
                    if (this.listElementsView.getListElements().getModel().getSize() > idx) {
                        String nombreClase = this.listElementsView.getListElements().getModel().getElementAt(idx).toString();
                        mainWindowController.getMainWindowModel().setAmbitoActual(ProjectProperties.CLASS);
                        mainWindowController.getMainWindowModel().setClassName(nombreClase);
                        mainWindowController.showInfo();                        
                    }
                    break;
                case ProjectProperties.CLASS:
                    if (this.listElementsView.getListElements().getModel().getSize() > idx) {
                        String nombreMetodoParametros = this.listElementsView.getListElements().getModel().getElementAt(idx).toString();
                        String nombreMetodo = "";
                        String parametros = "";
                        int i;
                        for (i = 0; i < nombreMetodoParametros.length() && nombreMetodoParametros.charAt(i) != '('; i++) {
                            nombreMetodo += nombreMetodoParametros.charAt(i);
                        }
                        for (; i < nombreMetodoParametros.length(); i++) {
                            parametros += nombreMetodoParametros.charAt(i);
                        }
                        mainWindowController.getMainWindowModel().setAmbitoActual(ProjectProperties.METHOD);
                        mainWindowController.getMainWindowModel().setMethodName(nombreMetodo);
                        parametros = parametros.replace("(", "").replace(")", "");
                        ArrayList<ParameterModel> paramatersModel = new ArrayList<ParameterModel>();
                        if (parametros.trim().length() > 0) {
                            paramatersModel = this.mainWindowController.getArrayParameters(parametros);
                        }
                        mainWindowController.getMainWindowModel().setParameters(paramatersModel);
                        mainWindowController.showInfo();
                    }
                    break;
                }                
            }
        } catch (Exception ex) {
            System.out.println("Error: ListElementsController - propertyChange - " + ex.toString());
        }    
    }
    
    @Override
    public void insertUpdate(DocumentEvent e) {
        this.filter();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.filter();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        this.filter();
    }
    
    private void filter() {
        this.listElementsModel.getModel().clear();
        ArrayList<String> copyValues = (ArrayList<String>)values.clone();
        this.filterValues(copyValues, this.listElementsView.getFilterElements().getText());
        this.updateModel(this.listElementsModel.getModel(), copyValues);
    }
    
    public void filterValues(ArrayList<String> copyValues, String filter) {
        for (int i = 0; i < this.values.size(); i++) {
            if (!values.get(i).toLowerCase().contains(filter.toLowerCase())) {
                copyValues.remove(values.get(i));
            }
        }
    }    

    public void clearFilter() {
        this.listElementsView.getFilterElements().setText("");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object source = e.getSource();
            if (source instanceof JButton) {
                switch (((JComponent) e.getSource()).getName()) {
                    case "btnClearFilter":
                        this.clearFilter();
                        this.show();
                        break;
                }
            }
        } catch (Exception ex) {
            System.out.println("Error: EnhancedSourceCodeController - actionPerformed - " + ex.getMessage());
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        final int x = e.getX();
        final int y = e.getY();
        // only display a hand if the cursor is over the items
        final Rectangle cellBounds = this.listElementsView.getListElements().getCellBounds(0, this.listElementsView.getListElements().getModel().getSize() - 1);
        if (cellBounds != null && cellBounds.contains(x, y)) {
            if (!this.mainWindowController.getMainWindowModel().getAmbitoActual().equals(ProjectProperties.METHOD)) {
                this.listElementsView.getListElements().setCursor(new Cursor(Cursor.HAND_CURSOR));
            } else {
                this.listElementsView.getListElements().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        } else {
            this.listElementsView.getListElements().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }    
    }
}