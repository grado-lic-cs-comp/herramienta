package controllers;

import config.ProjectProperties;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import models.DocumentationModel;
import views.DocumentationView;

/**
 * Controlador de la vista DocumentationView
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class DocumentationController implements ActionListener, DocumentListener, PropertyChangeListener {
    private DocumentationModel documentationModel;
    private DocumentationView documentationView;
    private MainWindowController mainWindowController;
    private boolean isExpanded;

    public DocumentationController(DocumentationModel documentationModel, DocumentationView documentationView, MainWindowController mainController) {
        this.documentationModel = documentationModel;
        this.documentationView = documentationView;
        this.mainWindowController = mainController;
        this.isExpanded = false;
    }
    
    public void setLanguage() {
        try {
            TitledBorder title = BorderFactory.createTitledBorder(this.mainWindowController.getLanguage().getProperty("titleDocumentationArea"));
            this.documentationView.setBorder(title);
            this.documentationView.getTabbedPaneDocumentation().setTitleAt(0, this.mainWindowController.getLanguage().getProperty("titleTabConclusions"));
            title = BorderFactory.createTitledBorder(this.mainWindowController.getLanguage().getProperty("messageLabelConclusions"));
            this.documentationView.getScrollPaneConclusions().setBorder(title);
            this.documentationView.getTabbedPaneDocumentation().setTitleAt(1, this.mainWindowController.getLanguage().getProperty("titleTabQuestions"));
            title = BorderFactory.createTitledBorder(this.mainWindowController.getLanguage().getProperty("messageLabelQuestions"));
            this.documentationView.getScrollPaneQuestions().setBorder(title);
            this.documentationView.getBtnAcceptConclusions().setToolTipText(this.mainWindowController.getLanguage().getProperty("saveDocument"));
            this.documentationView.getBtnDeleteConclusions().setToolTipText(this.mainWindowController.getLanguage().getProperty("deleteText"));
            this.documentationView.getBtnReadConclusions().setToolTipText(this.mainWindowController.getLanguage().getProperty("showConclusions"));
            this.documentationView.getBtnAcceptQuestions().setToolTipText(this.mainWindowController.getLanguage().getProperty("saveDocument"));
            this.documentationView.getBtnDeleteQuestions().setToolTipText(this.mainWindowController.getLanguage().getProperty("deleteText"));
            this.documentationView.getBtnReadQuestions().setToolTipText(this.mainWindowController.getLanguage().getProperty("showQuestions"));
            if (isExpanded) {
                this.documentationView.getBtnExpandConclusions().setToolTipText(this.mainWindowController.getLanguage().getProperty("collapseArea"));
                this.documentationView.getBtnExpandQuestions().setToolTipText(this.mainWindowController.getLanguage().getProperty("collapseArea"));
            } else {
                this.documentationView.getBtnExpandConclusions().setToolTipText(this.mainWindowController.getLanguage().getProperty("expandArea"));
                this.documentationView.getBtnExpandQuestions().setToolTipText(this.mainWindowController.getLanguage().getProperty("expandArea"));            
            }
        } catch (Exception e) {
            System.out.println("Error: DocumentationController - setLanguage - " + e.toString());
        }
    }
    
    public void init() {
        this.documentationView.getBtnAcceptConclusions().setEnabled(false);
        this.documentationView.getBtnDeleteConclusions().setEnabled(false);
        this.documentationView.getTxtPaneConclusions().setText("");
        this.documentationView.getBtnAcceptQuestions().setEnabled(false);
        this.documentationView.getBtnDeleteQuestions().setEnabled(false);
        this.documentationView.getTxtPaneQuestions().setText("");    
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object source = e.getSource();
            if (source instanceof JButton) {
                switch (((JComponent)e.getSource()).getName()) {
                    case "btnAcceptConclusions": 
                        this.saveConclusions();
                        break;
                    case "btnDeleteConclusions":
                        this.deleteTxtConclusions();
                        break;
                    case "btnReadConclusions":
                        this.readTxtConclusions();
                        break;
                    case "btnAcceptQuestions": 
                        this.saveQuestions();
                        break;
                    case "btnDeleteQuestions":
                        this.deleteTxtQuestions();
                        break;
                    case "btnReadQuestions":
                        this.readTxtQuestions();
                        break;
                    case "btnExpandConclusions":
                        if (isExpanded) {
                            this.collapse("conclusions");
                        } else {
                            this.expand("conclusions");                    
                        }
                        break;
                    case "btnExpandQuestions":
                        if (isExpanded) {
                            this.collapse("questions");
                        } else {
                            this.expand("questions");                    
                        }
                        break;
                }
            }
        } catch(Exception ex) {
            System.out.println("Error: DocumentationController - actionPerformed - " + ex.getMessage());
        }
    }

    public void saveConclusions() {
        FileWriter fileWriter;
        BufferedWriter bufferedWriter;
        PrintWriter printWriter;
        String header = "-------------------------------------------------------------------";
        boolean isFileCreated = false;
        try {
            File path = new File(this.mainWindowController.getBarController().getFolderSelectionDialogController().getRutaCarpetaDestino() + this.mainWindowController.getNameProject() + "/");
            File fileConclusions = new File(
                path, 
                this.mainWindowController.getLanguage().getProperty("fileNameConclusions")
            );
            if (!fileConclusions.exists()) {
                if (!path.exists()) {
                    path.mkdirs();
                }
                fileConclusions.createNewFile();
            }
            fileWriter = new FileWriter(fileConclusions, true);
            bufferedWriter = new BufferedWriter(fileWriter);
            printWriter = new PrintWriter(bufferedWriter);
            if (this.documentationView.getTxtPaneConclusions().getText().length() == 0) {
                printWriter.println(header);            
            }
            printWriter.println(this.mainWindowController.getLanguage().getProperty("file") + ": " + this.mainWindowController.getMainWindowModel().getPathFile());
            printWriter.println(this.mainWindowController.getLanguage().getProperty("conclusion") + ":\n" + this.documentationView.getTxtPaneConclusions().getText());
            printWriter.println(header);
            printWriter.close();
            fileWriter.close();
            this.mainWindowController.getMessageDialog().setTitle(this.mainWindowController.getLanguage().getProperty("documentConclusionSaveTitle"));
            this.mainWindowController.getMessageDialog().getLblTitle().setText(this.mainWindowController.getLanguage().getProperty("documentSavedSuccessfully"));
            this.mainWindowController.getMessageDialog().getTxtAreaContent().setText(fileConclusions.getAbsolutePath());
            Image logo = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/64_file-text2.png"));
            ImageIcon imageIcon1 = new ImageIcon(logo);
            this.mainWindowController.getMessageDialog().getIcon().setIcon(imageIcon1);
            this.mainWindowController.getMessageDialogController().showDialog();
            this.deleteTxtConclusions();
        } catch (HeadlessException | IOException e) {
            System.out.println("Error: DocumentacionController - recordConclusions - " + e.toString());
        }
    }

    protected ImageIcon createImageIcon(String path, String description) {
        java.net.URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL, description);
        } else {
            System.err.println("No encontro el archivo: " + path);
            return null;
        }
    }
    
    public void saveQuestions() {
        FileWriter fileWriter;
        BufferedWriter bufferedWriter;
        PrintWriter printWriter;
        String header = "-------------------------------------------------------------------";
        boolean isFileCreated = false;
        try {
            File path = new File(this.mainWindowController.getBarController().getFolderSelectionDialogController().getRutaCarpetaDestino() + this.mainWindowController.getNameProject() + "/");
            File fileQuestions = new File(
                path,
                this.mainWindowController.getLanguage().getProperty("fileNameQuestions")
            );
            if (!fileQuestions.exists()) {
                if (!path.exists()) {
                    path.mkdirs();
                }
                fileQuestions.createNewFile();                
            }
            fileWriter = new FileWriter(fileQuestions, true);
            bufferedWriter = new BufferedWriter(fileWriter);
            printWriter = new PrintWriter(bufferedWriter);
            if (this.documentationView.getTxtPaneQuestions().getText().length() == 0) {
                printWriter.println(header);            
            }
            printWriter.println(this.mainWindowController.getLanguage().getProperty("file") + ": " + this.mainWindowController.getMainWindowModel().getPathFile());
            printWriter.println(this.mainWindowController.getLanguage().getProperty("question") + ":\n" + this.documentationView.getTxtPaneQuestions().getText());
            printWriter.println(header);
            printWriter.close();
            fileWriter.close();
            this.mainWindowController.getMessageDialog().setTitle(this.mainWindowController.getLanguage().getProperty("documentQuestionsSaveTitle"));
            this.mainWindowController.getMessageDialog().getLblTitle().setText(this.mainWindowController.getLanguage().getProperty("documentSavedSuccessfully"));
            this.mainWindowController.getMessageDialog().getTxtAreaContent().setText(fileQuestions.getAbsolutePath());
            Image logo = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/64_file-text2.png"));
            ImageIcon imageIcon1 = new ImageIcon(logo);
            this.mainWindowController.getMessageDialog().getIcon().setIcon(imageIcon1);
            this.mainWindowController.getMessageDialogController().showDialog();
            this.deleteTxtQuestions();
        } catch (HeadlessException | IOException e) {
            System.out.println("Error: DocumentacionController -  recordQuestions - " + e.toString());
        }
    }
    
    public void deleteTxtConclusions() {
        this.documentationView.getTxtPaneConclusions().setText("");
        this.documentationView.getBtnAcceptConclusions().setEnabled(false);
        this.documentationView.getBtnDeleteConclusions().setEnabled(false);
    }

    public void readTxtConclusions() {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        String content = "";
        try {
            File path = new File(this.mainWindowController.getBarController().getFolderSelectionDialogController().getRutaCarpetaDestino() + this.mainWindowController.getNameProject() + "/");
            File file = new File(
                path,
                this.mainWindowController.getLanguage().getProperty("fileNameConclusions")
            );
            if (file.exists()) {
                fr = new FileReader (file.getAbsoluteFile());
                br = new BufferedReader(fr);
                String line;
                while((line = br.readLine())!= null) {
                    content+= line + "\n";
                }
            }
            this.mainWindowController.getMessageDialog().setTitle(this.mainWindowController.getLanguage().getProperty("documentConclusionsReadTitle"));
            this.mainWindowController.getMessageDialog().getLblTitle().setText(this.mainWindowController.getLanguage().getProperty("documentConclusionsReadSubtitle"));
            this.mainWindowController.getMessageDialog().getTxtAreaContent().setText(content.isEmpty()?this.mainWindowController.getLanguage().getProperty("conclusionsEmpty"):content);
            Image logo = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/64_show_content_file.png"));
            ImageIcon imageIcon1 = new ImageIcon(logo);
            this.mainWindowController.getMessageDialog().getIcon().setIcon(imageIcon1);
            this.mainWindowController.getMessageDialogController().showDialog();
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try {                    
                if( null != fr ) {   
                    fr.close();     
                }                  
            } catch (Exception e2) { 
                e2.printStackTrace();
            }
        }
    }
    
    public void deleteTxtQuestions() {
        this.documentationView.getTxtPaneQuestions().setText("");    
        this.documentationView.getBtnAcceptQuestions().setEnabled(false);
        this.documentationView.getBtnDeleteQuestions().setEnabled(false);
    }
    
    public void readTxtQuestions() {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        String content = "";
        try {
            File path = new File(this.mainWindowController.getBarController().getFolderSelectionDialogController().getRutaCarpetaDestino() + this.mainWindowController.getNameProject() + "/");
            File file = new File(
                path,
                this.mainWindowController.getLanguage().getProperty("fileNameQuestions")
            );
            if (file.exists()) {
                fr = new FileReader (file.getAbsoluteFile());
                br = new BufferedReader(fr);
                String line;
                while((line = br.readLine())!= null) {
                    content+= line + "\n";
                }
            }
            this.mainWindowController.getMessageDialog().setTitle(this.mainWindowController.getLanguage().getProperty("documentQuestionsReadTitle"));
            this.mainWindowController.getMessageDialog().getLblTitle().setText(this.mainWindowController.getLanguage().getProperty("documentQuestionsReadSubtitle"));
            this.mainWindowController.getMessageDialog().getTxtAreaContent().setText(content.isEmpty()?this.mainWindowController.getLanguage().getProperty("questionsEmpty"):content);
            Image logo = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/64_show_content_file.png"));
            ImageIcon imageIcon1 = new ImageIcon(logo);
            this.mainWindowController.getMessageDialog().getIcon().setIcon(imageIcon1);
            this.mainWindowController.getMessageDialogController().showDialog();        
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try {                    
                if( null != fr ) {   
                    fr.close();     
                }                  
            } catch (Exception e2) { 
                e2.printStackTrace();
            }
        }
    }

    public void changed(DocumentEvent e) {
        try {
            Object owner = e.getDocument().getProperty("owner");
            switch(owner.toString()) {
                case "conclusiones":
                    if (this.documentationView.getTxtPaneConclusions().getText().equals("")) {
                        this.documentationView.getBtnAcceptConclusions().setEnabled(false);
                        this.documentationView.getBtnDeleteConclusions().setEnabled(false);
                    } else {
                        this.documentationView.getBtnAcceptConclusions().setEnabled(true);
                        this.documentationView.getBtnDeleteConclusions().setEnabled(true);
                    }
                    break;
                case "dudas":
                    if (this.documentationView.getTxtPaneQuestions().getText().equals("")) {
                        this.documentationView.getBtnAcceptQuestions().setEnabled(false);
                        this.documentationView.getBtnDeleteQuestions().setEnabled(false);
                    } else {
                        this.documentationView.getBtnAcceptQuestions().setEnabled(true);
                        this.documentationView.getBtnDeleteQuestions().setEnabled(true);
                    }
                    break;
            }
        } catch(Exception ex) {
            System.out.println("Error en DocumentationController - changed  - " + ex);
        }
    }

    public void expand(String area) {
        this.isExpanded = true;
        this.mainWindowController.getMainWindowView().getSplitRight().setDividerLocation(0.0);
        this.mainWindowController.getMainWindowView().getRightComponent().setDividerLocation(0.0);
        switch (area) {
            case "conclusions":
                this.documentationView.getBtnExpandConclusions().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_shrink.png")));        
                this.documentationView.getBtnExpandConclusions().setToolTipText(this.mainWindowController.getLanguage().getProperty("collapseArea"));
                break;
            case "questions":
                this.documentationView.getBtnExpandQuestions().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_shrink.png")));        
                this.documentationView.getBtnExpandQuestions().setToolTipText(this.mainWindowController.getLanguage().getProperty("collapseArea"));
                break;
        }
        this.mainWindowController.getMainWindowView().repaint();
        this.mainWindowController.getMainWindowView().revalidate();
    }

    public void collapse(String area) {
        this.isExpanded = false;
        this.mainWindowController.getMainWindowView().getSplitRight().setDividerLocation(this.mainWindowController.getMainWindowView().getSplitRight().getLastDividerLocation());
        this.mainWindowController.getMainWindowView().getRightComponent().setDividerLocation(this.mainWindowController.getMainWindowView().getLeftComponent().getLastDividerLocation());
        switch (area) {
            case "conclusions":
                this.documentationView.getBtnExpandConclusions().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_enlarge.png")));
                this.documentationView.getBtnExpandConclusions().setToolTipText(this.mainWindowController.getLanguage().getProperty("expandArea"));
                break;
            case "questions":
                this.documentationView.getBtnExpandQuestions().setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/24_enlarge.png")));
                this.documentationView.getBtnExpandQuestions().setToolTipText(this.mainWindowController.getLanguage().getProperty("expandArea"));
                break;
        }
        this.mainWindowController.getMainWindowView().repaint();
        this.mainWindowController.getMainWindowView().revalidate();
    }
        
    @Override
    public void insertUpdate(DocumentEvent e) {
        changed(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changed(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        changed(e);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
    }
}