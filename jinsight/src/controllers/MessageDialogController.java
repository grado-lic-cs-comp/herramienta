package controllers;

import dialogs.MessageDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComponent;

/**
 * Controlador del diálogo MessageDialog
 * @version 1.0.0.0
 * @author Ceballos Arnaldo Raúl
 */
public class MessageDialogController implements ActionListener {

    private MessageDialog messageDialog;
    private MainWindowController mainWindowController;
    
    public MessageDialogController(MessageDialog messageDialog, MainWindowController mainWindowController) {
        this.messageDialog = messageDialog;
        this.mainWindowController = mainWindowController;
    }
    
    public void setLanguageMessageDialog() {
        this.messageDialog.getBtnAccept().setText(this.mainWindowController.getLanguage().getProperty("acceptButton"));
    }
    
    public void showDialog() {
        this.setLanguageMessageDialog();
        this.messageDialog.setLocationRelativeTo(this.mainWindowController.getMainWindowView());
        this.messageDialog.pack();
        this.messageDialog.setResizable(false);
        this.mainWindowController.getMainWindowView().setEnabled(false);
        this.messageDialog.setVisible(true);    
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object source = e.getSource();
            if (source instanceof JButton) {
                switch (((JComponent)e.getSource()).getName()) {
                    case "btnAccept":
                        this.messageDialog.dispose();
                        this.mainWindowController.getMainWindowView().setEnabled(true);
                        this.mainWindowController.getMainWindowView().requestFocus();
                        break;
                }
            }
        } catch(Exception ex) {
            System.out.println("Error en ErrorProcessDialogController - actionPerformed - " + ex.getMessage());
        }
    }
}