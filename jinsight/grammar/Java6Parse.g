parser grammar Java6Parse;

options { 
    tokenVocab = Java6Lex;
    backtrack = true;
    memoize = true;
    language = Java;
}

@parser::header {
    package compiler;
    import models.*;
    import controllers.*;
    import utils.FilesUtil;
}

@parser::members { 
    @Override    
    public void displayRecognitionError(String[] tokenNames, RecognitionException e) {
        String line = getErrorHeader(e);
        String msg = getErrorMessage(e, tokenNames);
        throw new RuntimeException(line.split(":")[0].split(" ")[1] + " - " + msg);
    }
}

/********************************************************************************************
                          Parser section
*********************************************************************************************/

compilationUnit[FileModel fileModelParam]
    scope{
        FileModel fileModel;
    } 
    @init{
        $compilationUnit::fileModel = $fileModelParam;
        String html = "";
    }
    @after{
        $compilationUnit::fileModel.setHtml("<html><head></head><body class=\"bodyesc\">"+html+"</body></html>");
        $compilationUnit::fileModel.setLoc(FilesUtil.countLinesHtml(html));
    }
    :   (   (annotations { html+= $annotations.html; }
            )?
            packageDeclaration { html+= $packageDeclaration.html; }
                               { $compilationUnit::fileModel.setPaquete($packageDeclaration.pathPackage); }
        )?
        (importDeclaration { html+= $importDeclaration.html; } 
        )*
        (typeDeclaration { html+= $typeDeclaration.html; }
        )*
    ;

packageDeclaration returns [String html="" , String codigoJava="", String pathPackage=""]  
    :   PACKAGE { $html+= "<font class=\"reservedesc\">package</font>"; }
        { $codigoJava+= "package "; }
                ( SPACE    { $html+= "&nbsp;"; }
                | NEWLINE { $html+= "<br>"; }
                | FORMFEED { $html+= "&nbsp;"; }
                | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                )*
        qualifiedName { $html+= $qualifiedName.html; }
                      { $pathPackage= $qualifiedName.codigoJava; }   
        SEMI { $html+= ";"; }
             { $codigoJava+= ";"; }
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
    ;

importDeclaration returns [String html="", String codigoJava=""] 
    :   IMPORT { $html+= "<font class=\"reservedesc\">import</font>"; }
               { $codigoJava+= "import "; }
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )* 
        (STATIC { $html+= "<font class=\"reservedesc\">static</font>"; } 
                { $codigoJava+= "static "; }
                ( SPACE    { $html+= "&nbsp;"; }
                | NEWLINE { $html+= "<br>"; }
                | FORMFEED { $html+= "&nbsp;"; }
                | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                )*  
        )?
        IDENTIFIER { $html+= $IDENTIFIER.text; }
                   { $codigoJava+= $IDENTIFIER.text; }
                   ( SPACE    
                   | NEWLINE 
                   | FORMFEED    
                   | TABULATION 
                   )* 
        DOT  { $html+= "."; } 
             { $codigoJava+= "."; }
             ( SPACE    
             | NEWLINE 
             | FORMFEED    
             | TABULATION 
             )*    
        STAR { $html+= ".*"; } 
             { $codigoJava+= ".*"; }     
             ( SPACE    
             | NEWLINE 
             | FORMFEED    
             | TABULATION 
             )*
        SEMI { $html+= ";"; } 
             { $codigoJava+= ";"; }     
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*
    |   IMPORT  { $html+= "<font class=\"reservedesc\">import</font>"; }
                { $codigoJava+= "import "; }
                ( SPACE    { $html+= "&nbsp;"; }
                | NEWLINE { $html+= "<br>"; }
                | FORMFEED { $html+= "&nbsp;"; }
                | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                )*
        (STATIC { $html+= "<font class=\"reservedesc\">static</font>"; } 
                { $codigoJava+= "static "; }
                ( SPACE    { $html+= "&nbsp;"; }
                | NEWLINE { $html+= "<br>"; }
                | FORMFEED { $html+= "&nbsp;"; }
                | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                )*
        )?
        ID1 = IDENTIFIER { $html+= $ID1.text; } 
                         { $codigoJava+= $ID1.text; }     
                         ( SPACE    
                         | NEWLINE 
                         | FORMFEED    
                         | TABULATION 
                         )*
        (   DOT { $html+= "."; } 
                { $codigoJava+= "."; } 
                ( SPACE    
                | NEWLINE 
                | FORMFEED    
                | TABULATION 
                )*
            ID2 = IDENTIFIER { $html+= $ID2.text; }
                             { $codigoJava+= $ID2.text; }   
                             ( SPACE    { $html+= "&nbsp;"; }
                             | NEWLINE { $html+= "<br>"; }
                             | FORMFEED { $html+= "&nbsp;"; }
                             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                             )*
        )+
        (DOT { $html+= "."; }
             { $codigoJava+= "."; }
             ( SPACE    
             | NEWLINE 
             | FORMFEED    
             | TABULATION 
             )*
         STAR { $html+= "*"; }
              { $codigoJava+= "*"; }
              ( SPACE    
              | NEWLINE 
              | FORMFEED    
              | TABULATION 
              )*
        )?
        SEMI { $html+= ";"; }
             { $codigoJava+= ";"; }
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
    ;

qualifiedImportName returns [String html="", String codigoJava=""] 
    :   ID1 = IDENTIFIER { $html+= $ID1.text; }
                         { $codigoJava+= $ID1.text; }
                         ( SPACE    
                         | NEWLINE 
                         | FORMFEED    
                         | TABULATION 
                         )*
        (DOT { $html+= "."; } 
             { $codigoJava+= "."; }
             ( SPACE    
             | NEWLINE 
             | FORMFEED    
             | TABULATION 
             )* 
         ID2 = IDENTIFIER { $html+= $ID2.text; }
                          { $codigoJava+= $ID2.text; }
                          ( SPACE    { $html+= "&nbsp;"; }
                          | NEWLINE { $html+= "<br>"; }
                          | FORMFEED { $html+= "&nbsp;"; }
                          | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                          )*
        )*
    ;

typeDeclaration returns [String html="", String codigoJava=""] 
    :   classOrInterfaceDeclaration { $html+= $classOrInterfaceDeclaration.html; }                            
                                    { $codigoJava+= $classOrInterfaceDeclaration.codigoJava; }
    |   SEMI { $html+= ";"; }
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*
    ;

classOrInterfaceDeclaration returns [String html="", String codigoJava=""] 
    :   
    modifiers { $html+= $modifiers.html; }
              { $codigoJava+= $modifiers.codigoJava; }             
    ( classDeclaration[$modifiers.html] { $html+= $classDeclaration.html; }                            
                         { $codigoJava+= $classDeclaration.codigoJava; }  
    |   { throw new RuntimeException("interfaceIgnore"); }
        interfaceDeclaration[$modifiers.codigoJava] { $html+= $interfaceDeclaration.html; }
                           { $codigoJava+= $interfaceDeclaration.codigoJava; }   
    )
    ;


modifiers returns [String html="", String codigoJava=""] 
    :
    (
        (   annotation { $html+= $annotation.html; }
                       { $codigoJava+= $annotation.codigoJava; } 
        |   PUBLIC { $html+= "<font class=\"reservedesc\">" + $PUBLIC.text + "</font>"; } 
                   { $codigoJava+= "public "; }
        |   PROTECTED { $html+= "<font class=\"reservedesc\">" + $PROTECTED.text + "</font>"; }
                      { $codigoJava+= "protected "; }
        |   PRIVATE { $html+= "<font class=\"reservedesc\">" + $PRIVATE.text + "</font>"; }
                    { $codigoJava+= "private "; }
        |   STATIC { $html+= "<font class=\"reservedesc\">" + $STATIC.text + "</font>"; }
                   { $codigoJava+= "static "; }
        |   ABSTRACT { $html+= "<font class=\"reservedesc\">" + $ABSTRACT.text + "</font>"; }
                     { $codigoJava+= "abstract "; }
        |   FINAL { $html+= "<font class=\"reservedesc\">" + $FINAL.text + "</font>"; }
                  { $codigoJava+= "final "; }
        |   NATIVE { $html+= "<font class=\"reservedesc\">" + $NATIVE.text + "</font>"; }
                   { $codigoJava+= "native "; }
        |   SYNCHRONIZED { $html+= "<font class=\"reservedesc\">" + $SYNCHRONIZED.text + "</font>"; }
                         { $codigoJava+= "synchronized "; }
        |   TRANSIENT { $html+= "<font class=\"reservedesc\">" + $TRANSIENT.text + "</font>"; }
                      { $codigoJava+= "transient "; }
        |   VOLATILE { $html+= "<font class=\"reservedesc\">" + $VOLATILE.text + "</font>"; }
                     { $codigoJava+= "volatile "; }
        |   STRICTFP { $html+= "<font class=\"reservedesc\">" + $STRICTFP.text + "</font>"; }
                     { $codigoJava+= "strictfp "; }
        )
        ( SPACE    { $html+= "&nbsp;"; }
        | NEWLINE { $html+= "<br>"; }
        | FORMFEED { $html+= "&nbsp;"; }
        | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
        )*
    )*
    ;


variableModifiers returns [String html="", String codigoJava=""] 
    :   (   FINAL { $html+= "<font class=\"reservedesc\">final</font>"; } 
                  { $codigoJava+= "final "; }   
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
        |   annotation { $html+= $annotation.html; }
                       { $codigoJava+= $annotation.codigoJava; }
        )*
    ;


classDeclaration[String modifiers] returns [String html="", String codigoJava=""] 
    scope {
        String mod;
    }
    @init{
        $classDeclaration::mod= "";
    }
    :   {$classDeclaration::mod = modifiers;}
        normalClassDeclaration[$classDeclaration::mod] { $html+= $normalClassDeclaration.html; }
    |   enumDeclaration { throw new RuntimeException("enumIgnore"); }
                        { $html+= $enumDeclaration.html; }
    ;

normalClassDeclaration[String modifiers] returns [String html=""] 
    scope {
        boolean flagNormalClassDeclaration;
        boolean flagMethodDeclaration;
        boolean flagNormalClassDeclarationBody;
        ClassModel classModel;
        ClassAttributeModel classAttributeModel;
    }
    @init{
        $normalClassDeclaration::flagNormalClassDeclaration= true;
        $normalClassDeclaration::flagMethodDeclaration= false;
        $normalClassDeclaration::classModel= new ClassModel();
    }
    @after{
        $normalClassDeclaration::flagNormalClassDeclaration= false;
    }
    :   CLASS { $html+= "<font class=\"reservedesc\">class</font>"; }
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*

        IDENTIFIER  { $normalClassDeclaration::classModel.setName($IDENTIFIER.text); }
                    { $html+= "<a class=\""+$IDENTIFIER.text.toLowerCase()+"\" name=\""+$IDENTIFIER.text+"\" href=\"class\">"+$IDENTIFIER.text + "</a>"; }
                    ( SPACE    { $html+= "&nbsp;"; }
                    | NEWLINE { $html+= "<br>"; }
                    | FORMFEED { $html+= "&nbsp;"; }
                    | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                    )*
        (typeParameters { $html+= $typeParameters.html; }
        )?
        (EXTENDS { $html+= "<font class=\"reservedesc\">extends</font>"; }
                 ( SPACE    { $html+= "&nbsp;"; }
                 | NEWLINE { $html+= "<br>"; }
                 | FORMFEED { $html+= "&nbsp;"; }
                 | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                 )*
         type { $html+= $type.html; }
        )?
        (IMPLEMENTS { $html+= "<font class=\"reservedesc\">implements</font>"; }
                    ( SPACE    { $html+= "&nbsp;"; }
                    | NEWLINE { $html+= "<br>"; }
                    | FORMFEED { $html+= "&nbsp;"; }
                    | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                    )*   
         typeList { $html+= $typeList.html; } 
        )?
        classBody { $html+= $classBody.html; }
        { $normalClassDeclaration::classModel.setHtml("<html><head></head><body class=\"bodyesc\">"+$modifiers + " " + $html+"</body></html>"); }
        { $normalClassDeclaration::classModel.setLoc(FilesUtil.countLinesHtml($html));}
        { $compilationUnit::fileModel.addClases($normalClassDeclaration::classModel); }
    ;


typeParameters returns [String html="", String codigoJava=""] 
    :   LT { $html+= "&lt;"; } 
           { $codigoJava+= "&lt;"; }  
           ( SPACE    { $html+= "&nbsp;"; }
           | NEWLINE { $html+= "<br>"; }
           | FORMFEED { $html+= "&nbsp;"; }
           | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
           )*
        TP1 = typeParameter { $html+= $TP1.html; } 
                            { $codigoJava+= $TP1.codigoJava; }
        (COMMA { $html+= ","; } 
               { $codigoJava+= ","; }  
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*   
         TP2 = typeParameter { $html+= $TP2.html; }
                             { $codigoJava+= $TP2.codigoJava; }
        )*
        GT { $html+= ">"; } 
           { $codigoJava+= ">"; }
           ( SPACE    
           | NEWLINE 
           | FORMFEED    
           | TABULATION 
           )*
        { $html+= "&nbsp;"; }   
        { $codigoJava+= " ";}
    ;

typeParameter returns [String html="", String codigoJava=""] 
    :   IDENTIFIER { $html+= $IDENTIFIER.text; }
                   { $codigoJava+= $IDENTIFIER.text; } 
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )*
        (EXTENDS { $html+= "<font class=\"reservedesc\">extends</font>"; }
                 { $codigoJava+= "extends "; }   
                 ( SPACE    { $html+= "&nbsp;"; }
                 | NEWLINE { $html+= "<br>"; }
                 | FORMFEED { $html+= "&nbsp;"; }
                 | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                 )*  
         typeBound { $html+= $typeBound.html; }
                   { $codigoJava+= $typeBound.codigoJava; }
        )?
    ;


typeBound returns [String html="", String codigoJava=""] 
    :   T1 = type { $html+= $T1.html; }
                  { $codigoJava+= $T1.codigoJava; }
        (AMP { $html+= "&"; }
             { $codigoJava+= "&"; }      
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*  
         T2 = type { $html+= $T2.html; }
                   { $codigoJava+= $T2.codigoJava; } 
        )*
    ;


enumDeclaration returns [String html="", String codigoJava=""] 
    :   (ENUM { $html+= "<font class=\"reservedesc\">enum</font>"; }
              { $codigoJava+= "enum "; }       
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*  
        )
        IDENTIFIER { $html+= $IDENTIFIER.text; }
                   { $codigoJava+= $IDENTIFIER.text; }  
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )*
        (IMPLEMENTS { $html+= "<font class=\"reservedesc\">implements</font>"; } 
                    { $codigoJava+= "implements "; }           
                    ( SPACE    { $html+= "&nbsp;"; }
                    | NEWLINE { $html+= "<br>"; }
                    | FORMFEED { $html+= "&nbsp;"; }
                    | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                    )*   
         typeList { $html+= $typeList.html; }
                  { $codigoJava+= $typeList.codigoJava; }  
        )?
        enumBody { $html+= $enumBody.html; }
                 { $codigoJava+= $enumBody.codigoJava; }
    ;


enumBody returns [String html="", String codigoJava=""] 
    :   LBRACE { $html+= "{"; }
               { $codigoJava+= "{"; }               
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
        (enumConstants { $html+= $enumConstants.html; }
                       { $codigoJava+= $enumConstants.codigoJava; }
        )?
        (COMMA { $html+= ","; }
               { $codigoJava+= ","; }
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
        )?
        (enumBodyDeclarations { $html+= $enumBodyDeclarations.html; }
                              { $codigoJava+= $enumBodyDeclarations.codigoJava; } 
        )?
        RBRACE { $html+= "}"; }
               { $codigoJava+= "}"; }               
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
    ;

enumConstants returns [String html="", String codigoJava=""] 
    :   EC1 = enumConstant { $html+= $EC1.html; }
                           { $codigoJava+= $EC1.codigoJava; } 
        (COMMA { $html+= ","; } 
               { $codigoJava+= ","; } 
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
         EC2 = enumConstant { $html+= $EC2.html; }
                            { $codigoJava+= $EC2.codigoJava; }
        )*
    ;

/**
 * NOTE: here differs from the javac grammar, missing TypeArguments.
 * EnumeratorDeclaration = AnnotationsOpt [TypeArguments] IDENTIFIER [ Arguments ] [ "{" ClassBody "}" ]
 */
enumConstant returns [String html="", String codigoJava=""] 
    :   (annotations { $html+= $annotations.html; }
                     { $codigoJava+= $annotations.codigoJava; }
        )?
        IDENTIFIER { $html+= $IDENTIFIER.text; } 
                   { $codigoJava+= $IDENTIFIER.text; }     
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )*
        (arguments { $html+= $arguments.html; }
                   { $codigoJava+= $arguments.codigoJava; }
        )?
        (classBody { $html+= $classBody.html; }
                   { $codigoJava+= $classBody.codigoJava; }
        )?
        /* TODO: $GScope::name = names.empty. enum constant body is actually
        an anonymous class, where constructor isn't allowed, have to add this check*/
    ;

enumBodyDeclarations returns [String html="", String codigoJava=""] 
    :   SEMI { $html+= ";"; }
             { $codigoJava+= ";"; }
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*
        (classBodyDeclaration { $html+= $classBodyDeclaration.html; } 
                              { $codigoJava+= $classBodyDeclaration.codigoJava; } 
        )*
    ;

interfaceDeclaration[String modifiers] returns [String html="", String codigoJava=""] 
    scope {
        String mod;
    }
    @init{
        $interfaceDeclaration::mod= "";
    }
    :   {$interfaceDeclaration::mod = modifiers;} 
        normalInterfaceDeclaration[$interfaceDeclaration::mod] { $html+= $normalInterfaceDeclaration.html; } 
                                   { $codigoJava+= $normalInterfaceDeclaration.codigoJava; } 
    |   annotationTypeDeclaration { $html+= $annotationTypeDeclaration.html; }
                                  { $codigoJava+= $annotationTypeDeclaration.codigoJava; } 
    ;

normalInterfaceDeclaration[String modifiers] returns [String html="", String codigoJava=""] 
    scope {
        String mod;
    }
    @init{
        $normalInterfaceDeclaration::mod= "";
    }
    :   {$normalInterfaceDeclaration::mod = modifiers;}
        INTERFACE { $html+= "<font class=\"reservedesc\">interface</font>";  } 
                  { $codigoJava+= "interface "; }     
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
        IDENTIFIER { $html+= $IDENTIFIER.text; } 
                   { $codigoJava+= $IDENTIFIER.text; }     
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )*
        (typeParameters { $html+= $typeParameters.html; }
                        { $codigoJava+= $typeParameters.codigoJava; } 
        )?
        (EXTENDS { $html+= "<font class=\"reservedesc\">extends</font>"; }
                 { $codigoJava+= "extends "; } 
                 ( SPACE    { $html+= "&nbsp;"; }
                 | NEWLINE { $html+= "<br>"; }
                 | FORMFEED { $html+= "&nbsp;"; }
                 | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                 )*  
         typeList { $html+= $typeList.html; }
                  { $codigoJava+= $typeList.codigoJava; }
        )?
        interfaceBody[$normalInterfaceDeclaration::mod] { $html+= $interfaceBody.html; }
                      { $codigoJava+= $interfaceBody.codigoJava; } 
    ;

typeList returns [String html="", String codigoJava=""] 
    :   T1 = type { $html+= $T1.html; }
                  { $codigoJava+= $T1.codigoJava; }  
        (COMMA { $html+= ","; } 
               { $codigoJava+= ","; }
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*  
         T2 = type { $html+= $T2.html; }
                   { $codigoJava+= $T2.codigoJava; }
        )*
    ;

classBody returns [String html="", String codigoJava=""] 
    :   LBRACE  { $html+= "{"; }
                { $codigoJava+= "{"; }
                ( SPACE    { $html+= "&nbsp;"; }
                | NEWLINE { $html+= "<br>"; }
                | FORMFEED { $html+= "&nbsp;"; }
                | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                )*
        (classBodyDeclaration { $html+= $classBodyDeclaration.html; }
                              { $codigoJava+= $classBodyDeclaration.codigoJava; }  
        )*
        RBRACE { $html+= "}"; }
               { $codigoJava+= "}"; } 
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
    ;

interfaceBody[String modifiers] returns [String html="", String codigoJava=""] 
    scope {
        String mod;
    }
    @init{
        $interfaceBody::mod= "";
    }
    :   {$interfaceBody::mod = modifiers; }
        LBRACE { $html+= "{"; }
               { $codigoJava+= "{"; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
        (interfaceBodyDeclaration[$interfaceBody::mod] { $html+= $interfaceBodyDeclaration.html; }
                                  { $codigoJava+= $interfaceBodyDeclaration.codigoJava; }   
        )*
        RBRACE { $html+= "}"; }
               { $codigoJava+= "}"; }
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
    ;

classBodyDeclaration returns [String html="", String codigoJava=""] 
    :   SEMI { $html+= ";"; }
             { $codigoJava+= ";"; }
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*
    |   (STATIC { $html+= "<font class=\"reservedesc\">static</font>"; }
                { $codigoJava+= "static "; }   
                ( SPACE    { $html+= "&nbsp;"; }
                | NEWLINE { $html+= "<br>"; }
                | FORMFEED { $html+= "&nbsp;"; }
                | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                )*
        )?
        block { $html+= $block.html; }
              { $codigoJava+= $block.codigoJava; }
    |   memberDecl { $html+= $memberDecl.html; }
                   { $codigoJava+= $memberDecl.codigoJava; } 
    ;

memberDecl returns [String html="", String codigoJava=""] 
    scope{
        boolean flagMemberDecl;
        boolean flagFieldDeclarationMD;
        String modifiersValue;
    }
    @init{
        $memberDecl::flagMemberDecl = true;
        $memberDecl::flagFieldDeclarationMD = false;
        $memberDecl::modifiersValue="";
    }
    @after{
        $memberDecl::flagMemberDecl = false;        
    }
    :    
    modifiers { $html+= $modifiers.html; }
              { $codigoJava+= $modifiers.codigoJava; }
              { $memberDecl::modifiersValue = $modifiers.codigoJava; }
    (    { $memberDecl::flagFieldDeclarationMD = true; }
         fieldDeclaration[$memberDecl::modifiersValue] { $html+= $fieldDeclaration.html; }
                                                       { $codigoJava+= $fieldDeclaration.codigoJava; }  
         { $memberDecl::flagFieldDeclarationMD = false; }
    |    methodDeclaration[$memberDecl::modifiersValue] { $html+= $methodDeclaration.html; }
    |    classDeclaration[$modifiers.html] { $html+= $classDeclaration.html; }
                                                 { $codigoJava+= $classDeclaration.codigoJava; }  
    |    interfaceDeclaration[$modifiers.codigoJava] { $html+= $interfaceDeclaration.html; }
                                                     { $codigoJava+= $interfaceDeclaration.codigoJava; }
    )
    ;


methodDeclaration[String modifiers] returns [String html=""] 
    scope {
        MethodModel methodModel;
        int parameterPosition;
    }
    @init{
        $normalClassDeclaration::flagMethodDeclaration = true;
        $methodDeclaration::methodModel = new MethodModel();
        $methodDeclaration::parameterPosition = 0;
    }
    @after{
        $normalClassDeclaration::flagMethodDeclaration = false;
    }
    :   {
            boolean result = $modifiers.contains("@Override") ? true : false;
            $methodDeclaration::methodModel.setOverride(result);         
        }
        (
            typeParameters { $html+= $typeParameters.html; }
        )?
        IDENTIFIER { $methodDeclaration::methodModel.setName($IDENTIFIER.text); }
                   {
                        String[] scopes = $modifiers.trim().split(" ");
                        for (String scope : scopes) {
                            $methodDeclaration::methodModel.addScope(scope.trim());
                        }
                   }                          
                   ( SPACE    
                   | NEWLINE 
                   | FORMFEED    
                   | TABULATION 
                   )*
        formalParameters { $html+= "<a class=\""+$IDENTIFIER.text.toLowerCase()+"\" name=\""+$normalClassDeclaration::classModel.getName()+"!"+$IDENTIFIER.text+"!"+$formalParameters.codigoJava+"\" href=\"method\">"+$IDENTIFIER.text+$formalParameters.codigoJava+"&nbsp;</a>"; }
        (
            THROWS { $html+= "<font class=\"reservedesc\">throws</font>"; }
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*   
            qualifiedNameList { $html+= $qualifiedNameList.html; }
        )?
        LBRACE { $html+= "{"; } 
        ( SPACE    { $html+= "&nbsp;"; }
        | NEWLINE { $html+= "<br>"; }
        | FORMFEED { $html+= "&nbsp;"; }
        | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
        )*                
        (explicitConstructorInvocation { $html+= $explicitConstructorInvocation.html; } 
        )?
        (blockStatement { $html+= $blockStatement.html; }
        )*
        RBRACE { $html+= "}"; } 
        ( SPACE    { $html+= "&nbsp;"; }
        | NEWLINE { $html+= "<br>"; }
        | FORMFEED { $html+= "&nbsp;"; }
        | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
        )*
        { $methodDeclaration::methodModel.setHtml("<html><head></head><body class=\"bodyesc\">"+modifiers + $html+"</body></html>"); }
        { $methodDeclaration::methodModel.setLoc(FilesUtil.countLinesHtml($html));}
        { $normalClassDeclaration::classModel.addMetodos($methodDeclaration::methodModel); }
           
    |   { 
            boolean result = $modifiers.contains("@Override") ? true : false;
            $methodDeclaration::methodModel.setOverride(result); 
        }
        { 
            String[] scopes = $modifiers.trim().split(" ");
            for (String scope : scopes) {
                $methodDeclaration::methodModel.addScope(scope.trim());
            }
        }
        (typeParameters { $html+= $typeParameters.html; }
        )?
        (type { $html+= $type.html; }  
        |
            VOID { $html+= "<font class=\"reservedesc\">void</font>"; }
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
        )
        IDENTIFIER { $methodDeclaration::methodModel.setName($IDENTIFIER.text); }
        ( SPACE    
        | NEWLINE 
        | FORMFEED    
        | TABULATION 
        )*
        formalParameters { $html+= "<a class=\""+$IDENTIFIER.text.toLowerCase()+"\" name=\""+$normalClassDeclaration::classModel.getName()+"!"+$IDENTIFIER.text+"!"+$formalParameters.codigoJava+"\" href=\"method\">"+$IDENTIFIER.text+$formalParameters.codigoJava+"&nbsp;</a>"; }
        (
            LBRACKET { $html+= "["; } 
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
            RBRACKET { $html+= "]"; } 
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
        )*
        (
            THROWS { $html+= "<font class=\"reservedesc\">throws</font>"; } 
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*   
            qualifiedNameList { $html+= $qualifiedNameList.html; }
        )?
        (
            block { $html+= $block.html; }
        |   
            SEMI { $html+= ";"; }
            ( SPACE { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "<br>"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
        )
        { $methodDeclaration::methodModel.setHtml("<html><head></head><body class=\"bodyesc\">" + modifiers + $html +"</body></html>"); }
        { $methodDeclaration::methodModel.setLoc(FilesUtil.countLinesHtml($html));}
        { $normalClassDeclaration::classModel.addMetodos($methodDeclaration::methodModel); }
    ;


fieldDeclaration[String modifiers] returns [String html="", String codigoJava=""] 
    :   
        type { $html+= $type.html; }
             { $codigoJava+= $type.codigoJava; }
        VD1 = variableDeclarator { $html+= $VD1.html; }
                                 { $codigoJava+= $VD1.codigoJava; }
                                 {
                                    ClassAttributeModel classAttributeModel = new ClassAttributeModel();
                                    String[] scopes = $modifiers.trim().split(" ");
                                    for (String scope : scopes) {
                                        classAttributeModel.addScope(scope.trim());
                                    }
                                    classAttributeModel.setType($type.codigoJava);
                                    classAttributeModel.setName($VD1.nameVariableDeclarator);
                                    $normalClassDeclaration::classModel.addAttribute(classAttributeModel);
                                 }
        (COMMA { $html+= ","; }
               { $codigoJava+= ","; }   
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "<br>"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*  
         VD2 = variableDeclarator { $html+= $VD2.html; } 
                                  { $codigoJava+= $VD2.codigoJava; }
                                 {
                                    ClassAttributeModel classAttributeModel = new ClassAttributeModel();
                                    String[] scopes = $modifiers.trim().split(" ");
                                    for (String scope : scopes) {
                                        classAttributeModel.addScope(scope.trim());
                                    }
                                    classAttributeModel.setType($type.codigoJava);
                                    classAttributeModel.setName($VD2.nameVariableDeclarator);
                                    $normalClassDeclaration::classModel.addAttribute(classAttributeModel);
                                 }
        )*
        SEMI { $html+= ";"; } 
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*
    ;

variableDeclarator returns [String html="", String codigoJava="", String nameVariableDeclarator=""] 
    :   IDENTIFIER { $html+= $IDENTIFIER.text; }
                   { $codigoJava+= $IDENTIFIER.text+" "; }
                   { $nameVariableDeclarator+=$IDENTIFIER.text; }
                   ( SPACE    
                   | NEWLINE 
                   | FORMFEED    
                   | TABULATION 
                   )*
        (LBRACKET { $html+= "["; }
                  { $codigoJava+= "["; }
                  { $nameVariableDeclarator+= "["; }
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
         RBRACKET { $html+= "]"; }
                  { $codigoJava+= "]"; }
                  { $nameVariableDeclarator+= "]"; }
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
        )*
        (EQ { $html+= "="; }
            { $codigoJava+= "="; }
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
         variableInitializer { $html+= $variableInitializer.html; }
                             { $codigoJava+= $variableInitializer.codigoJava; }
        )?
    ;

/**
 *TODO: add predicates
 */
interfaceBodyDeclaration[String modifiers] returns [String html="", String codigoJava=""] 
    scope {
        String mod;
    }
    @init{
        $interfaceBodyDeclaration::mod= "";
    }
    :
        interfaceFieldDeclaration { $html+= $interfaceFieldDeclaration.html; }
                                  { $codigoJava+= $interfaceFieldDeclaration.codigoJava; } 
    |   interfaceMethodDeclaration { $html+= $interfaceMethodDeclaration.html; }
                                   { $codigoJava+= $interfaceMethodDeclaration.codigoJava; } 
    |   {$interfaceBodyDeclaration::mod= modifiers;}
        interfaceDeclaration[$interfaceBodyDeclaration::mod] { $html+= $interfaceDeclaration.html; }
                             { $codigoJava+= $interfaceDeclaration.codigoJava; }
    |   {$interfaceBodyDeclaration::mod= modifiers;}
        classDeclaration[$interfaceBodyDeclaration::mod] { $html+= $classDeclaration.html; }
                         { $codigoJava+= $classDeclaration.codigoJava; }   
    |   SEMI { $html+= ";"; } 
             { $codigoJava+= ";"; }       
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
    ;

interfaceMethodDeclaration returns [String html="", String codigoJava=""] 
    :   modifiers { $html+= $modifiers.html; }
                  { $codigoJava+= $modifiers.codigoJava; }
        (typeParameters { $html+= $typeParameters.html; }
                        { $codigoJava+= $typeParameters.codigoJava; }
        )?
        (type { $html+= $type.html; }
              { $codigoJava+= $type.codigoJava; } 
        |VOID { $html+= "<font class=\"reservedesc\">void</font>"; }
              { $codigoJava+= "void "; }   
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )* 
        )
        IDENTIFIER { $html+= "<a href=\"\">"+$IDENTIFIER.text+"</a>" ; } 
                   { $codigoJava+= $IDENTIFIER.text+" "; }    
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )* 
        formalParameters { $html+= $formalParameters.html; }   
                         { $codigoJava+= $formalParameters.codigoJava; } 
        (LBRACKET { $html+= "["; } 
                  { $codigoJava+= "["; }   
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )* 
         RBRACKET { $html+= "]"; } 
                  { $codigoJava+= "]"; }   
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
        )*
        (THROWS { $html+= "<font class=\"reservedesc\">throws</font>"; }
                { $codigoJava+= "throws "; }   
                ( SPACE    { $html+= "&nbsp;"; }
                | NEWLINE { $html+= "<br>"; }
                | FORMFEED { $html+= "&nbsp;"; }
                | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                )*    
         qualifiedNameList { $html+= $qualifiedNameList.html; }
                           { $codigoJava+= $qualifiedNameList.codigoJava; } 
        )? 
        SEMI { $html+= ";"; }
             { $codigoJava+= ";"; }
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
    ;

/**
 * NOTE, should not use variableDeclarator here, as it doesn't necessary require
 * an initializer, while an interface field does, or judge by the returned value.
 * But this gives better diagnostic message, or antlr won't predict this rule.
 */
interfaceFieldDeclaration returns [String html="", String codigoJava=""] 
    :   modifiers { $html+= $modifiers.html; }
                  { $codigoJava+= $modifiers.codigoJava; }  
        type { $html+= $type.html;}   
             { $codigoJava+= $type.codigoJava; } 
        VD1 = variableDeclarator { $html+= $VD1.html; }
                                 { $codigoJava+= $VD1.codigoJava; }
        (COMMA { $html+= ","; } 
               { $codigoJava+= ","; }     
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*  
         VD2 = variableDeclarator { $html+= $VD2.html; }
                                  { $codigoJava+= $VD2.codigoJava; }
        )*
        SEMI { $html+= ";"; }  
             { $codigoJava+= ";"; }         
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*
    ;


type returns [String html="", String codigoJava=""] 
    :   classOrInterfaceType { $html+= $classOrInterfaceType.html; }
                             { $codigoJava+= $classOrInterfaceType.codigoJava; }
        (LBRACKET { $html+= "["; }
                  { $codigoJava+= "["; }
                  ( SPACE    
                  | NEWLINE
                  | FORMFEED  
                  | TABULATION 
                  )* 
         RBRACKET { $html+= "]"; }
                  { $codigoJava+= "]"; }
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
        )*
    |   primitiveType { $html+= $primitiveType.html; }
                      { $codigoJava+= $primitiveType.codigoJava; } 
        (LBRACKET { $html+= "["; }
                  { $codigoJava+= "["; }
                  ( SPACE   
                  | NEWLINE 
                  | FORMFEED    
                  | TABULATION 
                  )*
         RBRACKET { $html+= "]"; }
                  { $codigoJava+= "]"; }
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
        )*
    ;


classOrInterfaceType returns [String html="", String codigoJava=""] 
    scope{
        ClassModel clasePadre;
        boolean flagEntro;
    }
    @init{
        $classOrInterfaceType::flagEntro = false;
    }
    @after{
        $classOrInterfaceType::flagEntro = false;    
    }
    :   ID1 = IDENTIFIER { $html+= $ID1.text; }
                         { $codigoJava+= $ID1.text; }  
                         ( SPACE    
                         | NEWLINE 
                         | FORMFEED    
                         | TABULATION 
                         )* 
        (TAS1 = typeArguments { $html+= $TAS1.html; }
                              { $codigoJava+= $TAS1.codigoJava; }
        )?
        (DOT { $html+= "."; } 
             { $codigoJava+= "."; }  
             ( SPACE    
             | NEWLINE 
             | FORMFEED    
             | TABULATION 
             )*
         ID2 = IDENTIFIER { $html+= $ID2.text; } 
                          { $codigoJava+= $ID2.text; } 
                          { $classOrInterfaceType::flagEntro = true; }    
                          ( SPACE    
                          | NEWLINE 
                          | FORMFEED   
                          | TABULATION 
                          )*
            (TAS2 = typeArguments { $html+= $TAS2.html; }
                                  { $codigoJava+= $TAS2.codigoJava; }     
            )?
        )*
        ( SPACE    { $html+= "&nbsp;"; }
        | NEWLINE { $html+= "<br>"; }
        | FORMFEED { $html+= "&nbsp;"; }
        | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
        )*
        { $html+= "&nbsp;"; }
        { $codigoJava+= " "; }
    ;

primitiveType returns [String html="", String codigoJava=""] 
    :   
    (   BOOLEAN { $html+= "<font class=\"reservedesc\">boolean</font>"; } 
                { $codigoJava+= "boolean "; }  
    |   CHAR { $html+= "<font class=\"reservedesc\">char</font>"; } 
             { $codigoJava+= "char "; }   
    |   BYTE { $html+= "<font class=\"reservedesc\">byte</font>"; } 
             { $codigoJava+= "byte "; } 
    |   SHORT { $html+= "<font class=\"reservedesc\">short</font>"; } 
              { $codigoJava+= "short "; }
    |   INT { $html+= "<font class=\"reservedesc\">int</font>"; }    
            { $codigoJava+= "int "; }
    |   LONG { $html+= "<font class=\"reservedesc\">long</font>"; } 
             { $codigoJava+= "long "; }
    |   FLOAT { $html+= "<font class=\"reservedesc\">float</font>"; } 
              { $codigoJava+= "float "; }
    |   DOUBLE { $html+= "<font class=\"reservedesc\">double</font>"; } 
               { $codigoJava+= "double "; }
    )
    ( SPACE    { $html+= "&nbsp;"; }
    | NEWLINE { $html+= "<br>"; }
    | FORMFEED { $html+= "&nbsp;"; }
    | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
    )* 
    ;

typeArguments returns [String html="", String codigoJava=""] 
    :   LT { $html+= "&lt;"; }
           { $codigoJava+= "&lt;"; }    
           ( SPACE    
           | NEWLINE 
           | FORMFEED    
           | TABULATION 
           )*  
        TA1 = typeArgument { $html+= $TA1.html; }
                           { $codigoJava+= $TA1.codigoJava.trim(); }
        (COMMA { $html+= ","; } 
               { $codigoJava+= ","; } 
               ( SPACE    
               | NEWLINE 
               | FORMFEED    
               | TABULATION 
               )*  
         TA2 = typeArgument { $html+= $TA2.html; }
                            { $codigoJava+= $TA2.codigoJava.trim(); }
        )*
        GT { $html+= ">"; } 
           { $codigoJava+= ">"; } 
           ( SPACE   
           | NEWLINE 
           | FORMFEED    
           | TABULATION 
           )*
        { $html+= "&nbsp;"; }   
        { $codigoJava+= " "; }   
    ;

typeArgument returns [String html="", String codigoJava=""] 
    :   type { $html+= $type.html; }
             { $codigoJava+= $type.codigoJava; }
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
    |   QUES { $html+= "?"; }
             { $codigoJava+= "?"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
        (
            (EXTENDS { $html+= "<font class=\"reservedesc\">extends</font>"; } 
                     { $codigoJava+= "extends "; }
            |SUPER { $html+= "<font class=\"reservedesc\">super</font>"; } 
                   { $codigoJava+= "super "; } 
            )
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
            type { $html+= $type.html; }
                 { $codigoJava+= $type.codigoJava; } 
        )?
    ;

qualifiedNameList returns [String html="", String codigoJava=""] 
    :   QN1 = qualifiedName { $html+= $QN1.html; }
                            { $codigoJava+= $QN1.codigoJava; }
        (COMMA { $html+= ","; }
               { $codigoJava+= ","; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*  
         QN2 = qualifiedName { $html+= $QN2.html; }
                             { $codigoJava+= $QN2.codigoJava; }
        )*
    ;

formalParameters returns [String html="", String codigoJava=""] 
    :   LPAREN { $html+= "("; }
               { $codigoJava+= "("; } 
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )* 
        (formalParameterDecls { $html+= $formalParameterDecls.html; }
                              { $codigoJava+= $formalParameterDecls.codigoJava; }
        )?
        RPAREN { $html+= ")"; }
               { $codigoJava+= ")"; } 
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
    ;

formalParameterDecls returns [String html="", String codigoJava="", String nameParameter="", String typeParameter=""] 
    :   ellipsisParameterDecl { $html+= $ellipsisParameterDecl.html; }
                              { $codigoJava+= $ellipsisParameterDecl.codigoJava; } 
    |   NPD1 = normalParameterDecl { $html+= $NPD1.html; }
                                   { $codigoJava+= $NPD1.codigoJava; } 
        (COMMA { $html+= ","; }
               { $codigoJava+= ","; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )* 
         NPD2 = normalParameterDecl { $html+= $NPD2.html; }
                                    { $codigoJava+= $NPD2.codigoJava; }
        )*
    |   (normalParameterDecl { $html+= $normalParameterDecl.html; }
                             { $codigoJava+= $normalParameterDecl.codigoJava; } 
        COMMA { $html+= ","; }
              { $codigoJava+= ","; }        
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )* 
        )+
        ellipsisParameterDecl { $html+= $ellipsisParameterDecl.html; }
                              { $codigoJava+= $ellipsisParameterDecl.codigoJava; }
    ;

normalParameterDecl returns [String html="", String codigoJava=""] 
    scope{
        String nombre;
        String tipo;
    }
    @init{
        $normalParameterDecl::nombre="";
        $normalParameterDecl::tipo="";
    }
    :   variableModifiers { $html+= $variableModifiers.html; }
                          { $codigoJava+= $variableModifiers.codigoJava; }      
        type { $html+= $type.html; }
             { $codigoJava+= $type.codigoJava; } 
        { $normalParameterDecl::tipo = $type.codigoJava; }
        ( SPACE    { $html+= "&nbsp;"; }
        | NEWLINE { $html+= "<br>"; }
        | FORMFEED { $html+= "&nbsp;"; }
        | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
        )*
        IDENTIFIER { $html+= $IDENTIFIER.text; }
                   { $codigoJava+= $IDENTIFIER.text+" "; }
                   { $normalParameterDecl::nombre += $IDENTIFIER.text+" "; }  
                   ( SPACE
                   | NEWLINE 
                   | FORMFEED    
                   | TABULATION
                   )*     
        (LBRACKET { $html+= "["; }
                  { $codigoJava+= "["; }
                  { $normalParameterDecl::nombre += "["; }    
                  ( SPACE   
                  | NEWLINE
                  | FORMFEED   
                  | TABULATION
                  )* 
         RBRACKET { $html+= "]"; }
                  { $codigoJava+= "]"; }
                  { $normalParameterDecl::nombre += "]"; } 
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
        )*
        { $methodDeclaration::methodModel.addParametro($normalParameterDecl::tipo.replaceAll("\\s+",""), $normalParameterDecl::nombre.replaceAll("\\s+",""), ++$methodDeclaration::parameterPosition); }        
    ;

ellipsisParameterDecl returns [String html="", String codigoJava=""] 
    :   variableModifiers { $html+= $variableModifiers.html; }
                          { $codigoJava+= $variableModifiers.codigoJava; }      
        type { $html+= $type.html; }
             { $codigoJava+= $type.codigoJava; }
        ELLIPSIS { $html+= "..."; }
                 { $codigoJava+= "..."; }      
                 ( SPACE    { $html+= "&nbsp;"; }
                 | NEWLINE { $html+= "<br>"; }
                 | FORMFEED { $html+= "&nbsp;"; }
                 | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                 )* 
        IDENTIFIER { $html+= $IDENTIFIER.text; }
                   { $codigoJava+= $IDENTIFIER.text+" "; }     
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )* 
    ;


explicitConstructorInvocation returns [String html="", String codigoJava=""] 
    :   (nonWildcardTypeArguments { $html+= $nonWildcardTypeArguments.html; }
                                  { $codigoJava+= $nonWildcardTypeArguments.codigoJava; }     
        )?     //NOTE: the position of Identifier 'super' is set to the type args position here
        (THIS { $html+= "<font class=\"reservedesc\">this</font>"; }
              { $codigoJava+= "this "; } 
        |SUPER { $html+= "<font class=\"reservedesc\">super</font>"; }
               { $codigoJava+= "super "; }
        )
        ( SPACE    { $html+= "&nbsp;"; }
        | NEWLINE { $html+= "<br>"; }
        | FORMFEED { $html+= "&nbsp;"; }
        | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
        )*
        arguments { $html+= $arguments.html; }
                  { $codigoJava+= $arguments.codigoJava; }  
        SEMI { $html+= ";"; }
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
    |   primary { $html+= $primary.html; }
                { $codigoJava+= $primary.codigoJava; } 
        DOT { $html+= "."; } 
            { $codigoJava+= "."; }    
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )* 
        (nonWildcardTypeArguments { $html+= $nonWildcardTypeArguments.html; }
                                  { $codigoJava+= $nonWildcardTypeArguments.codigoJava; }
        )?
        SUPER { $html+= "<font class=\"reservedesc\">super</font>"; }
              { $codigoJava+= "super "; }    
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )* 
        arguments { $html+= $arguments.html; }
                  { $codigoJava+= $arguments.codigoJava; }      
        SEMI { $html+= ";"; }
             { $codigoJava+= ";"; }        
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
    ;

qualifiedName returns [String html="", String codigoJava=""] 
    :   ID1 = IDENTIFIER { $html+= $ID1.text; }
                         { $codigoJava+= $ID1.text; }
                         ( SPACE   { $html+= "&nbsp;"; }
                         | NEWLINE 
                         | FORMFEED  
                         | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                         )*
        (DOT { $html+= "."; }
             { $codigoJava+= "."; }    
             ( SPACE    
             | NEWLINE 
             | FORMFEED    
             | TABULATION 
             )*
         ID2 = IDENTIFIER { $html+= $ID2.text; }
                          { $codigoJava+= $ID2.text; }     
                          ( SPACE    
                          | NEWLINE 
                          | FORMFEED   
                          | TABULATION 
                          )*  
        )*
    ;

annotations returns [String html="", String codigoJava=""] 
    :   (annotation { $html+= $annotation.html; }
                    { $codigoJava+= $annotation.codigoJava; }
        )+
    ;

/**
 *  Using an annotation.
 * '@' is flaged in modifier
 */
annotation returns [String html="", String codigoJava=""] 
    :       
        MONKEYS_AT { $html+= "<br>&nbsp;&nbsp;&nbsp;&nbsp;@"; } 
                   { $codigoJava+= "@"; }        
                   ( SPACE   
                   | NEWLINE
                   | FORMFEED   
                   | TABULATION
                   )*  
        qualifiedName { $html+= $qualifiedName.html+ "<br>&nbsp;&nbsp;&nbsp;&nbsp;"; }
                      { $codigoJava+= $qualifiedName.codigoJava + "\n"; }
                       ( SPACE   
                       | NEWLINE
                       | FORMFEED   
                       | TABULATION 
                       )*   
        (   LPAREN { $html+= "("; }
                   { $codigoJava+= "("; }       
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )*   
                  (   elementValuePairs { $html+= $elementValuePairs.html; }
                                        { $codigoJava+= $elementValuePairs.codigoJava; }
                  |   elementValue { $html+= $elementValue.html; }
                                   { $codigoJava+= $elementValue.codigoJava; } 
                  )?
            RPAREN { $html+= ")"; } 
                   { $codigoJava+= ")"; }    
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )*  
        )?
    ;

elementValuePairs returns [String html="", String codigoJava=""] 
    :   EVP1 = elementValuePair { $html+= $EVP1.html; }
                                { $codigoJava+= $EVP1.codigoJava; } 
        (COMMA { $html+= ","; } 
               { $codigoJava+= ","; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*   
         EVP2 = elementValuePair { $html+= $EVP2.html; }
                                 { $codigoJava+= $EVP2.codigoJava; }
        )*
    ;

elementValuePair returns [String html="", String codigoJava=""]  
    :   IDENTIFIER { $html+= $IDENTIFIER.text; } 
                   { $codigoJava+= $IDENTIFIER.text+" "; }     
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )*
        EQ { $html+= "="; }
           { $codigoJava+= "="; }    
           ( SPACE    { $html+= "&nbsp;"; }
           | NEWLINE { $html+= "<br>"; }
           | FORMFEED { $html+= "&nbsp;"; }
           | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
           )*
        elementValue { $html+= $elementValue.html; }
                     { $codigoJava+= $elementValue.codigoJava; }  
    ;

elementValue returns [String html="", String codigoJava=""] 
    :   conditionalExpression { $html+= $conditionalExpression.html; }
                              { $codigoJava+= $conditionalExpression.codigoJava; }
    |   annotation { $html+= $annotation.html; }
                   { $codigoJava+= $annotation.codigoJava; } 
    |   elementValueArrayInitializer { $html+= $elementValueArrayInitializer.html; }
                                     { $codigoJava+= $elementValueArrayInitializer.codigoJava; } 
    ;

elementValueArrayInitializer returns [String html="", String codigoJava=""] 
    :   LBRACE { $html+= "{"; } 
               { $codigoJava+= "{"; }        
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )* 
        (EV1 = elementValue { $html+= $EV1.html; }
                            { $codigoJava+= $EV1.codigoJava; }
            (COMMA { $html+= ","; }  
                   { $codigoJava+= ","; }        
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )*  
             EV2 = elementValue { $html+= $EV2.html; }
                                { $codigoJava+= $EV2.codigoJava; }
            )*
        )? (COMMA { $html+= ","; } 
                  { $codigoJava+= ","; }    
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
           )? 
           RBRACE { $html+= "}"; } 
                  { $codigoJava+= "}"; }    
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*  
    ;


/**
 * Annotation declaration.
 */
annotationTypeDeclaration returns [String html="", String codigoJava=""] 
    :   MONKEYS_AT { $html+= "@"; } 
                   { $codigoJava+= "@"; }    
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )*   
        INTERFACE { $html+= "<font class=\"reservedesc\">interface</font>"; }
                  { $codigoJava+= "interface "; }    
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
        ID1= IDENTIFIER { $html+= $ID1.text; }
                        { $codigoJava+= $ID1.text+" "; }        
                        ( SPACE    { $html+= "&nbsp;"; }
                        | NEWLINE { $html+= "<br>"; }
                        | FORMFEED { $html+= "&nbsp;"; }
                        | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                        )*
        annotationTypeBody { $html+= $annotationTypeBody.html; }
                           { $codigoJava+= $annotationTypeBody.codigoJava; }                          
    ;


annotationTypeBody returns [String html="", String codigoJava=""] 
    :   LBRACE { $html+= "{"; } 
               { $codigoJava+= "{"; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )* 
        (annotationTypeElementDeclaration { $html+= $annotationTypeElementDeclaration.html; }
                                          { $codigoJava+= $annotationTypeElementDeclaration.codigoJava; }    
        )*
        RBRACE { $html+= "}"; } 
               { $codigoJava+= "}"; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )* 
    ;

/**
 * NOTE: here use interfaceFieldDeclaration for field declared inside annotation. they are sytactically the same.
 */
annotationTypeElementDeclaration returns [String html="", String codigoJava=""] 
    :   annotationMethodDeclaration { $html+= $annotationMethodDeclaration.html; }
                                    { $codigoJava+= $annotationMethodDeclaration.codigoJava; }    
    |   interfaceFieldDeclaration { $html+= $interfaceFieldDeclaration.html; }
                                  { $codigoJava+= $interfaceFieldDeclaration.codigoJava; } 
    |   modifiers { $html+= $modifiers.html; }
                  { $codigoJava+= $modifiers.codigoJava; }  
        normalClassDeclaration[$modifiers.codigoJava] { $html+= $normalClassDeclaration.html; }
    |   modifiers { $html+= $modifiers.html; }
                  { $codigoJava+= $modifiers.codigoJava; }  
        normalInterfaceDeclaration[$modifiers.codigoJava] { $html+= $normalInterfaceDeclaration.html; }
                                   { $codigoJava+= $normalInterfaceDeclaration.codigoJava; }
    |   modifiers enumDeclaration { $html+= $enumDeclaration.html; }
                                  { $codigoJava+= $enumDeclaration.codigoJava; } 
    |   modifiers { $html+= $modifiers.html; }
                  { $codigoJava+= $modifiers.codigoJava; }  
        annotationTypeDeclaration { $html+= $annotationTypeDeclaration.html; }
                                  { $codigoJava+= $annotationTypeDeclaration.codigoJava; }
    |   SEMI { $html+= ";"; } 
             { $codigoJava+= ";"; }        
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
    ;

annotationMethodDeclaration returns [String html="", String codigoJava=""] 
    :   modifiers { $html+= $modifiers.html; }
                  { $codigoJava+= $modifiers.codigoJava; }  
        type { $html+= $type.html; }
             { $codigoJava+= $type.codigoJava; }
        IDENTIFIER { $html+= $IDENTIFIER.text; }
                   { $codigoJava+= $IDENTIFIER.text+" "; }     
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )* 
        LPAREN { $html+= "("; } 
               { $codigoJava+= "("; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*   
        RPAREN { $html+= ")"; } 
               { $codigoJava+= ")"; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
        (DEFAULT { $html+= "<font class=\"reservedesc\">default</font>"; }
                 { $codigoJava+= "default "; }      
                 ( SPACE    { $html+= "&nbsp;"; }
                 | NEWLINE { $html+= "<br>"; }
                 | FORMFEED { $html+= "&nbsp;"; }
                 | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                 )*  
         elementValue { $html+= $elementValue.html; }
                      { $codigoJava+= $elementValue.codigoJava; }
        )?
        SEMI { $html+= ";"; } 
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
        ;

block returns [String html="", String codigoJava=""] 
    :   LBRACE { $html+= "{"; } 
               { $codigoJava+= "{"; }
        ( SPACE    { $html+= "&nbsp;"; }
        | NEWLINE { $html+= "<br>"; }
        | FORMFEED { $html+= "&nbsp;"; }
        | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
        )* 
        (blockStatement { $html+= $blockStatement.html; }
                        { $codigoJava+= $blockStatement.codigoJava; }
        )*
        RBRACE { $html+= "}"; } 
               { $codigoJava+= "}"; }    
        ( SPACE    { $html+= "&nbsp;"; }
        | NEWLINE { $html+= "<br>"; }
        | FORMFEED { $html+= "&nbsp;"; }
        | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
        )* 
    ;

/*
staticBlock returns [JCBlock tree]
        @init {
            ListBuffer<JCStatement> stats = new ListBuffer<JCStatement>();
            int pos = ((AntlrJavacToken) $start).getStartIndex();
        }
        @after {
            $tree = T.at(pos).Block(Flags.STATIC, stats.toList());
            pu.storeEnd($tree, $stop);
            // construct a dummy static modifiers for end position
            pu.storeEnd(T.at(pos).Modifiers(Flags.STATIC,  com.sun.tools.javac.util.List.<JCAnnotation>nil()),$st);
        }
    :   st_1=STATIC LBRACE
        (blockStatement
            {
                if ($blockStatement.tree == null) {
                    stats.appendList($blockStatement.list);
                } else {
                    stats.append($blockStatement.tree);
                }
            }
        )* RBRACE
    ;
*/
blockStatement returns [String html="", String codigoJava=""] 
    :   localVariableDeclarationStatement { $html+= $localVariableDeclarationStatement.html; } 
                                          { $codigoJava+= $localVariableDeclarationStatement.codigoJava; }
    |   classOrInterfaceDeclaration { $html+= $classOrInterfaceDeclaration.html; } 
                                    { $codigoJava+= $classOrInterfaceDeclaration.codigoJava; }
    |   statement { $html+= $statement.html; } 
                  { $codigoJava+= $statement.codigoJava; }
    ;


localVariableDeclarationStatement returns [String html="", String codigoJava=""] 
    :   localVariableDeclaration { $html+= $localVariableDeclaration.html; } 
                                 { $codigoJava+= $localVariableDeclaration.codigoJava; } 
        SEMI { $html+= ";"; } 
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
    ;

localVariableDeclaration returns [String html="", String codigoJava=""] 
    :   variableModifiers { $html+= $variableModifiers.html; }
                          { $codigoJava+= $variableModifiers.codigoJava; }
        type { $html+= $type.html; }
             { $codigoJava+= $type.codigoJava; }
        VD1 = variableDeclarator { $html+= $VD1.html; }
                                 { $codigoJava+= $VD1.codigoJava; }  
                                 {
                                    if($normalClassDeclaration::flagNormalClassDeclaration) { 
                                        if($normalClassDeclaration::flagMethodDeclaration) {
                                            MethodAttributeModel methodAttributeModel = new MethodAttributeModel();
                                            methodAttributeModel.setType($type.codigoJava);
                                            methodAttributeModel.setName($VD1.nameVariableDeclarator);
                                            $methodDeclaration::methodModel.addAtributos(methodAttributeModel);
                                        }
                                    }
                                 }

        (COMMA { $html+= ","; } 
               { $codigoJava+= ","; }
        ( SPACE    { $html+= "&nbsp;"; }
        | NEWLINE { $html+= "<br>"; }
        | FORMFEED { $html+= "&nbsp;"; }
        | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
        )*  
        VD2 = variableDeclarator { $html+= $VD2.html; }
                                 { $codigoJava+= $VD2.codigoJava; } 
                                 {
                                    if($normalClassDeclaration::flagNormalClassDeclaration) { 
                                        if($normalClassDeclaration::flagMethodDeclaration) {
                                            MethodAttributeModel methodAttributeModel = new MethodAttributeModel();
                                            methodAttributeModel.setType($type.codigoJava);
                                            methodAttributeModel.setName($VD2.nameVariableDeclarator);
                                            $methodDeclaration::methodModel.addAtributos(methodAttributeModel);
                                        } else {
                                            ClassAttributeModel classAttributeModel = new ClassAttributeModel();
                                            classAttributeModel.setType($type.codigoJava);
                                            classAttributeModel.setName($VD2.nameVariableDeclarator);
                                            $normalClassDeclaration::classModel.addAttribute(classAttributeModel);
                                        }
                                    }
                                 }
        )*
    ;

statement returns [String html="", String codigoJava=""] 
    :   block { $html+= $block.html; }         
              { $codigoJava+= $block.codigoJava; }
    |   ASSERT { $html+= "<font class=\"reservedesc\">assert</font>"; }
               { $codigoJava+= "assert "; }       
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )* 
        E1 = expression { $html+= $E1.html; }
                        { $codigoJava+= $E1.codigoJava; }
        (COLON { $html+= ":"; }
               { $codigoJava+= ":"; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
         E2 = expression { $html+= $E2.html; }
                         { $codigoJava+= $E2.codigoJava; }    
        )? 
        SEMI { $html+= ";"; }
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*
    |   IF { $html+= "<font class=\"reservedesc\">if</font>"; }
           { $codigoJava+= "if "; }    
           ( SPACE    { $html+= "&nbsp;"; }
           | NEWLINE { $html+= "<br>"; }
           | FORMFEED { $html+= "&nbsp;"; }
           | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
           )*  
        parExpression { $html+= $parExpression.html; } 
                      { $codigoJava+= $parExpression.codigoJava; }
        S1 = statement { $html+= $S1.html; }
                       { $codigoJava+= $S1.codigoJava; }
        (ELSE { $html+= "<font class=\"reservedesc\">else</font>"; }
              { $codigoJava+= "else "; }     
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*
         S2 = statement { $html+= $S2.html; }
                        { $codigoJava+= $S2.codigoJava; }
        )?
    |   forstatement { $html+= $forstatement.html; }
                     { $codigoJava+= $forstatement.codigoJava; }
    |   WHILE { $html+= "<font class=\"reservedesc\">while</font>"; }
              { $codigoJava+= "while "; }    
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*
        parExpression { $html+= $parExpression.html; }
                      { $codigoJava+= $parExpression.codigoJava; }
        S1 = statement { $html+= $S1.html; }
                       { $codigoJava+= $S1.codigoJava; }
    |   DO { $html+= "<font class=\"reservedesc\">do</font>"; } 
           { $codigoJava+= "do "; }    
           ( SPACE    { $html+= "&nbsp;"; }
           | NEWLINE { $html+= "<br>"; }
           | FORMFEED { $html+= "&nbsp;"; }
           | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
           )*  
        S1 = statement { $html+= $S1.html; }
                       { $codigoJava+= $S1.codigoJava; }
        WHILE { $html+= "<font class=\"reservedesc\">while</font>"; }
              { $codigoJava+= "while "; }        
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*
        parExpression { $html+= $parExpression.html; }
                      { $codigoJava+= $parExpression.codigoJava; }
        SEMI { $html+= ";"; }
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*
    |   trystatement { $html+= $trystatement.html; }
                     { $codigoJava+= $trystatement.codigoJava; }  
    |   SWITCH { $html+= "<font class=\"reservedesc\">switch</font>"; }
               { $codigoJava+= "switch "; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*  
        parExpression { $html+= $parExpression.html; }
                      { $codigoJava+= $parExpression.codigoJava; }
        LBRACE { $html+= "{"; }
               { $codigoJava+= "{"; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
        switchBlockStatementGroups { $html+= $switchBlockStatementGroups.html; } 
                                   { $codigoJava+= $switchBlockStatementGroups.codigoJava; }
        RBRACE { $html+= "}"; }
               { $codigoJava+= "}"; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
    |   SYNCHRONIZED { $html+= "<font class=\"reservedesc\">synchronized</font>"; } 
                     { $codigoJava+= "synchronized "; }     
                     ( SPACE    { $html+= "&nbsp;"; }
                     | NEWLINE { $html+= "<br>"; }
                     | FORMFEED { $html+= "&nbsp;"; }
                     | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                     )*  
        parExpression { $html+= $parExpression.html; }
                      { $codigoJava+= $parExpression.codigoJava; }
        block { $html+= $block.html; }
              { $codigoJava+= $block.codigoJava; }
    |   RETURN { $html+= "<font class=\"reservedesc\">return</font>"; } 
               { $codigoJava+= "return "; }       
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )* 
        (expression { $html+= $expression.html; } 
                    { $codigoJava+= $expression.codigoJava; }  
        )? 
        SEMI { $html+= ";"; }
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*
    |   THROW { $html+= "<font class=\"reservedesc\">throw</font>"; } 
              { $codigoJava+= "throw "; }    
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*  
        expression { $html+= $expression.html; } 
                   { $codigoJava+= $expression.codigoJava; }
        SEMI { $html+= ";"; }
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*
    |   BREAK { $html+= "<font class=\"reservedesc\">break</font>"; } 
              { $codigoJava+= "break "; }    
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )* 
        (IDENTIFIER { $html+= $IDENTIFIER.text; }
                    { $codigoJava+= $IDENTIFIER.text+" "; }     
                    ( SPACE    { $html+= "&nbsp;"; }
                    | NEWLINE { $html+= "<br>"; }
                    | FORMFEED { $html+= "&nbsp;"; }
                    | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                    )* 
        )? 
        SEMI { $html+= ";"; } 
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
    |   CONTINUE { $html+= "<font class=\"reservedesc\">continue</font>"; } 
                 { $codigoJava+= "continue "; }     
                 ( SPACE    { $html+= "&nbsp;"; }
                 | NEWLINE { $html+= "<br>"; }
                 | FORMFEED { $html+= "&nbsp;"; }
                 | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                 )*  
        (IDENTIFIER { $html+= $IDENTIFIER.text; }
                    { $codigoJava+= $IDENTIFIER.text+" "; }     
                    ( SPACE    { $html+= "&nbsp;"; }
                    | NEWLINE { $html+= "<br>"; }
                    | FORMFEED { $html+= "&nbsp;"; }
                    | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                    )* 
        )? 
        SEMI { $html+= ";"; } 
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
    |   expression { $html+= $expression.html; }
                   { $codigoJava+= $expression.codigoJava; } 
        SEMI { $html+= ";"; } 
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*  
    |   IDENTIFIER { $html+= $IDENTIFIER.text; }
                   { $codigoJava+= $IDENTIFIER.text+" "; }     
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )*
        COLON { $html+= ":"; } 
              { $codigoJava+= ":"; }    
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*
        S1 = statement { $html+= $S1.html; }
                       { $codigoJava+= $S1.codigoJava; }
    |   SEMI { $html+= ";"; } 
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
    ;

switchBlockStatementGroups returns [String html="", String codigoJava=""] 
    :   (switchBlockStatementGroup { $html+= $switchBlockStatementGroup.html; }
                                   { $codigoJava+= $switchBlockStatementGroup.codigoJava; } 
        )*
    ;

switchBlockStatementGroup returns [String html="", String codigoJava=""] 
    :
        switchLabel { $html+= $switchLabel.html; }
                    { $codigoJava+= $switchLabel.codigoJava; }
        (blockStatement { $html+= $blockStatement.html; } 
                        { $codigoJava+= $blockStatement.codigoJava; }
        )*
    ;

switchLabel returns [String html="", String codigoJava=""] 
    :   CASE { $html+= "<font class=\"reservedesc\">case</font>"; } 
             { $codigoJava+= "case "; }     
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*  
        expression { $html+= $expression.html; }
                   { $codigoJava+= $expression.codigoJava; }
        COLON { $html+= ":"; }
              { $codigoJava+= ":"; }    
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*
    |   DEFAULT { $html+= "<font class=\"reservedesc\">default</font>"; }
                { $codigoJava+= "default "; }      
                ( SPACE    { $html+= "&nbsp;"; }
                | NEWLINE { $html+= "<br>"; }
                | FORMFEED { $html+= "&nbsp;"; }
                | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                )*  
        COLON { $html+= ":"; }
              { $codigoJava+= ":"; }    
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )* 
    ;


trystatement returns [String html="", String codigoJava=""] 
    :   TRY { $html+= "<font class=\"reservedesc\">try</font>"; } 
            { $codigoJava+= "try "; }      
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*  
        B1 = block { $html+= $B1.html; } 
                   { $codigoJava+= $B1.codigoJava; }
        (   CS1 = catches { $html+= $CS1.html; }
                          { $codigoJava+= $CS1.codigoJava; }  
            FINALLY { $html+= "<font class=\"reservedesc\">finally</font>"; }
                    { $codigoJava+= "finally "; }      
                    ( SPACE    { $html+= "&nbsp;"; }
                    | NEWLINE { $html+= "<br>"; }
                    | FORMFEED { $html+= "&nbsp;"; }
                    | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                    )*   
            B2 = block { $html+= $B2.html; }
                       { $codigoJava+= $B2.codigoJava; }
        |   CS2 = catches { $html+= $CS2.html; }
                          { $codigoJava+= $CS2.codigoJava; }
        |   FINALLY {  $html+= "<font class=\"reservedesc\">finally</font>"; } 
                    { $codigoJava+= "finally "; }      
                    ( SPACE    { $html+= "&nbsp;"; }
                    | NEWLINE { $html+= "<br>"; }
                    | FORMFEED { $html+= "&nbsp;"; }
                    | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                    )*  
            B2 = block { $html+= $B2.html; }
                       { $codigoJava+= $B2.codigoJava; }
        )
     ;

catches returns [String html="", String codigoJava=""] 
    :   CC1 = catchClause { $html+= $CC1.html; }
                          { $codigoJava+= $CC1.codigoJava; }
        (CC2 = catchClause { $html+= $CC2.html; }
                           { $codigoJava+= $CC2.codigoJava; }
        )*
    ;

catchClause returns [String html="", String codigoJava=""] 
    :   CATCH { $html+= "<font class=\"reservedesc\">catch</font>"; }
              { $codigoJava+= "catch "; }    
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*
        LPAREN { $html+= "("; }
               { $codigoJava+= "("; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )* 
        formalParameter { $html+= $formalParameter.html; }
                        { $codigoJava+= $formalParameter.codigoJava; } 
        RPAREN { $html+= ")"; } 
               { $codigoJava+= ")"; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*  
        block { $html+= $block.html; }
              { $codigoJava+= $block.codigoJava; }
    ;

formalParameter returns [String html="", String codigoJava=""] 
    scope{
        String typeParameter;
        String nameParameter;
    }
    @init{
        $formalParameter::typeParameter="";
        $formalParameter::nameParameter="";
    }
    :   variableModifiers { $html+= $variableModifiers.html; }
                          { $codigoJava+= $variableModifiers.codigoJava; } 
        type { $html+= $type.html; } 
             { $codigoJava+= $type.codigoJava; }
             { $formalParameter::typeParameter = $type.codigoJava; } 
        IDENTIFIER { $html+= $IDENTIFIER.text; }
                   { $codigoJava+= $IDENTIFIER.text+" "; } 
                   { $formalParameter::nameParameter+= $IDENTIFIER.text; } 
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )*
        (LBRACKET { $html+= "["; }
                  { $codigoJava+= "["; } 
                  { $formalParameter::nameParameter+= "[";}        
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
         RBRACKET { $html+= "]"; }
                  { $codigoJava+= "]"; }        
                  { $formalParameter::nameParameter+= "]";}        
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
        )*
    ;

forstatement returns [String html="", String codigoJava=""] 
    :
        FOR { $html+= "<font class=\"reservedesc\">for</font>"; }
            { $codigoJava+= "for "; }      
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
        LPAREN { $html+= "("; } 
               { $codigoJava+= "("; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )* 
        variableModifiers { $html+= $variableModifiers.html; } 
                          { $codigoJava+= $variableModifiers.codigoJava; } 
        type { $html+= $type.html; } 
             { $codigoJava+= $type.codigoJava; }
        IDENTIFIER { $html+= $IDENTIFIER.text; }
                   { $codigoJava+= $IDENTIFIER.text+" "; }     
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )*
        COLON { $html+= ":"; }
              { $codigoJava+= ":"; }    
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*
        expression { $html+= $expression.html; }
                   { $codigoJava+= $expression.codigoJava; }
        RPAREN { $html+= ")"; } 
               { $codigoJava+= ")"; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*  
        statement { $html+= $statement.html; }
                  { $codigoJava+= $statement.codigoJava; }

        // normal for loop
    |   FOR { $html+= "<font class=\"reservedesc\">for</font>"; }
            { $codigoJava+= "for "; }      
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*  
        LPAREN { $html+= "("; } 
               { $codigoJava+= "("; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
        (forInit { $html+= $forInit.html; }
                 { $codigoJava+= $forInit.codigoJava; }
        )? 
        SEMI { $html+= ";"; } 
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
        (expression { $html+= $expression.html; }
                    { $codigoJava+= $expression.codigoJava; }
        )? 
        SEMI { $html+= ";"; } 
             { $codigoJava+= ";"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*
        (expressionList { $html+= $expressionList.html; }
                        { $codigoJava+= $expressionList.codigoJava; }
        )? 
        RPAREN { $html+= ")"; } 
               { $codigoJava+= ")"; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )* 
        statement { $html+= $statement.html; }
                  { $codigoJava+= $statement.codigoJava; }  
    ;

forInit returns [String html="", String codigoJava=""] 
    :   localVariableDeclaration { $html+= $localVariableDeclaration.html; }
                                 { $codigoJava+= $localVariableDeclaration.codigoJava; } 
    |   expressionList { $html+= $expressionList.html; }
                       { $codigoJava+= $expressionList.codigoJava; }
    ;

parExpression returns [String html="", String codigoJava=""] 
    :   LPAREN { $html+= "("; } 
               { $codigoJava+= "("; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )* 
        expression { $html+= $expression.html; }
                   { $codigoJava+= $expression.codigoJava; }
        RPAREN { $html+= ")"; } 
               { $codigoJava+= ")"; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*  
    ;

expressionList returns [String html="", String codigoJava=""] 
    :   E1 = expression { $html+= $E1.html; }
                        { $codigoJava+= $E1.codigoJava; }
        (COMMA { $html+= ","; } 
               { $codigoJava+= ","; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*  
        E2 = expression { $html+= $E2.html; }
                        { $codigoJava+= $E2.codigoJava; }
        )*
    ;


expression returns [String html="", String codigoJava=""] 
    :   conditionalExpression { $html+= $conditionalExpression.html; }
                              { $codigoJava+= $conditionalExpression.codigoJava; }
        (assignmentOperator { $html+= $assignmentOperator.html; } 
                            { $codigoJava+= $assignmentOperator.codigoJava; }  
         E = expression { $html+= $E.html; }
                        { $codigoJava+= $E.codigoJava; }
        )?
    ;


assignmentOperator returns [String html="", String codigoJava=""] 
    :   
    (   EQ { $html+= "="; } 
           { $codigoJava+= "="; }
    |   PLUSEQ { $html+= "+="; }
               { $codigoJava+= "+="; }  
    |   SUBEQ { $html+= "-="; }
              { $codigoJava+= "-="; } 
    |   STAREQ { $html+= "*="; }
               { $codigoJava+= "*="; } 
    |   SLASHEQ { $html+= "/="; }
                { $codigoJava+= "/="; }
    |   AMPEQ { $html+= "&="; }
              { $codigoJava+= "&="; }  
    |   BAREQ { $html+= "|="; }
              { $codigoJava+= "|="; }
    |   CARETEQ { $html+= "^="; }
                { $codigoJava+= "^="; }
    |   PERCENTEQ { $html+= "\%="; }
                  { $codigoJava+= "\%="; }  
    |   LT LT EQ { $html+= "<&lt;&lt;="; }
                 { $codigoJava+= "<&lt;&lt;="; } 
    |   GT GT GT EQ { $html+= ">>>="; }
                    { $codigoJava+= ">>>="; } 
    |   GT GT EQ { $html+= ">>="; }
                 { $codigoJava+= ">>="; }
    )
    ( SPACE    { $html+= "&nbsp;"; }
    | NEWLINE { $html+= "<br>"; }
    | FORMFEED { $html+= "&nbsp;"; }
    | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
    )*
    ;


conditionalExpression returns [String html="", String codigoJava=""] 
    :   conditionalOrExpression { $html+= $conditionalOrExpression.html; }  
                                { $codigoJava+= $conditionalOrExpression.codigoJava; }
        (QUES { $html+= "?"; } 
              { $codigoJava+= "?"; }    
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*
         expression { $html+= $expression.html; }
                    { $codigoJava+= $expression.codigoJava; }  
         COLON { $html+= ":"; } 
               { $codigoJava+= ":"; }    
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
         CE1 = conditionalExpression { $html+= $CE1.html; }
                                     { $codigoJava+= $CE1.codigoJava; }
        )?
    ;

conditionalOrExpression returns [String html="", String codigoJava=""] 
    :   CAE1 = conditionalAndExpression { $html+= $CAE1.html; }
                                        { $codigoJava+= $CAE1.codigoJava; }
        (BARBAR { $html+= "||"; } 
                { $codigoJava+= "||"; }       
                ( SPACE    { $html+= "&nbsp;"; }
                | NEWLINE { $html+= "<br>"; }
                | FORMFEED { $html+= "&nbsp;"; }
                | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                )*  
         CAE2 = conditionalAndExpression { $html+= $CAE2.html; }
                                         { $codigoJava+= $CAE2.codigoJava; }
        )*
    ;

conditionalAndExpression returns [String html="", String codigoJava=""] 
    :   IOE1 = inclusiveOrExpression { $html+= $IOE1.html; } 
                                     { $codigoJava+= $IOE1.codigoJava; } 
        (AMPAMP { $html+= "&&"; } 
                { $codigoJava+= "&&"; }       
                ( SPACE    { $html+= "&nbsp;"; }
                | NEWLINE { $html+= "<br>"; }
                | FORMFEED { $html+= "&nbsp;"; }
                | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                )*  
         IOE2 = inclusiveOrExpression { $html+= $IOE2.html; }
                                      { $codigoJava+= $IOE2.codigoJava; } 
        )*
    ;

inclusiveOrExpression returns [String html="", String codigoJava=""] 
    :   EOE1 = exclusiveOrExpression { $html+= $EOE1.html; }
                                     { $codigoJava+= $EOE1.codigoJava; }
        (BAR { $html+= "|"; } 
             { $codigoJava+= "|"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*  
         EOE2 = exclusiveOrExpression { $html+= $EOE2.html; }
                                      { $codigoJava+= $EOE2.codigoJava; }
        )*
    ;

exclusiveOrExpression returns [String html="", String codigoJava=""] 
    :   AE1 = andExpression { $html+= $AE1.html; }
                            { $codigoJava+= $AE1.codigoJava; }
        (CARET { $html+= "^"; } 
               { $codigoJava+= "^"; }        
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*  
         AE2 = andExpression { $html+= $AE2.html; }
                             { $codigoJava+= $AE2.codigoJava; }   
        )*
    ;

andExpression returns [String html="", String codigoJava=""] 
    :   EE1 = equalityExpression { $html+= $EE1.html; }
                                 { $codigoJava+= $EE1.codigoJava; }   
        (AMP { $html+= "&"; } 
             { $codigoJava+= "&"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*
         EE2 = equalityExpression { $html+= $EE2.html; }
                                  { $codigoJava+= $EE2.codigoJava; }
        )*
    ;

equalityExpression returns [String html="", String codigoJava=""] 
    :   IOE1 = instanceOfExpression { $html+= $IOE1.html; }
                                    { $codigoJava+= $IOE1.codigoJava; } 
        (
            (   EQEQ { $html+= "=="; } 
                     { $codigoJava+= "=="; }       
            |   BANGEQ { $html+= "!="; }
                       { $codigoJava+= "!="; }   
            )
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )* 
            IOE2 = instanceOfExpression { $html+= $IOE2.html; }
                                        { $codigoJava+= $IOE2.codigoJava; }
        )*
    ;

instanceOfExpression returns [String html="", String codigoJava=""] 
    :   relationalExpression { $html+= $relationalExpression.html; }
                             { $codigoJava+= $relationalExpression.codigoJava; }
        (INSTANCEOF { $html+= "<font class=\"reservedesc\">instanceof</font>"; }
                    { $codigoJava+= "instanceof "; }      
                    ( SPACE    { $html+= "&nbsp;"; }
                    | NEWLINE { $html+= "<br>"; }
                    | FORMFEED { $html+= "&nbsp;"; }
                    | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                    )*
         type { $html+= $type.html; }
              { $codigoJava+= $type.codigoJava; }
        )?
    ;

relationalExpression returns [String html="", String codigoJava=""] 
    :   SE1 = shiftExpression { $html+= $SE1.html; }
                              { $codigoJava+= $SE1.codigoJava; }  
        (relationalOp { $html+= $relationalOp.html; }
                      { $codigoJava+= $relationalOp.codigoJava; }
         SE2 = shiftExpression { $html+= $SE2.html; }
                               { $codigoJava+= $SE2.codigoJava; }
        )*
    ;

relationalOp returns [String html="", String codigoJava=""] 
    :    
    (   LT EQ { $html+= "&lt;="; } 
              { $codigoJava+= "&lt;="; }
    |   GT EQ { $html+= ">="; }
              { $codigoJava+= ">="; } 
    |   LT { $html+= "&lt;"; } 
           { $codigoJava+= "&lt;"; }  
    |   GT { $html+= ">"; }
           { $codigoJava+= ">"; } 
    )
    ( SPACE    { $html+= "&nbsp;"; }
    | NEWLINE { $html+= "<br>"; }
    | FORMFEED { $html+= "&nbsp;"; }
    | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
    )*
    ;

shiftExpression returns [String html="", String codigoJava=""] 
    :   AE1 = additiveExpression { $html+= $AE1.html; }
                                 { $codigoJava+= $AE1.codigoJava; }
        (shiftOp { $html+= $shiftOp.html; }
                 { $codigoJava+= $shiftOp.codigoJava; }
         AE2 = additiveExpression { $html+= $AE2.html; }
                                  { $codigoJava+= $AE2.codigoJava; }
        )*
    ;


shiftOp returns [String html="", String codigoJava=""] 
    :    
    (   LT LT { $html+= "&lt;&lt;"; } 
              { $codigoJava+= "&lt;&lt;"; }
    |   GT GT GT { $html+= ">>>"; }
                 { $codigoJava+= ">>>"; }
    |   GT GT { $html+= ">>"; } 
              { $codigoJava+= ">>"; }  
    )
    ( SPACE    { $html+= "&nbsp;"; }
    | NEWLINE { $html+= "<br>"; }
    | FORMFEED { $html+= "&nbsp;"; }
    | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
    )*
    ;


additiveExpression returns [String html="", String codigoJava=""] 
    :   ME1 = multiplicativeExpression { $html+= $ME1.html; }
                                       { $codigoJava+= $ME1.codigoJava; }  
        (
            (   PLUS { $html+= "+"; } 
                     { $codigoJava+= "+"; }
            |   SUB { $html+= "-"; } 
                    { $codigoJava+= "-"; }
            )
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
            ME2 = multiplicativeExpression { $html+= $ME2.html; }
                                           { $codigoJava+= $ME2.codigoJava; }
         )*
    ;

multiplicativeExpression returns [String html="", String codigoJava=""] 
    :
        UE1 = unaryExpression { $html+= $UE1.html; }
                              { $codigoJava+= $UE1.codigoJava; }
        (
            (   STAR { $html+= "*"; } 
                     { $codigoJava+= "*"; } 
            |   SLASH { $html+= "/"; } 
                      { $codigoJava+= "/"; }
            |   PERCENT { $html+= "\%"; }
                        { $codigoJava+= "\%"; }
            )
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
            UE2 = unaryExpression { $html+= $UE2.html; }
                                  { $codigoJava+= $UE2.codigoJava; }
        )*
    ;

/**
 * NOTE: for '+' and '-', if the next token is int or long interal, then it's not a unary expression.
 *       it's a literal with signed value. INTLTERAL AND LONG LITERAL are added here for this.
 */
unaryExpression returns [String html="", String codigoJava=""] 
    :   PLUS { $html+= "+"; } 
             { $codigoJava+= "+"; }    
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*   
        UE1 = unaryExpression { $html+= $UE1.html; }
                              { $codigoJava+= $UE1.codigoJava; }
    |   SUB { $html+= "-"; } 
            { $codigoJava+= "-"; }  
            ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*   
        UE2 = unaryExpression { $html+= $UE2.html; }
                              { $codigoJava+= $UE2.codigoJava; }
    |   PLUSPLUS { $html+= "++"; } 
                 { $codigoJava+= "++"; }
                 ( SPACE    { $html+= "&nbsp;"; }
                 | NEWLINE { $html+= "<br>"; }
                 | FORMFEED { $html+= "&nbsp;"; }
                 | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                 )*   
        UE2 = unaryExpression { $html+= $UE2.html; }
                              { $codigoJava+= $UE2.codigoJava; }
    |   SUBSUB { $html+= "--"; } 
               { $codigoJava+= "--"; } 
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*   
        UE2 = unaryExpression { $html+= $UE2.html; }
                              { $codigoJava+= $UE2.codigoJava; }
    |   unaryExpressionNotPlusMinus { $html+= $unaryExpressionNotPlusMinus.html; }
                                    { $codigoJava+= $unaryExpressionNotPlusMinus.codigoJava; }       
    ;

unaryExpressionNotPlusMinus returns [String html="", String codigoJava=""] 
    :   TILDE { $html+= "~"; } 
              { $codigoJava+= "~"; }       
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*  
        unaryExpression { $html+= $unaryExpression.html; }
                        { $codigoJava+= $unaryExpression.codigoJava; }    
    |   BANG { $html+= "!"; } 
             { $codigoJava+= "!"; }       
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*
        unaryExpression { $html+= $unaryExpression.html; }
                        { $codigoJava+= $unaryExpression.codigoJava; }
    |   castExpression { $html+= $castExpression.html; }
                       { $codigoJava+= $castExpression.codigoJava; }  
    |   primary { $html+= $primary.html; }
                { $codigoJava+= $primary.codigoJava; }    
        (selector { $html+= $selector.html; }
                  { $codigoJava+= $selector.codigoJava; }
        )*
        (   PLUSPLUS { $html+= "++"; } 
                     { $codigoJava+= "++"; }          
                     ( SPACE    { $html+= "&nbsp;"; }
                     | NEWLINE { $html+= "<br>"; }
                     | FORMFEED { $html+= "&nbsp;"; }
                     | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                     )* 
        |   SUBSUB { $html+= "--"; } 
                   { $codigoJava+= "--"; }          
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )* 
        )?
    ;

castExpression returns [String html="", String codigoJava=""] 
    :   LPAREN { $html+= "("; } 
               { $codigoJava+= "("; }           
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*  
        primitiveType { $html+= $primitiveType.html; }
                      { $codigoJava+= $primitiveType.codigoJava; }
        RPAREN { $html+= ")"; } 
               { $codigoJava+= ")"; }           
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
        unaryExpression { $html+= $unaryExpression.html; }
                        { $codigoJava+= $unaryExpression.codigoJava; } 
    |   LPAREN { $html+= "("; } 
               { $codigoJava+= "("; }           
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
        type { $html+= $type.html; }
             { $codigoJava+= $type.codigoJava; }
        RPAREN { $html+= ")"; } 
               { $codigoJava+= ")"; }           
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*
        unaryExpressionNotPlusMinus { $html+= $unaryExpressionNotPlusMinus.html; }
                                    { $codigoJava+= $unaryExpressionNotPlusMinus.codigoJava; }       
    ;

/**
 * have to use scope here, parameter passing isn't well supported in antlr.
 */
primary returns [String html="", String codigoJava=""] 
    :   parExpression { $html+= $parExpression.html; }
                      { $codigoJava+= $parExpression.codigoJava; } 
    |   THIS { $html+= "<font class=\"reservedesc\">this</font>"; }
             { $codigoJava+= "this "; }        
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )* 
        (DOT { $html+= "."; } 
             { $codigoJava+= "."; }       
             ( SPACE    
             | NEWLINE 
             | FORMFEED    
             | TABULATION 
             )*
         IDENTIFIER { $html+= $IDENTIFIER.text; }
                    { $codigoJava+= $IDENTIFIER.text+" "; }        
                    ( SPACE    { $html+= "&nbsp;"; }
                    | NEWLINE { $html+= "<br>"; }
                    | FORMFEED { $html+= "&nbsp;"; }
                    | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                    )* 
        )*
        (identifierSuffix { $html+= $identifierSuffix.html; }
                          { $codigoJava+= $identifierSuffix.codigoJava; }  
        )?
    |   ID1 = IDENTIFIER { $html+= $ID1.text; } 
                         { $codigoJava+= $ID1.text+" "; }       
                         ( SPACE   
                         | NEWLINE 
                         | FORMFEED   
                         | TABULATION 
                         )* 
        (DOT { $html+= "."; } 
             { $codigoJava+= "."; }       
             ( SPACE    
             | NEWLINE 
             | FORMFEED    
             | TABULATION 
             )*
         ID2 = IDENTIFIER { $html+= $ID2.text; }
                          { $codigoJava+= $ID2.text+" "; }          
                          ( SPACE    { $html+= "&nbsp;"; }
                          | NEWLINE { $html+= "<br>"; }
                          | FORMFEED { $html+= "&nbsp;"; }
                          | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                          )*
        )*
        (identifierSuffix { $html+= $identifierSuffix.html; }
                          { $codigoJava+= $identifierSuffix.codigoJava; }
        )?
    |   SUPER { $html+= "<font class=\"reservedesc\">super</font>"; } 
              { $codigoJava+= "super "; }       
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )* 
        superSuffix { $html+= $superSuffix.html; }
                    { $codigoJava+= $superSuffix.codigoJava; }
    |   literal { $html+= $literal.html; }
                { $codigoJava+= $literal.codigoJava; }  
    |   creator { $html+= $creator.html; }
                { $codigoJava+= $creator.codigoJava; } 
    |   primitiveType { $html+= $primitiveType.html; }
                      { $codigoJava+= $primitiveType.codigoJava; }
        (LBRACKET { $html+= "["; } 
                  { $codigoJava+= "["; }       
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )* 
        RBRACKET { $html+= "]"; } 
                 { $codigoJava+= "]"; }       
                 ( SPACE    { $html+= "&nbsp;"; }
                 | NEWLINE { $html+= "<br>"; }
                 | FORMFEED { $html+= "&nbsp;"; }
                 | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                 )*
        )*
        DOT { $html+= "."; } 
            { $codigoJava+= "."; }       
            ( SPACE    
            | NEWLINE 
            | FORMFEED    
            | TABULATION 
            )*
        CLASS { $html+= "<font class=\"reservedesc\">class</font>"; }
              { $codigoJava+= "class "; }       
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )* 
    |   VOID { $html+= "<font class=\"reservedesc\">void</font>"; } 
             { $codigoJava+= "void "; }
             ( SPACE    
             | NEWLINE 
             | FORMFEED    
             | TABULATION 
             )*
        DOT { $html+= "."; } 
            { $codigoJava+= "."; }       
            ( SPACE    
            | NEWLINE 
            | FORMFEED    
            | TABULATION 
            )*
        CLASS { $html+= "<font class=\"reservedesc\">class</font>"; }
              { $codigoJava+= "class "; }
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )* 
    ;


superSuffix returns [String html="", String codigoJava=""] 
    :   arguments { $html+= $arguments.html; }
                  { $codigoJava+= $arguments.codigoJava; }
    |   DOT { $html+= "."; } 
            { $codigoJava+= "."; }       
            ( SPACE    
            | NEWLINE 
            | FORMFEED    
            | TABULATION 
            )*
        (typeArguments { $html+= $typeArguments.html; }
                       { $codigoJava+= $typeArguments.codigoJava; } 
        )?
        IDENTIFIER { $html+= $IDENTIFIER.text; }
                   { $codigoJava+= $IDENTIFIER.text+" "; }        
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )* 
        (arguments { $html+= $arguments.html; } 
                   { $codigoJava+= $arguments.codigoJava; }
        )?
    ;


identifierSuffix returns [String html="", String codigoJava=""] 
    :   (LBRACKET { $html+= "["; } 
                  { $codigoJava+= "["; }       
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )* 
         RBRACKET { $html+= "]"; } 
                  { $codigoJava+= "]"; }       
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
        )+
        DOT { $html+= "."; } 
            { $codigoJava+= "."; }       
            ( SPACE    
            | NEWLINE 
            | FORMFEED    
            | TABULATION 
            )*
        CLASS { $html+= "<font class=\"reservedesc\">class</font>"; }
              { $codigoJava+= "class "; }
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*
    |   (LBRACKET { $html+= "["; } 
                  { $codigoJava+= "["; }       
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
         expression { $html+= $expression.html; }
                    { $codigoJava+= $expression.codigoJava; } 
         RBRACKET { $html+= "]"; } 
                  { $codigoJava+= "]"; }       
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
        )+
    |   arguments { $html+= $arguments.html; }
                  { $codigoJava+= $arguments.codigoJava; }
    |   DOT { $html+= "."; } 
            { $codigoJava+= "."; }       
            ( SPACE    
            | NEWLINE 
            | FORMFEED    
            | TABULATION 
            )*
        CLASS { $html+= "<font class=\"reservedesc\">class</font>"; }
              { $codigoJava+= "class "; }
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*
    |   DOT { $html+= "."; } 
            { $codigoJava+= "."; }       
            ( SPACE    
            | NEWLINE 
            | FORMFEED    
            | TABULATION 
            )*
        nonWildcardTypeArguments { $html+= $nonWildcardTypeArguments.html; }
                                 { $codigoJava+= $nonWildcardTypeArguments.codigoJava; } 
        IDENTIFIER { $html+= $IDENTIFIER.text; } 
                   { $codigoJava+= $IDENTIFIER.text+" "; }        
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )*  
        arguments { $html+= $arguments.html; }
                  { $codigoJava+= $arguments.codigoJava; }
    |   DOT { $html+= "."; } 
            { $codigoJava+= "."; }       
            ( SPACE    
            | NEWLINE 
            | FORMFEED    
            | TABULATION 
            )*
        THIS { $html+= "<font class=\"reservedesc\">this</font>"; } 
             { $codigoJava+= "this "; }
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*   
    |   DOT { $html+= "."; } 
            { $codigoJava+= "."; }       
            ( SPACE    
            | NEWLINE 
            | FORMFEED    
            | TABULATION 
            )*
        SUPER { $html+= "<font class=\"reservedesc\">super</font>"; } 
              { $codigoJava+= "super "; }       
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*  
        arguments { $html+= $arguments.html; } 
                  { $codigoJava+= $arguments.codigoJava; } 
    |   innerCreator { $html+= $innerCreator.html; } 
                     { $codigoJava+= $innerCreator.codigoJava; }
    ;


selector returns [String html="", String codigoJava=""] 
    :   DOT { $html+= "."; } 
            { $codigoJava+= "."; }       
            ( SPACE    
            | NEWLINE 
            | FORMFEED    
            | TABULATION 
            )*
        IDENTIFIER { $html+= $IDENTIFIER.text; }
                   { $codigoJava+= $IDENTIFIER.text+" "; }        
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )* 
        (arguments { $html+= $arguments.html; } 
                   { $codigoJava+= $arguments.codigoJava; } 
        )?
    |   DOT { $html+= "."; } 
            { $codigoJava+= "."; }       
            ( SPACE    
            | NEWLINE 
            | FORMFEED    
            | TABULATION 
            )*
        THIS { $html+= "<font class=\"reservedesc\">this</font>"; } 
             { $codigoJava+= "this "; }
             ( SPACE    { $html+= "&nbsp;"; }
             | NEWLINE { $html+= "<br>"; }
             | FORMFEED { $html+= "&nbsp;"; }
             | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
             )*
    |   DOT { $html+= "."; } 
            { $codigoJava+= "."; }       
            ( SPACE    
            | NEWLINE 
            | FORMFEED    
            | TABULATION 
            )*
        SUPER { $html+= "<font class=\"reservedesc\">super</font>"; } 
              { $codigoJava+= "super "; }       
              ( SPACE    { $html+= "&nbsp;"; }
              | NEWLINE { $html+= "<br>"; }
              | FORMFEED { $html+= "&nbsp;"; }
              | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
              )*
        superSuffix { $html+= $superSuffix.html; } 
                    { $codigoJava+= $superSuffix.codigoJava; }
    |   innerCreator { $html+= $innerCreator.html; }
                     { $codigoJava+= $innerCreator.codigoJava; }
    |   LBRACKET { $html+= "["; } 
                 { $codigoJava+= "["; }       
                 ( SPACE    { $html+= "&nbsp;"; }
                 | NEWLINE { $html+= "<br>"; }
                 | FORMFEED { $html+= "&nbsp;"; }
                 | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                 )*
        expression { $html+= $expression.html; }
                   { $codigoJava+= $expression.codigoJava; }  
        RBRACKET { $html+= "]"; } 
                 { $codigoJava+= "]"; }       
                 ( SPACE    { $html+= "&nbsp;"; }
                 | NEWLINE { $html+= "<br>"; }
                 | FORMFEED { $html+= "&nbsp;"; }
                 | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                 )*
    ;

creator returns [String html="", String codigoJava=""] 
    :   NEW { $html+= "<font class=\"reservedesc\">new</font>"; } 
            { $codigoJava+= "new "; }         
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*  
        nonWildcardTypeArguments { $html+= $nonWildcardTypeArguments.html; } 
                                 { $codigoJava+= $nonWildcardTypeArguments.codigoJava; }  
        classOrInterfaceType { $html+= $classOrInterfaceType.html; }
                             { $codigoJava+= $classOrInterfaceType.codigoJava; }
        classCreatorRest { $html+= $classCreatorRest.html; }
                         { $codigoJava+= $classCreatorRest.codigoJava; }
    |   NEW { $html+= "<font class=\"reservedesc\">new</font>"; } 
            { $codigoJava+= "new "; }         
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
        classOrInterfaceType { $html+= $classOrInterfaceType.html; } 
                             { $codigoJava+= $classOrInterfaceType.codigoJava; } 
        classCreatorRest { $html+= $classCreatorRest.html; } 
                         { $codigoJava+= $classCreatorRest.codigoJava; } 
    |   arrayCreator { $html+= $arrayCreator.html; } 
                     { $codigoJava+= $arrayCreator.codigoJava; }
    ;

arrayCreator returns [String html="", String codigoJava=""] 
    :   NEW { $html+= "<font class=\"reservedesc\">new</font>"; } 
            { $codigoJava+= "new "; }         
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
        createdName { $html+= $createdName.html; } 
                    { $codigoJava+= $createdName.codigoJava; } 
        LBRACKET { $html+= "["; } 
                 { $codigoJava+= "["; }       
                 ( SPACE    { $html+= "&nbsp;"; }
                 | NEWLINE { $html+= "<br>"; }
                 | FORMFEED { $html+= "&nbsp;"; }
                 | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                 )*
        RBRACKET { $html+= "]"; } 
                 { $codigoJava+= "]"; }       
                 ( SPACE    { $html+= "&nbsp;"; }
                 | NEWLINE { $html+= "<br>"; }
                 | FORMFEED { $html+= "&nbsp;"; }
                 | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                 )*
        (LBRACKET { $html+= "["; } 
                  { $codigoJava+= "["; }       
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )* 
         RBRACKET { $html+= "]"; } 
                  { $codigoJava+= "]"; }       
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
        )*
        arrayInitializer { $html+= $arrayInitializer.html; }
                         { $codigoJava+= $arrayInitializer.codigoJava; }

    |   NEW { $html+= "<font class=\"reservedesc\">new</font>"; } 
            { $codigoJava+= "new "; }         
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
        createdName { $html+= $createdName.html; }
                    { $codigoJava+= $createdName.codigoJava; } 
        LBRACKET { $html+= "["; } 
                 { $codigoJava+= "["; }       
                 ( SPACE    { $html+= "&nbsp;"; }
                 | NEWLINE { $html+= "<br>"; }
                 | FORMFEED { $html+= "&nbsp;"; }
                 | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                 )*
        E1 = expression { $html+= $E1.html; } 
                        { $codigoJava+= $E1.codigoJava; } 
        RBRACKET { $html+= "]"; } 
                 { $codigoJava+= "]"; }       
                 ( SPACE    { $html+= "&nbsp;"; }
                 | NEWLINE { $html+= "<br>"; }
                 | FORMFEED { $html+= "&nbsp;"; }
                 | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                 )*
        (   LBRACKET { $html+= "["; } 
                     { $codigoJava+= "["; }       
                     ( SPACE    { $html+= "&nbsp;"; }
                     | NEWLINE { $html+= "<br>"; }
                     | FORMFEED { $html+= "&nbsp;"; }
                     | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                     )* 
            E2 = expression { $html+= $E2.html; } 
                            { $codigoJava+= $E2.codigoJava; }
            RBRACKET { $html+= "]"; } 
                     { $codigoJava+= "]"; }       
                     ( SPACE    { $html+= "&nbsp;"; }
                     | NEWLINE { $html+= "<br>"; }
                     | FORMFEED { $html+= "&nbsp;"; }
                     | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                     )*
        )*
        (LBRACKET { $html+= "["; } 
                  { $codigoJava+= "["; }       
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
         RBRACKET { $html+= "]"; } 
                  { $codigoJava+= "]"; }       
                  ( SPACE    { $html+= "&nbsp;"; }
                  | NEWLINE { $html+= "<br>"; }
                  | FORMFEED { $html+= "&nbsp;"; }
                  | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                  )*
        )*
    ;

variableInitializer returns [String html="", String codigoJava=""] 
    :   arrayInitializer { $html+= $arrayInitializer.html; }
                         { $codigoJava+= $arrayInitializer.codigoJava; }        
    |   expression { $html+= $expression.html; }
                   { $codigoJava+= $expression.codigoJava; }
    ;

arrayInitializer returns [String html="", String codigoJava=""] 
    :   LBRACE { $html+= "{"; } 
               { $codigoJava+= "{"; }       
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )* 
        ( VI1 = variableInitializer { $html+= $VI1.html; }
                                    { $codigoJava+= $VI1.codigoJava; } 
            (COMMA { $html+= ","; } 
                   { $codigoJava+= ","; }       
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )*
             VI2 = variableInitializer { $html+= $VI2.html; }
                                       { $codigoJava+= $VI2.codigoJava; }
            )*
        )?
        (COMMA { $html+= ","; } 
               { $codigoJava+= ","; }       
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*  
        )?
        RBRACE { $html+= "}"; }  
               { $codigoJava+= "}"; }       
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*            
        //Yang's fix, position change.
    ;


createdName returns [String html="", String codigoJava=""] 
    :   classOrInterfaceType { $html+= $classOrInterfaceType.html; }  
                             { $codigoJava+= $classOrInterfaceType.codigoJava; }
    |   primitiveType { $html+= $primitiveType.html; }
                      { $codigoJava+= $primitiveType.codigoJava; } 
    ;

innerCreator returns [String html="", String codigoJava=""] 
    :   DOT { $html+= "."; } 
            { $codigoJava+= "."; }           
            ( SPACE    
            | NEWLINE 
            | FORMFEED    
            | TABULATION 
            )* 
        NEW { $html+= "<font class=\"reservedesc\">new</font>"; } 
            { $codigoJava+= "new "; }         
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
        (nonWildcardTypeArguments { $html+= $nonWildcardTypeArguments.html; } 
                                  { $codigoJava+= $nonWildcardTypeArguments.codigoJava; } 
        )?
        IDENTIFIER { $html+= $IDENTIFIER.text; } 
                   { $codigoJava+= $IDENTIFIER.text+" "; }        
                   ( SPACE    { $html+= "&nbsp;"; }
                   | NEWLINE { $html+= "<br>"; }
                   | FORMFEED { $html+= "&nbsp;"; }
                   | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                   )* 
        (typeArguments { $html+= $typeArguments.html; }
                       { $codigoJava+= $typeArguments.codigoJava; }
        )?
        classCreatorRest { $html+= $classCreatorRest.html; } 
                         { $codigoJava+= $classCreatorRest.codigoJava; }
    ;


classCreatorRest returns [String html="", String codigoJava=""] 
    :   arguments { $html+= $arguments.html; }
                  { $codigoJava+= $arguments.codigoJava; }         
        (classBody { $html+= $classBody.html; }
                   { $codigoJava+= $classBody.codigoJava; }
        )?
    ;


nonWildcardTypeArguments returns [String html="", String codigoJava=""] 
    :   LT  { $html+= "&lt;"; } 
            { $codigoJava+= "&lt;"; }       
            ( SPACE    { $html+= "&nbsp;"; }
            | NEWLINE { $html+= "<br>"; }
            | FORMFEED { $html+= "&nbsp;"; }
            | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
            )*
        typeList { $html+= $typeList.html; } 
                 { $codigoJava+= $typeList.codigoJava; }
        GT { $html+= ">"; } 
           { $codigoJava+= ">"; }       
           ( SPACE    { $html+= "&nbsp;"; }
           | NEWLINE { $html+= "<br>"; }
           | FORMFEED { $html+= "&nbsp;"; }
           | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
           )* 
    ;

arguments returns [String html="", String codigoJava=""] 
    :   LPAREN { $html+= "("; } 
               { $codigoJava+= "("; }       
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )*   
        (expressionList { $html+= $expressionList.html; }
                        { $codigoJava+= $expressionList.codigoJava; }
        )? 
        RPAREN { $html+= ")"; } 
               { $codigoJava+= ")"; }       
               ( SPACE    { $html+= "&nbsp;"; }
               | NEWLINE { $html+= "<br>"; }
               | FORMFEED { $html+= "&nbsp;"; }
               | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
               )* 
    ;

literal returns [String html="", String codigoJava=""] 
    :   
    (   INTLITERAL { $html+= $INTLITERAL.text; }
                   { $codigoJava+= $INTLITERAL.text+" "; }
    |   LONGLITERAL { $html+= $LONGLITERAL.text; } 
                    { $codigoJava+= $LONGLITERAL.text+" "; }
    |   FLOATLITERAL { $html+= $FLOATLITERAL.text; }
                     { $codigoJava+= $FLOATLITERAL.text+" "; } 
    |   DOUBLELITERAL { $html+= $DOUBLELITERAL.text; }
                      { $codigoJava+= $DOUBLELITERAL.text+" "; }
    |   CHARLITERAL { $html+= $CHARLITERAL.text; }
                    { $codigoJava+= $CHARLITERAL.text+" "; }
    |   STRINGLITERAL  { $html+=  "<font class=\"stringsesc\">"+$STRINGLITERAL.text.replaceAll("<","<!--").replaceAll(">","-->")+"</font>";} 
                       { $codigoJava+= $STRINGLITERAL.text+" "; }
    |   TRUE { $html+= "<font class=\"reservedesc\">"+$TRUE.text+"</font>"; }
             { $codigoJava+= $TRUE.text+" "; }
    |   FALSE { $html+= "<font class=\"reservedesc\">"+$FALSE.text+"</font>"; }
              { $codigoJava+= $FALSE.text+" "; }
    |   NULL { $html+= "<font class=\"reservedesc\">"+$NULL.text+"</font>"; } 
             { $codigoJava+= $NULL.text+" "; }
    )
    ( SPACE    { $html+= "&nbsp;"; }
    | NEWLINE { $html+= "<br>"; }
    | FORMFEED { $html+= "&nbsp;"; }
    | TABULATION { $html+= "&nbsp;&nbsp;&nbsp;&nbsp;"; }
    )*
    ;

/**
 * These are headers help to make syntatical predicates, not necessary but helps to make grammar faster.
 */

classHeader
    :   modifiers CLASS IDENTIFIER
    ;

enumHeader
    :   modifiers (ENUM|IDENTIFIER) IDENTIFIER
    ;

interfaceHeader
    :   modifiers INTERFACE IDENTIFIER
    ;

annotationHeader
    :   modifiers MONKEYS_AT INTERFACE IDENTIFIER
    ;

typeHeader
    :   modifiers (CLASS|ENUM|(MONKEYS_AT ? INTERFACE)) IDENTIFIER
    ;

methodHeader
    :   modifiers typeParameters? (type|VOID)? IDENTIFIER RPAREN
    ;

fieldHeader
    :   modifiers type IDENTIFIER (LBRACKET RBRACKET)* (EQ|COMMA|SEMI)
    ;

localVariableHeader
    :   variableModifiers type IDENTIFIER (LBRACKET RBRACKET)* (EQ|COMMA|SEMI)
    ;